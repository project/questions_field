<?php


function questions_field_overview($bundle_name = NULL, $entity_type = NULL) {
    // When displaying the page, make sure the list of fields is up-to-date.
    field_info_cache_clear();

    $fields = field_info_fields();
    $instances = field_info_instances($entity_type, $bundle_name);
    $instances_clone = $instances;
    if ($entity_type && $bundle_name) {
        $instances = array();
        $instances[$entity_type][$bundle_name] = $instances_clone;
    } elseif ($entity_type) {
        $instances = array();
        $instances[$entity_type] = $instances_clone;
    }
    $entities = $entity_type ? array($entity_type => entity_get_info($entity_type)) : entity_get_info();

    $headers = array(
        'field_name' => array('data' => 'Field name'),
        'operations' => array('data' => 'Operations')
    );


    $build = array();
    $build['#attached']['library'][] = array('system', 'drupal.collapse');

    foreach ($instances as $_entity_type => $bundles) {
        foreach ($bundles as $_bundle => $bundle_field_instances) {
            if (empty($bundle_field_instances))
                continue;
            $rows = array();
            foreach ($bundle_field_instances as $field_name => $instance) {
                if (isset($fields[$field_name]['type']) && $fields[$field_name]['type'] == 'questions') {
                    $manage_field = array(
                        '#type' => 'link',
                        '#title' => t('Manage Questions'),
                        '#href' => 'admin/structure/questions_field/' . $_entity_type . '/' . $_bundle . '/fields/' . $field_name,
                        '#options' => array('query' => array(), 'attributes' => array('title' => t('Manage questions.'))),
                        '#query' => drupal_get_destination(),
                    );
                    if (questions_field_manage_components_access($_entity_type, $_bundle, $field_name)) {
                        $rows[] = array(
                            'field_name' => $instance['label'],
                            'operations' => render($manage_field)
                        );
                    }
                }
            }
            if (!empty($rows)) {
                if (empty($build[$_entity_type])) {
                    if ($entity_type) {
                        $build[$entity_type] = array();
                    } else {
                        $build[$_entity_type] = array(
                            '#type' => 'fieldset',
                            '#title' => $entities[$_entity_type]['label'],
                            '#collapsible' => TRUE,
                            '#collapsed' => TRUE,
                            '#attributes' => array(
                                'class' => array('collapsible collapsed'),
                            ),
                        );
                    }
                }
                $build[$_entity_type][$_bundle] = array(
                    '#prefix' => '<h2>' . $entities[$_entity_type]['bundles'][$_bundle]['label'] . '</h2>',
                    '#markup' => theme('table', array(
                        'header' => $headers,
                        'rows' => $rows,
                        'attributes' => array('class' => array('invited_users')),
                        '#empty' => t('No invited users yet'),
                            )
                    ),
                    '#access' => true);
            }
        }
    }
    return $build;
}
