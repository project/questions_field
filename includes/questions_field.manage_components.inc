<?php

function questions_field_manage_components($entity_type, $bundle_name, $field_name){
  $components = questions_field_get_component_load($entity_type, $bundle_name, $field_name);

  $output = drupal_get_form('questions_field_manage_components_form', $entity_type, $bundle_name, $field_name, $components);


  return array(
    '#theme' => 'questions_field_manage_components_page',
    '#form' => $output,
    '#fields' => $components
  );
}


/**
 * Theme the output of the fields.
 *
 * This theming provides a way to toggle between the editing modes if Form
 * Builder module is available.
 */
function theme_questions_field_manage_components_page($variables) {

  $form = $variables['form'];
  return drupal_render($form);
}

/**
 * The table-based listing of all components for this Questions field.
 */
function questions_field_manage_components_form($form, &$form_state, $entity_type, $bundle_name, $field_name, $components) {

  $form = array(
    '#tree' => TRUE,
    '#components' => $components
  );

  $options = array();
  if($components){
  foreach ($components as $cid => $component) {
    $options[$cid] = check_plain($component['name']);
    $form['components'][$cid]['cid'] = array(
      '#type' => 'hidden',
      '#default_value' => $component['cid'],
    );
    $form['components'][$cid]['weight'] = array(
      '#type' => 'textfield',
      '#size' => 4,
      '#title' => t('Weight'),
      '#default_value' => $component['weight'],
    );
    $form['components'][$cid]['required'] = array(
      '#type' => 'checkbox',
      '#title' => t('Required'),
      '#default_value' => $component['required'],
      '#access' => questions_field_component_feature($component['type'], 'required'),
    );
    if (!isset($max_weight) || $component['weight'] > $max_weight) {
      $max_weight = $component['weight'];
    }
   }
  }

  $form['add']['name'] = array(
    '#type' => 'textfield',
    '#size' => 24,
    '#maxlength' => NULL,
  );

  $form['add']['type'] = array(
    '#type' => 'select',
    '#options' => questions_field_components_options(),
    '#weight' => 3,
    '#default_value' => 'textfield',
  );
  $form['add']['required'] = array(
    '#type' => 'checkbox',
  );

  $form['add']['weight'] = array(
    '#type' => 'textfield',
    '#size' => 4,
    '#delta' => count($components) > 10 ? count($components) : 10,
  );

    if (isset($_GET['cid']) && isset($components[$_GET['cid']])) {
    // Make the new component appear by default directly after the one that was
    // just added.
    $form['add']['weight']['#default_value'] = $components[$_GET['cid']]['weight'] + 1;
    foreach (array_keys($components) as $cid) {
      // Adjust all later components also, to make sure none of them have the
      // same weight as the new component.
      if ($form['components'][$cid]['weight']['#default_value'] >= $form['add']['weight']['#default_value']) {
        $form['components'][$cid]['weight']['#default_value']++;
      }
    }
  }
  else {
    // If no component was just added, the new component should appear by
    // default at the end of the list.
    $form['add']['weight']['#default_value'] = isset($max_weight) ? $max_weight + 1 : 0;
  }


  $form['add']['add'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
    '#weight' => 45,
    '#validate' => array('questions_field_form_add_validate', 'questions_field_manage_components_form_validate'),
    '#submit' => array('questions_field_form_add_submit'),
  );

  $form['actions'] = array(
    '#type' => 'actions',
    '#weight' => 45,
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#access' =>  !empty($components)
  );

  $form['warning'] = array(
    '#weight' => -1,
  );
//  questions_field_input_vars_check($form, $form_state, 'components', 'warning');

  return $form;

}

function questions_field_manage_components_form_validate(&$form, &$form_state){
}

/**
 * Submit handler for questions_field_manage_components_form() to save component order.
 */
function questions_field_manage_components_form_submit(&$form, &$form_state){
  list($entity_type, $bundle_name, $field_name, $components) = $form_state['build_info']['args'];

  // Update all required and weight values.
  $changes = FALSE;
  foreach ($components as $cid => $component) {
    if ($component['weight'] != $form_state['values']['components'][$cid]['weight']
            || $component['required'] != $form_state['values']['components'][$cid]['required']) {
      $changes = TRUE;
      $components[$cid]['weight'] = $form_state['values']['components'][$cid]['weight'];
      $components[$cid]['required'] = $form_state['values']['components'][$cid]['required'];
    }
  }

  if ($changes) {
      questions_field_component_multiple_save($components, $entity_type, $bundle_name, $field_name);
  }

  drupal_set_message(t('The component positions and required values have been updated.'));
}

function questions_field_form_add_validate(&$form, &$form_state){
    // Check that the entered field name is valid.
  if (drupal_strlen(trim($form_state['values']['add']['name'])) <= 0) {
    form_error($form['add']['name'], t('When adding a new field, the name field is required.'));
  }
  //check if the name already in use
   $form_key = _questions_field_safe_name($form_state['values']['add']['name']);
   if(questions_field_form_key_exist($form_key, $form_state['values']['add'], $form_state)){
    form_error($form['add']['name'], t('This name is already in use. It must be unique.'));
   }
}
function questions_field_form_add_submit(&$form, &$form_state){

  $component = $form_state['values']['add'];

  // Set the values in the query string for the add component page.
  $query = array(
    'name' => $component['name'],
    'required' => $component['required'],
    'weight' => $component['weight'],
  );

  // Forward the "destination" query string value to the next form.
  if (isset($_GET['destination'])) {
    $query['destination'] = $_GET['destination'];
    unset($_GET['destination']);
    drupal_static_reset('drupal_get_destination');
  }
  $form_state['redirect'] = array('/'.current_path().'/add/' . $component['type'], array('query' => $query));

}

/**
 * Preprocess variables for theming the questions field components form.
 */
function template_preprocess_questions_field_manage_components_form(&$variables) {
  $form = $variables['form'];
  $components = $variables['form']['#components'];

  // TODO: Attach these. See http://drupal.org/node/732022.
  drupal_add_tabledrag('questions-field-manage-components', 'order', 'sibling', 'questions-field-weight');


  $header = array(t('Label'), t('Type'), t('Value'), t('Required'), t('Weight'), array('data' => t('Operations'), 'colspan' => 3));
  $rows = array();

  // Add a row containing form elements for a new item.
  unset($form['add']['name']['#title'], $form['add_type']['#description']);
  $form['add']['name']['#attributes']['placeholder'] = t('New question name');
  $form['add']['cid']['#attributes']['class'][] = 'questions-field-cid';
  $form['add']['weight']['#attributes']['class'][] = 'questions-field-weight';
  $row_data = array(
    array('data' => drupal_render($form['add']['name']), 'class' => array('questions-field-component-name')),
    array('data' => drupal_render($form['add']['type']), 'class' => array('questions-field-component-type')),
    array('data' => '', 'class' => array('questions-field-component-value')),
    array('data' => drupal_render($form['add']['required']), 'class' => array('questions-field-component-required', 'checkbox')),
    array('data' => drupal_render($form['add']['cid']) . drupal_render($form['add']['weight'])),
    array('colspan' => 3, 'data' => drupal_render($form['add']['add']), 'class' => array('questions-field-component-add')),
  );
  $add_form = array('data' => $row_data, 'class' => array('draggable', 'questions-field-add-form', 'tabledrag-leaf'));

  if (!empty($components)) {
    // Build the table rows recursively.
    foreach ($components as $cid => $component) {
      _questions_field_components_form_rows($cid, $component, $form, $rows);
    }
  }
  else {
    $rows[] = array(array('data' => t('No Questions, add a question below.'), 'colspan' => 9));
  }

  // Append the add form if not already printed.
  if ($add_form) {
    $rows[] = $add_form;
  }

  $variables['rows'] = $rows;
  $variables['header'] = $header;
  $variables['form'] = $form;
}



/**
 * Recursive function for nesting components into a table.
 *
 * @see preprocess_questions_field_components_form()
 */
function _questions_field_components_form_rows($cid, $component, &$form, &$rows) {
  // Create presentable values.
  if (drupal_strlen($component['value']) > 30) {
    $component['value'] = drupal_substr($component['value'], 0, 30);
    $component['value'] .= '...';
  }
  $component['value'] = check_plain($component['value']);

  // Remove individual titles from the required and weight fields.
  unset($form['components'][$cid]['required']['#title']);
  unset($form['components'][$cid]['weight']['#title']);

  // Add special classes for weight and parent fields.
  $form['components'][$cid]['cid']['#attributes']['class'] = array('questions-field-cid');
  $form['components'][$cid]['weight']['#attributes']['class'] = array('questions-field-weight');



  // Add each component to a table row.
  $row_data = array(
    array('data' => filter_xss($component['name']), 'class' => array('questions-field-component-name')),
    array('data' => $form['add']['type']['#options'][$component['type']], 'class' => array('questions-field-component-type')),
    array('data' => ($component['value'] == '') ? '-' : $component['value'], 'class' => array('questions-field-component-value')),
    array('data' => drupal_render($form['components'][$cid]['required']), 'class' => array('questions-field-component-required', 'checkbox')),
    array('data' => drupal_render($form['components'][$cid]['cid']) . drupal_render($form['components'][$cid]['weight'])),
    array('data' => l(t('Edit'), '/'.current_path().'/'.$cid.'/edit', array('query' => drupal_get_destination())), 'class' => array('questions-field-component-edit')),
    array('data' => l(t('Delete'), '/'.current_path().'/'.$cid.'/delete', array('query' => drupal_get_destination())), 'class' => array('questions-field-component-delete')),
  );
  $row_class = array('draggable');


  $rows[] = array('data' => $row_data, 'class' => $row_class, 'data-cid' => $cid);


}

/**
 * Theme the node components form. Use a table to organize the components.
 *
 * @return
 *   Formatted HTML form, ready for display.
 */
function theme_questions_field_manage_components_form($variables) {
  $output = '';
  $output .= drupal_render_children($variables['form']['warning']);
  $output .= theme('table', array('header' => $variables['header'], 'rows' => $variables['rows'], 'attributes' => array('id' => 'questions-field-manage-components')));
  $output .= drupal_render_children($variables['form']);
  return $output;
}



/**
 * Form to confirm deletion of a component.
 */
function questions_field_component_delete_form($form, &$form_state, $entity_type, $bundle_name, $field_name, $component) {

  $form['component'] = array(
    '#type' => 'value',
    '#value' => $component,
  );

   $question = t('Delete the %name component?', array('%name' => $component['name']));
   $description = t('This will immediately delete the %name component from the field %field. This cannot be undone.', array('%name' => $component['name'], '%field' => $field_name));

  return confirm_form($form, $question, "admin/structure/questions_field/$entity_type/$bundle_name/fields/$field_name", $description, t('Delete'));
}


/**
 * Submit handler for questions_field_component_delete_form().
 */
function questions_field_component_delete_form_submit($form, &$form_state) {
  list($entity_type, $bundle_name, $field_name) = $form_state['build_info']['args'];
  $component = $form_state['values']['component'];
  questions_field_component_delete($component, $entity_type, $bundle_name, $field_name);
  drupal_set_message(t('Component %name deleted.', array('%name' => $component['name'])));
  $form_state['redirect'] = "admin/structure/questions_field/$entity_type/$bundle_name/fields/$field_name";
}

/**
 * Form to configure a field.
 */
function questions_field_component_edit_form($form, $form_state, $entity_type, $bundle, $field_name, $component) {
  drupal_set_title(t('Edit field: @name', array('@name' => $component['name'])), PASS_THROUGH);

  $form['#tree'] = TRUE;

  // Print the correct field type specification.
  // We always need: name and description.
  $form['type'] = array(
    '#type' => 'value',
    '#value' => $component['type'],
  );

  $form['cid'] = array(
    '#type' => 'value',
    '#value' => isset($component['cid']) ? $component['cid'] : NULL,
  );

  if (questions_field_component_feature($component['type'], 'title')) {
    $form['name'] = array(
      '#type' => 'textfield',
      '#default_value' => $component['name'],
      '#title' => t('Label'),
      '#description' => t('This is used as a descriptive label when displaying this form element.'),
      '#required' => TRUE,
      '#weight' => -10,
      '#maxlength' => NULL,
    );
  }


  $form['form_key'] = array(
    '#type' => 'machine_name',
    '#default_value' => empty($component['form_key']) ? _questions_field_safe_name($component['name']) : $component['form_key'],
    '#title' => t('Machine name'),
    '#description' => t('Enter a machine readable key for this form element. May contain only alphanumeric characters and underscores. This key will be used as the name attribute of the form element. This value has no effect on the way data is saved, but may be helpful if doing custom form processing.'),
    '#required' => TRUE,
    '#weight' => -9,
    '#disabled' => !empty($component['form_key']),
    '#maxlength' => 32,
    '#machine_name' => array(
      'exists' => 'questions_field_form_key_exist',
    ),
  );

    $form['question'] = array(
      '#type' => 'textarea',
      '#default_value' => !empty($component['question']) ? $component['question'] : $component['name'].' : ',
      '#title' => t('Enter your question'),
      '#description' => t('This question appear above the field.') . ' ' . theme('questions_field_token_help'),
      '#required' => TRUE,
      '#weight' => -8,
    );

  $form['extra'] = array();
  if (questions_field_component_feature($component['type'], 'description')) {
    $form['extra']['description'] = array(
      '#type' => 'textarea',
      '#default_value' => isset($component['extra']['description']) ? $component['extra']['description'] : '',
      '#title' => t('Description'),
      '#description' => t('A short description of the field used as help for the user when he/she uses the form.') . ' ' . theme('questions_field_token_help'),
      '#weight' => -1,
    );
  }

  // Display settings.
  $form['display'] = array(
    '#type' => 'fieldset',
    '#title' => t('Display'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#weight' => 8,
  );
  if (questions_field_component_feature($component['type'], 'title_display')) {
    if (questions_field_component_feature($component['type'], 'question_before')) {
      $form['display']['title_display'] = array(
        '#type' => 'select',
        '#title' => t('Question display'),
        '#default_value' => !empty($component['extra']['title_display']) ? $component['extra']['title_display'] : 'before',
        '#options' => array(
          'before' => t('Above'),
          'inline' => t('Inline'),
          'none' => t('None'),
        ),
        '#description' => t('Determines the placement of the component\'s question.'),
      );
    }
    else {
      $form['display']['title_display'] = array(
        '#type' => 'checkbox',
        '#title' => t('Hide label'),
        '#default_value' => strcmp($component['extra']['title_display'], 'none') === 0,
        '#return_value' => 'none',
        '#description' => t('Do not display the label of this component.'),
      );
    }
    $form['display']['title_display']['#weight'] = 8;
    $form['display']['title_display']['#parents'] = array('extra', 'title_display');
  }

  if (questions_field_component_feature($component['type'], 'description')) {
    $form['display']['description_above'] = array(
      '#type' => 'checkbox',
      '#default_value' => !empty($component['extra']['description_above']),
      '#title' => t('Description above field'),
      '#description' => t('Place the description above &mdash; rather than below &mdash; the field.'),
      '#weight' => 8.5,
      '#parents' => array('extra', 'description_above'),
    );
  }

  if (questions_field_component_feature($component['type'], 'readonly')) {
    $form['display']['readonly'] = array(
      '#type' => 'checkbox',
      '#default_value' => !empty($component['extra']['readonly']),
      '#title' => t('Add readonly attribute'),
      '#description' => t('The readonly attribute can be set to keep a user from changing the value until some other conditions have been met (like selecting a checkbox, etc.). Then, a JavaScript can remove the readonly value, and make the input field editable'),
      '#weight' => 8.6,
      '#parents' => array('extra', 'readonly'),
    );
  }

  if (questions_field_component_feature($component['type'], 'wrapper_classes')) {
    $form['display']['wrapper_classes'] = array(
      '#type' => 'textfield',
      '#title' => t('Wrapper CSS classes'),
      '#default_value' => isset($component['extra']['wrapper_classes']) ? $component['extra']['wrapper_classes'] : '',
      '#description' => t('Apply a class to the wrapper around both the field and its label. Separate multiple by spaces.'),
      '#weight' => 50,
      '#parents' => array('extra', 'wrapper_classes'),
    );
  }
  if (questions_field_component_feature($component['type'], 'css_classes')) {
    $form['display']['css_classes'] = array(
      '#type' => 'textfield',
      '#title' => t('CSS classes'),
      '#default_value' => isset($component['extra']['css_classes']) ? $component['extra']['css_classes'] : '',
      '#description' => t('Apply a class to the field. Separate multiple by spaces.'),
      '#weight' => 51,
      '#parents' => array('extra', 'css_classes'),
    );
  }

  // Validation settings.
  $form['validation'] = array(
    '#type' => 'fieldset',
    '#title' => t('Validation'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#weight' => 5,
  );
  if (questions_field_component_feature($component['type'], 'required')) {
    $form['validation']['required'] = array(
      '#type' => 'checkbox',
      '#title' => t('Required'),
      '#default_value' => ($component['required'] == '1' ? TRUE : FALSE),
      '#description' => t('Check this option if the user must enter a value.'),
      '#weight' => -1,
      '#parents' => array('required'),
    );
  }


  // Position settings, only shown if JavaScript is disabled.
  $form['position'] = array(
    '#type' => 'fieldset',
    '#title' => t('Position'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#tree' => FALSE,
    '#weight' => 20,
    '#attributes' => array('class' => array('questions-field-position')),
  );


  $form['position']['weight'] = array(
    '#type' => 'textfield',
    '#size' => 4,
    '#title' => t('Weight'),
    '#default_value' => $component['weight'],
    '#description' => t('Optional. In the menu, the heavier items will sink and the lighter items will be positioned nearer the top.'),
    '#weight' => 4,
  );

  // Add the fields specific to this component type:
  $additional_form_elements = (array) questions_field_component_invoke($component['type'], 'edit', $component, $form, $form_state, $entity_type, $bundle, $field_name, $component);
  if (empty($additional_form_elements)) {
    drupal_set_message(t('The Questions field component of type @type does not have an edit function defined.', array('@type' => $component['type'])));
  }


  // Merge the additional fields with the current fields:
  if (isset($additional_form_elements['extra'])) {
    $form['extra'] = array_merge($form['extra'], $additional_form_elements['extra']);
    unset($additional_form_elements['extra']);
  }
  if (isset($additional_form_elements['position'])) {
    $form['position'] = array_merge($form['position'], $additional_form_elements['position']);
    unset($additional_form_elements['position']);
  }
  if (isset($additional_form_elements['display'])) {
    $form['display'] = array_merge($form['display'], $additional_form_elements['display']);
    unset($additional_form_elements['display']);
  }
  if (isset($additional_form_elements['validation'])) {
    $form['validation'] = array_merge($form['validation'], $additional_form_elements['validation']);
    unset($additional_form_elements['validation']);
  }
  elseif (count(element_children($form['validation'])) == 0) {
    unset($form['validation']);
  }
  $form = array_merge($form, $additional_form_elements);


  // Add the submit button.
  $form['actions'] = array(
    '#type' => 'actions',
    '#weight' => 50,
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save component'),
  );

  // Remove fieldsets without any child form controls.
  foreach (element_children($form) as $group_key) {
    $group = $form[$group_key];
    if (isset($group['#type']) && $group['#type'] === 'fieldset' && !element_children($group)) {
      unset($form[$group_key]);
    }
  }
  $form['#submit'][] = 'questions_field_component_edit_form_submit';
  return $form;
}

/**
 * Field name validation for the questions field unique key. Must be alphanumeric.
 */
function questions_field_component_edit_form_validate($form, &$form_state) {


  if (!preg_match('/^[a-z0-9_]+$/i', $form_state['values']['form_key'])) {
    form_set_error('form_key', t('The field key %field_key is invalid. Please include only lowercase alphanumeric characters and underscores.', array('%field_key' => $form_state['values']['form_key'])));
  }

//  foreach ($node->questions_field['components'] as $cid => $component) {
//    if (($component['cid'] != $form_state['values']['cid'] && strcasecmp($component['form_key'], $form_state['values']['form_key']) == 0)) {
//      form_set_error('form_key', t('The field key %field_key is already in use by the field labeled %existing_field. Please use a unique key.', array('%field_key' => $form_state['values']['form_key'], '%existing_field' => $component['name'])));
//    }
//  }
}

/**
 * Submit handler for questions_field_component_edit_form().
 */
function questions_field_component_edit_form_submit($form, &$form_state) {
 list($entity_type, $bundle_name, $field_name) = $form_state['build_info']['args'];

  // Remove extra values that match the default.
  if (isset($form_state['values']['extra'])) {
    $default = array('type' => $form_state['values']['type'], 'extra' => array());
    questions_field_component_defaults($default);

  
    foreach ($form_state['values']['extra'] as $key => $value) {
      if (isset($default['extra'][$key]) && $default['extra'][$key] === $value) {
        unset($form_state['values']['extra'][$key]);
      }
    }
  }

  // Remove empty attribute values.
  if (isset($form_state['values']['extra']['attributes'])) {
    foreach ($form_state['values']['extra']['attributes'] as $key => $value) {
      if ($value === '') {
        unset($form_state['values']['extra']['attributes'][$key]);
      }
    }
  }
   questions_field_component_save($form_state['values'], $entity_type, $bundle_name, $field_name);
//    drupal_set_message(t('New component %name added.', array('%name' => $form_state['values']['name'])));

 $form_state['redirect'] = "admin/structure/questions_field/$entity_type/$bundle_name/fields/$field_name";
}





/**
 * Check if a field has a particular feature.
 *
 */
function questions_field_component_feature($type, $feature) {
  $fields = questions_field_components();
  $defaults = array(
    'analysis' => TRUE,
    'csv' => TRUE,
    'default_value' => TRUE,
    'description' => TRUE,
    'email' => TRUE,
    'email_address' => FALSE,
    'email_name' => FALSE,
    'required' => TRUE,
    'title' => TRUE,
    'title_display' => TRUE,
    'question_before' => TRUE,
    'conditional' => TRUE,
    'conditional_action_set' => FALSE,
    'spam_analysis' => FALSE,
    'group' => FALSE,
    'attachment' => FALSE,
    'placeholder' => FALSE,
    'readonly' => TRUE,
    'wrapper_classes' => TRUE,
    'css_classes' => TRUE,
    'views_range' => FALSE,
  );
  return isset($fields[$type]['features'][$feature]) ? $fields[$type]['features'][$feature] : !empty($defaults[$feature]);
}


/**
 * Validate an element value is unique with no duplicates in the database.
 */
function questions_field_validate_unique($element, $form_state) {
  // if ($element['#value'] !== '') {
  //   $nid = $form_state['values']['details']['nid'];
  //   $sid = $form_state['values']['details']['sid'];
  //   $query = db_select('questions_field_submitted_data')
  //     ->fields('questions_field_submitted_data', array('sid'))
  //     ->condition('nid', $nid)
  //     ->condition('cid', $element['#questions_field_component']['cid'])
  //     ->condition('data', $element['#value'])
  //     // More efficient than using countQuery() for data checks.
  //     ->range(0, 1);
  //   if ($sid) {
  //     $query->condition('sid', $sid, '<>');
  //   }
  //   $count = $query->execute()->fetchField();
  //   if ($count) {
  //     form_error($element, t('The value %value has already been submitted once for the %title field. You may have already submitted this form, or you need to use a different value.', array('%value' => $element['#value'], '%title' => $element['#title'])));
  //   }
  // }
}
