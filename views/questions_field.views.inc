<?php

/**
 * Implements hook_field_views_data().
 */
function questions_field_field_views_data($field) {
  $data = field_views_field_default_views_data($field);
  $components = questions_field_field_components_load($field);
  if(!empty($components)){
//   Add a filter handler for select component.
  $key = key($field['storage']['details']);
  foreach ($field['storage']['details'][$key] as $type => $tables) {
    foreach ($tables as $table_name => $columns) {

      foreach($columns as $cid => $column_real_name){

    if (isset($data[$table_name][$column_real_name]['filter'])) {
     if(in_array($components[$cid]['type'], array('select', 'email'))){
        $data[$table_name][$column_real_name]['filter']['handler']
              = 'questions_field_views_handler_filter_' . $components[$cid]['type'];
        }
       }
      }
     }
    }

    foreach($components as $cid => $component){
      //$data passed by reference
      $data_returned = questions_field_component_invoke($component['type'], 'field_views_data', $data, $field, $component);
     if(!empty($data_returned)){
      $data = $data_returned;
     }
    }
  }
  return $data;
}


/**
 * Implements hook_field_views_data_views_data_alter().
 */
function questions_field_field_views_data_views_data_alter(&$data) {
  
   foreach(field_info_fields() as $field_name  => $field){

    $components = questions_field_field_components_load($field);

    if(!empty($components)){
      foreach($components as $cid => $component){
        //$data passed by reference
        $data_returned = questions_field_component_invoke($component['type'], 'field_views_data_views_data_alter', $data, $field, $component);
       if(!empty($data_returned)){
        $data = $data_returned;
       }
      }
    }

   }


  }
