<?php

/**
 * @file
 * Definition of views_handler_relationship.
 */

/**
 * @defgroup views_relationship_handlers Views relationship handlers
 * @{
 * Handlers to tell Views how to create alternate relationships.
 */

/**
 * Relationship handler, allows a new version of the primary table to be linked.
 *
 * The base relationship handler can only handle a single join. Some
 * relationships are more complex and might require chains of joins; for those,
 * you must use a custom relationship handler.
 *
 * Definition items:
 * - base: The new base table this relationship will be adding. This does not
 *   have to be a declared base table, but if there are no tables that
 *   utilize this base table, it won't be very effective.
 * - base field: The field to use in the relationship; if left out this will be
 *   assumed to be the primary field.
 * - relationship table: The actual table this relationship operates against.
 *   This is analogous to using a 'table' override.
 * - relationship field: The actual field this relationship operates against.
 *   This is analogous to using a 'real field' override.
 * - label: The default label to provide for this relationship, which is
 *   shown in parentheses next to any field/sort/filter/argument that uses
 *   the relationship.
 *
 * @ingroup views_relationship_handlers
 */
class questions_field_views_handler_relationship extends views_handler_relationship {

  /**
   * Let relationships live on tables other than the table they operate on.
   */
  public function init(&$view, &$options) {
    parent::init($view, $options);
  }

  /**
   * Get this field's label.
   */
  public function label() {
    parent::label();
  }

  /**
   * {@inheritdoc}
   */
  public function option_definition() {
    $options = parent::option_definition();
 
    return $options;
  }

  /**
   * Provide the label widget that all fields should have.
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
  }

 /**
   * Called to implement a relationship in a query.
   */
  public function query() {
  
    
    parent::query();
    // $table_data = views_fetch_data($this->definition['base']);
    // $field_name = $this->definition['arguments']['field_name'];
    // $component_id =$this->definition['arguments']['component_id'];

    // $field = field_info_field($field_name );
    // $field_component_info = questions_field_field_components_load($field, array($component_id));
    // $field_component_info = $field_component_info[$component_id];

    // if(!empty($field_component_info) 
    //          and $field_component_info['extra']['multiple']){
    //     $this->query->add_Tag(__CLASS__);
    //     $data = &$GLOBALS[__CLASS__];
    //     $data['table_alias'] = $this->table_alias;
    //     $data['real_field'] = $this->real_field;
    // }
  }

  /**
   * You can't groupby a relationship.
   */
  public function use_group_by() {
    $use_group_by = parent::use_group_by();
    return $use_group_by;
  }

}

/**
 * A special handler to take the place of missing or broken handlers.
 *
 * @ingroup views_relationship_handlers
 */
class questions_field_views_handler_relationship_broken extends questions_field_views_handler_relationship {

  /**
   * {@inheritdoc}
   */
  public function ui_name($short = FALSE) {
    return t('Broken/missing handler');
  }

  /**
   * {@inheritdoc}
   */
  public function ensure_my_table() {

    // No table to ensure!
  }

  /**
   * {@inheritdoc}
   */
  public function query() {

    // No query to run.
  }

  /**
   * {@inheritdoc}
   */
  public function options_form(&$form, &$form_state) {
    $form['markup'] = array(
      '#markup' => '<div class="form-item description">' . t('The handler for this item is broken or missing and cannot be used. If a module provided the handler and was disabled, re-enabling the module may restore it. Otherwise, you should probably delete this item.') . '</div>',
    );
  }

  /**
   * {@inheritdoc}
   */
  public function broken() {
    return TRUE;
  }

}

/**
 * @}
 */