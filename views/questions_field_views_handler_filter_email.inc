<?php

class questions_field_views_handler_filter_email extends views_handler_filter_string {


  function operators() {
    $operators = parent::operators();
    unset($operators['=']);
    unset($operators['!=']);

    $operators = array(
      'FIND_IN_SET' => array(
        'title' => t('Is equal to'),
        'short' => t('='),
        'method' => 'op_simple',
        'values' => 1,
      ),
      'NOT FIND_IN_SET' => array(
        'title' => t('Is not equal to'),
        'short' => t('!='),
        'method' => 'op_simple',
        'values' => 1,
      ),
    )+ $operators;

    return $operators;
  }


  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['operator']['#default_value'] = 'FIND_IN_SET';
  }

  function op_simple() {
    if (empty($this->value)) {
      return;
    }

    $this->ensure_my_table();
    $op = ($this->operator == 'NOT FIND_IN_SET') ? ' AND ' : ' OR ';
    $column_name = "$this->table_alias.$this->real_field";
    $expression = "$this->operator('$this->value', $column_name)";
    $this->query->add_where_expression($this->options['group'], '( ' . $expression . ' )');
 }

}
