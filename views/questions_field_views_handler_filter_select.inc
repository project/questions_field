<?php

class questions_field_views_handler_filter_select extends views_handler_filter_in_operator {

  function get_value_options() {
        $field = field_info_field($this->definition['field_name']);
        $cid = str_replace($field['field_name'] . '_', '', $this->field);
        $components = questions_field_field_components_load($field, array($cid));
        $component = $components[$cid];
        questions_field_component_defaults($component);
        $this->value_title = t($component['name']);

         module_load_include('inc', 'questions_field', 'components/select');
        $options = _questions_field_select_options($component, FALSE, FALSE);
        $this->value_options = $options;
    }


  function operators() {
    $operators = parent::operators();
    unset($operators['in']);
    unset($operators['not in']);

    $operators = array(
      'FIND_IN_SET' => array(
        'title' => t('Is one of'),
        'short' => t('FIND_IN_SET'),
        'short_single' => t('='),
        'method' => 'op_simple',
        'values' => 1,
      ),
      'NOT FIND_IN_SET' => array(
        'title' => t('Is not one of'),
        'short' => t('NOT FIND_IN_SET'),
        'short_single' => t('<>'),
        'method' => 'op_simple',
        'values' => 1,
      ),
    )+ $operators;
    return $operators;
  }


  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['operator']['#default_value'] = 'FIND_IN_SET';
  }

  function op_simple() {
    if (empty($this->value)) {
      return;
    }

    $this->ensure_my_table();
    $op = ($this->operator == 'NOT FIND_IN_SET') ? ' AND ' : ' OR ';
    $values = array_values($this->value);
    $expression = array();
    $column_name = "$this->table_alias.$this->real_field";
    foreach($values as  $value){
      $expression[] = "$this->operator('$value', $column_name)";
   }
    $this->query->add_where_expression($this->options['group'], '( '.implode($op, $expression).' )');
 }
}
