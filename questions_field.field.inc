<?php


/**
 * Implements hook_field_info().
 * @see https://www.drupal.org/node/1021466
 * @see https://www.drupal.org/node/1156554
 * @see http://www.drupalcontrib.org/api/drupal/contributions!entity!includes!entity.wrapper.inc/function/EntityMetadataWrapper::optionsList/7
 */

function questions_field_field_info()
	{
	return array(
		'questions' => array(
			'label' => t('Questions') ,
			'description' => t('Questions field') ,
			'default_widget' => 'questions_default',
			'default_formatter' => 'questions_answers_default',
			'property_type' => 'questions',
			'property_callbacks' => array(
				'questions_field_property_info_callback'
			) ,
		) ,
	);
	}



	/**
 * Implements hook_field_formatter_info().
 */

function questions_field_field_formatter_info()
{

return array(
	'questions_answers_default' => array(
		'label' => t('Default') ,
		'field types' => array(
			'questions'
		) ,
	) ,
	'questions_answers_selected' => array(
		'label' => t('Questions answers selected') ,
		'field types' => array(
			'questions'
		) ,
//@TODO: get settings from component using hook
		'settings' => array(
			'questions_selected' => null
		) ,
	) ,
);
}

/**
* Implements hook_field_formatter_settings_summary().
*/

function questions_field_field_formatter_settings_summary($field, $instance, $view_mode)
{
$display = $instance['display'][$view_mode];
$settings = $display['settings'];
$summary = '';
switch ($display['type'])
	{
case 'questions_answers_selected':
	$summary = array();
	$components_title = questions_field_format_plugins_options($field);
	if (!empty($settings['questions_selected']))
		{
		foreach($settings['questions_selected'] as $cid => $setting)
			{
			if (isset($components_title[$cid]) && $setting['show'])
				{
				$summary[$setting['weight']] = $components_title[$cid];
				}
			}

		ksort($summary);
		}

	$summary = ($summary) ? implode(', ', $summary) : t('No questions selected');
	break;
	}

return $summary;
}

/**
* Implements hook_field_formatter_settings_form().
*/

function questions_field_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state)
{
$display = $instance['display'][$view_mode];
$settings = $display['settings'];
$element = array();

switch ($display['type'])
	{
case 'questions_answers_selected':
	$components_title = questions_field_format_plugins_options($field);

	if ($components_title)
		{
		$field_name = $field['field_name'];
		$delta = 0;
		foreach($components_title as $cid => $component_name)
			{
			$element['questions_selected'][$cid]['show'] = array(
				'#type' => 'checkbox',
				'#default_value' => !empty($settings['questions_selected'][$cid]['show']) ? $settings['questions_selected'][$cid]['show'] : 0,
			);
			$element['questions_selected'][$cid]['name'] = array(
				'#markup' => $component_name
			);
			$element['questions_selected'][$cid]['weight'] = array(
				'#type' => 'textfield',
				'#size' => 4,
				'#title' => t('Weight') ,
				'#default_value' => !empty($settings['questions_selected'][$cid]['weight']) ? $settings['questions_selected'][$cid]['weight'] : $delta,
			);
			$field_component_info = questions_field_field_components_load($field, array(
				$cid
			));
			$field_component_info = $field_component_info[$cid];
//@TODO: remove conditions and it from component
	if($field_component_info['type'] == 'email'){

	  $format_options = array(
		'email_default' => t('Default email link'),
		'email_plain' =>t('Email plain text')
	  );

	  $settings_email = $settings['questions_selected'][$cid]['email_settings'];
	  $element['questions_selected'][$cid]['email_settings'] = array(
			'#type' => 'fieldset',
			'#title' => t('Format') ,
			'#collapsible' => TRUE,
			'#collapsed' => TRUE,
			'#weight' => 4,
		  );

		$element['questions_selected'][$cid]['email_settings']['email_format'] = array(
		  '#title' => t('Email format'),
		  '#type' => 'select',
		  '#default_value' => $settings_email['email_format'],
		  '#options' => $format_options,
		);

	}

	if($field_component_info['type'] == 'term_reference'){
		
		$list_options = render_term_reference_items_list_options();

		 $element['questions_selected'][$cid]['term_reference_render_settings'] = array(
		   '#title' => t('Render'),
		   '#type' => 'select',
		   '#description' => t('Select how to display each item.'),
		   '#default_value' => $settings['questions_selected'][$cid]['term_reference_render_settings'],
		   '#options' => $list_options,
		 );


	   }

	if($field_component_info['type'] == 'nodereference'){
		
		$list_options = render_nodereference_items_list_options();

		 $element['questions_selected'][$cid]['nodereference_render_settings'] = array(
		   '#title' => t('Render'),
		   '#type' => 'select',
		   '#description' => t('Select how to display each item.'),
		   '#default_value' => $settings['questions_selected'][$cid]['nodereference_render_settings'],
		   '#options' => $list_options,
		 );


	   }

	if($field_component_info['type'] == 'revisionreference'){
		
	 $list_options = render_items_list_options();

	  $element['questions_selected'][$cid]['revisionreference_render_settings'] = array(
		'#title' => t('Render'),
		'#type' => 'select',
		'#description' => t('Select how to display each item.'),
		'#default_value' => $settings['questions_selected'][$cid]['revisionreference_render_settings'],
		'#options' => $list_options,
	  );


	}
	if($field_component_info['type'] == 'image'){

		  $image_styles = image_style_options(FALSE, PASS_THROUGH);
		  $settings_image_style = $settings['questions_selected'][$cid]['image_style_settings'];
		  $element['questions_selected'][$cid]['image_style_settings'] = array(
			'#type' => 'fieldset',
			'#title' => t('Image style') ,
			'#collapsible' => TRUE,
			'#collapsed' => TRUE,
			'#weight' => 4,
		  );


		$element['questions_selected'][$cid]['image_style_settings']['image_style'] = array(
		  '#title' => t('Image style'),
		  '#type' => 'select',
		  '#default_value' => $settings_image_style['image_style'],
		  '#empty_option' => t('None (original image)'),
		  '#options' => $image_styles,
		);

		$link_types = array(
		  'content' => t('Content'),
		  'file' => t('File'),
		);
		$element['questions_selected'][$cid]['image_style_settings']['image_link'] = array(
		  '#title' => t('Link image to'),
		  '#type' => 'select',
		  '#default_value' => $settings_image_style['image_link'],
		  '#empty_option' => t('Nothing'),
		  '#options' => $link_types,
		);

	}


	if (!empty($field_component_info['extra']['multiple']))
				{
				$element['questions_selected'][$cid]['multiple_field_settings'] = array(
					'#type' => 'fieldset',
					'#title' => t('Manage multiple values display') ,
					'#collapsible' => TRUE,
					'#collapsed' => TRUE,
					'#weight' => 5,
				);
				$settings_prefix = $settings['questions_selected'][$cid]['multiple_field_settings'];

				// Make the string translatable by keeping it as a whole rather than
				// translating prefix and suffix separately.

				list($prefix, $suffix) = explode('@count', t('Display @count value(s)'));
				$type = 'textfield';
				$options = NULL;
				$size = 5;
				$element['questions_selected'][$cid]['multiple_field_settings']['multi_type'] = array(
					'#type' => 'radios',
					'#title' => t('Display type') ,
					'#options' => array(
						'ul' => t('Unordered list') ,
						'ol' => t('Ordered list') ,
						'separator' => t('Simple separator') ,
					) ,
					'#default_value' => !empty($settings_prefix['multi_type']) ? $settings_prefix['multi_type'] : 'separator',
				);
				$element['questions_selected'][$cid]['multiple_field_settings']['separator'] = array(
					'#type' => 'textfield',
					'#title' => t('Separator') ,
					'#default_value' => !empty($settings_prefix['separator']) ? $settings_prefix['separator'] : ', ',
					'#states' => array(
						'visible' => array(
							':input[name$="[questions_selected][' . $cid . '][multiple_field_settings][multi_type]"]' => array(
								'value' => 'separator'
							) ,
						) ,
					) ,
				);
				$element['questions_selected'][$cid]['multiple_field_settings']['delta_limit'] = array(
					'#type' => $type,
					'#size' => $size,
					'#field_prefix' => $prefix,
					'#field_suffix' => $suffix,
					'#options' => $options,
					'#default_value' => !empty($settings_prefix['delta_limit']) ? $settings_prefix['delta_limit'] : 'all',
					'#prefix' => '<div class="container-inline">',
				);
				list($prefix, $suffix) = explode('@count', t('starting from @count'));
				$element['questions_selected'][$cid]['multiple_field_settings']['delta_offset'] = array(
					'#type' => 'textfield',
					'#size' => 5,
					'#field_prefix' => $prefix,
					'#field_suffix' => $suffix,
					'#default_value' => !empty($settings_prefix['delta_offset']) ? $settings_prefix['delta_offset'] : 0,
					'#description' => t('(first item is 0)') ,
				);
				$element['questions_selected'][$cid]['multiple_field_settings']['delta_reversed'] = array(
					'#title' => t('Reversed') ,
					'#type' => 'checkbox',
					'#default_value' => !empty($settings_prefix['delta_reversed']) ? $settings_prefix['delta_reversed'] : null,
					'#suffix' => $suffix,
					'#description' => t('(start from last values)') ,
				);
				$element['questions_selected'][$cid]['multiple_field_settings']['delta_first_last'] = array(
					'#title' => t('First and last only') ,
					'#type' => 'checkbox',
					'#default_value' => !empty($settings_prefix['delta_first_last']) ? $settings_prefix['delta_first_last'] : null,
					'#suffix' => '</div>',
				);
				}

			$delta++;
			}

		$element['#theme'][] = 'questions_field_manage_display';
		}

	break;
	}
return $element;
}


/**
 * Implements hook_field_formatter_view()
 */

function questions_field_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display)
	{
	$element = array();
  $arguments = array(
         $entity_type, $entity, $field, $instance, $langcode, $items, $display
			);
			 
	switch ($display['type'])
		{
	case 'questions_answers_default':
		$components = _questions_field_get_component_from_field($field, $instance);
		foreach($items as $delta => $item)
			{
			foreach($item as $cid => $value)
				{
				if (!empty($value))
					{
					$component = $components[$cid];
					questions_field_component_defaults($component);
					$component['display_type'] = $display['type'];
					$element[$delta][$cid] = questions_field_component_invoke($component['type'], 'display', $component, $value, 0,  'html',  $arguments);
					}
				}
			}

		break;

	case 'questions_answers_selected':
		$components = _questions_field_get_component_from_field($field, $instance);
		$questions_selected = !empty($display['settings']['questions_selected']) ? $display['settings']['questions_selected'] : array();
		foreach($items as $delta => $item)
			{
				 
			foreach($questions_selected as $cid => $setting)
				{   if(!isset($components[$cid])) continue;
					$component = $components[$cid];
					$component['display_type'] = $display['type'];
				
					questions_field_component_defaults($component);
					if($component['type'] == 'nodereference'){
						$component['display_settings']['render'] = $questions_selected[$cid]['nodereference_render_settings'];
					  }
					if($component['type'] == 'revisionreference'){
						$component['display_settings']['render'] = $questions_selected[$cid]['revisionreference_render_settings'];
						}
						if($component['type'] == 'term_reference'){
							$component['display_settings']['render'] = $questions_selected[$cid]['term_reference_render_settings'];
							}
					 if($component['type'] == 'email'){
		   
						$component['email_settings']['email_format'] = $questions_selected[$cid]['email_settings']['email_format'];
					 }
					 if($component['type'] == 'image'){
		   
					   $component['image_style_settings']['image_style'] = $questions_selected[$cid]['image_style_settings']['image_style'];
					   $component['image_style_settings']['image_link'] = $questions_selected[$cid]['image_style_settings']['image_link'];
					   if(empty($item[$cid])) $item[$cid] = $component['extra']['default_image'];
					}
 
				if (!empty($item[$cid]) && $setting['show'])
					{

        	if (!empty($component['extra']['multiple']))
						{
						$multiple_settings = array(
							'delta_limit' => $questions_selected[$cid]['multiple_field_settings']['delta_limit'],
							'delta_offset' => $questions_selected[$cid]['multiple_field_settings']['delta_offset'],
							'delta_reversed' => $questions_selected[$cid]['multiple_field_settings']['delta_reversed'],
							'delta_first_last' => $questions_selected[$cid]['multiple_field_settings']['delta_first_last'],
							'multi_type' => $questions_selected[$cid]['multiple_field_settings']['multi_type'],
							'separator' => $questions_selected[$cid]['multiple_field_settings']['separator'],
						);
						$component['multiple_settings'] = $multiple_settings;
						}
 
					$element[$delta][$cid] = questions_field_component_invoke($component['type'], 'display', $component, $item[$cid], $setting['weight'], 'html', $arguments);
					}
				}
			}

		break;
		}

	return $element;
	}

/**
 * Implements hook_field_widget_info().
 */

function questions_field_field_widget_info()
	{
	return array(
		'questions_default' => array(
			'label' => t('Default') ,
			'field types' => array(
				'questions'
			) ,
		) ,
	);
	}



/*
* hook_field_delete_instance()
*/

function questions_field_field_delete_instance($instance)
{
$field = field_info_field($instance['field_name']);
if ($field['type'] == 'questions')
	{

	// Remove tab manage Questions field

	menu_rebuild();
	}
}

/**
* Implements hook_field_widget_form().
*/

function questions_field_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element)
{
$element+= array(
	'#type' => 'fieldset'
);
$entity = isset($form['#entity']) ? $form['#entity'] : NULL;
$text_manage_questions_link = 'Manage questions';
$weight_manage_questions_link = 0;
$components_settings = _questions_field_get_component_from_field($field, $instance);

if ($components_settings)
	{
	foreach($components_settings as $column_name => $component_setting)
		{
		questions_field_component_defaults($component_setting);

		// remove required property if opened in page field settings

		if (empty($entity)) $component_setting['required'] = 0;
		if ($component_setting['weight'] > $weight_manage_questions_link) $weight_manage_questions_link = $component_setting['weight'];
		$arguments = array(
			$form,
			$form_state,
			$field,
			$instance,
			$langcode,
			$items,
			$delta,
			$element
		);
		$element[$column_name] = questions_field_component_invoke($component_setting['type'], 'render', $component_setting, @$items[$delta][$column_name], $entity, module_exists('token') , $arguments);
		$element[$column_name]['#questions_field_component'] = $component_setting;
		}

	if (empty($form['#process']) || !in_array('questions_field_widget_form_process', $form['#process']))
		{
		$form['#process'][] = 'questions_field_widget_form_process';
		}
	}
  else
	{
	$text_manage_questions_link = 'Add first question';
	}

// Give the ability to manage questions in edit field

if (empty($entity))
	{
	$element['manage_questions'] = array(
		'#type' => 'link',
		'#title' => t($text_manage_questions_link) ,
		'#href' => 'admin/structure/questions_field/' . $instance['entity_type'] . '/' . $instance['bundle'] . '/fields/' . $instance['field_name'],
		'#options' => array(
			'query' => array() ,
			'attributes' => array(
				'title' => t('Add question.') ,
				'target' => '_blank'
			)
		) ,
		'#weight' => ($weight_manage_questions_link + 1)
	);
}
return $element;
}



/**
 * Implements hook_field_is_empty().
 */

function questions_field_field_is_empty($item, $field)
	{  
	if (!empty($field['settings']['questions']))
		{
		foreach($field['settings']['questions'] as $cid => $info)
			{  
			  if ( !_qf_is_empty($item[$cid]) ) return false;
			}
		}
 
	return true;
	}


	

 /**
 * Implements hook_field_delete().
 */

function questions_field_field_delete($entity_type, $entity, $field, $instance, $langcode, &$items)
{
list($id, $vid, $bundle) = entity_extract_ids($entity_type, $entity);
if ($field['type'] == 'questions')
	{
	$cids_image = get_image_component($field);

	// Delete all file usages within this entity.

	foreach($items as $delta => $item)
		{
		foreach($item as $cid => $value)
			{
			if (in_array($cid, $cids_image) and !empty($value))
				{
				$fids = explode(',', $value);
				foreach($fids as $fid)
					{
					$file = file_load($fid);
					questions_field_field_delete_file($file, $field, $entity_type, $id, 0);
					}
				}
			}
		}
	}
}



/**
* Implements hook_field_insert().
*/

function questions_field_field_insert($entity_type, $entity, $field, $instance, $langcode, &$items)
{

list($id, $vid, $bundle) = entity_extract_ids($entity_type, $entity);

if ($field['type'] == 'questions')
	{

  $cids_image = get_image_component($field);
	foreach($items as $item)
		{
		foreach($item as $cid => $value)
			{
			if (in_array($cid, $cids_image) and !empty($value))
				{
				$fids = explode(',', $value);
				foreach($fids as $fid)
					{
					$file = file_load($fid);
					$file = (object)$file;

					file_usage_add($file, 'questions_field', $entity_type, $id);
					}
				}
			}
		}
	}
}



/**
* Implements hook_field_presave().
*/

function questions_field_field_presave($entity_type, $entity, $field, $instance, $langcode, &$items)
{

list($id, $vid, $bundle) = entity_extract_ids($entity_type, $entity);
if ($field['type'] == 'questions')
	{
   _questions_field_field_formating_items($entity_type, $entity, $field, $instance, $langcode, $items);
}
}


/**
 * hook_field_load()
 */
function questions_field_field_load($entity_type, $entities, $field, $instances, $langcode, &$items, $age) {
}