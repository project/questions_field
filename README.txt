-- SUMMARY --

Questions field defines a new field type that allows to add several questions
(components field) with one field, in addition each field created has permission
that allows users(e.g. Webmaster) with specific role to ADD|EDIT|DELETE question.

For a full description of the module, visit the project page:
  http://drupal.org/project/questions_field

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/questions_field


-- REQUIREMENTS --

Drupal 7.x : Fields API is provided already by core [no dependencies].


-- INSTALLATION --

1. Copy the entire questions_field directory the Drupal sites/all/modules directory.

2. Login as an administrator. Enable the module in the "Administer" -> "Modules"

3. Add field of type questions follow
   Administration » Structure » Content types » Article » Manage fields
   see https://www.drupal.org/node/774742 for further information to add field type

4. Click on "Add first question" in Edit field(field added) page.



-- CONFIGURATION --

* Configure user permissions in Administration » People » Permissions, in order
to grant administrator permission to manage the questions.

-- VIEWS --
this module actually create field type, so you can
use the formatter view of this field in order to
display components value in the views in addition
the formatter view allows you to display each component
value(as separate field) of this field a part.



