<?php

/**
 * @file
 * Questions field module file component.
 */

/**
 * Implements _questions_field_defaults_component().
 */
function _questions_field_defaults_file() {
  return array(
    'name' => '',
    'form_key' => NULL,
    'required' => 0,
    'pid' => 0,
    'weight' => 0,
    'extra' => array(
      'filtering' => array(
        'types' => array('gif', 'jpg', 'png'),
        'addextensions' => '',
        'size' => '2 MB',
      ),
      'rename' => '',
      'scheme' => 'public',
      'directory' => '',
      'progress_indicator' => 'throbber',
      'title_display' => 0,
      'description' => '',
      'description_above' => FALSE,
      'attributes' => array(),
      'private' => FALSE,
      'analysis' => FALSE,
    ),
  );
}


function _questions_field_field_file($component){
    $data = array('type', 'cid', 'name', 'form_key', 'extra' => array('filtering'));
        $compnent_returned = array();
    foreach($data as $key => $value){
               if($key === 'extra'){
            foreach($value as $extra_key){
              if(isset($component[$key][$extra_key]))
                  $compnent_returned[$key][$extra_key] = $component[$key][$extra_key];
            }
        }
        elseif(isset($component[$value])) $compnent_returned[$value] = $component[$value];
    }
 return $compnent_returned;

}

function _questions_field_instance_file($component){
    $exclude = array('type', 'cid', 'name', 'form_key', 'extra' => array('filtering'));
    foreach ($exclude as $key => $value){
        if($key === 'extra'){
            foreach($value as $extra_key){
              if(isset($component[$key][$extra_key]))
                  unset($component[$key][$extra_key]);
            }
        }
        elseif(isset($component[$value])) unset($component[$value]);
    }

    return $component;
}


/**
 * Implements _questions_field_theme_component().
 */
function _questions_field_theme_file() {
  return array(
    'questions_field_edit_file_extensions' => array(
      'render element' => 'element',
      'file' => 'components/file.inc',
    ),
    'questions_field_render_file' => array(
      'render element' => 'element',
      'file' => 'components/file.inc',
    ),
    'questions_field_display_file' => array(
      'render element' => 'element',
      'file' => 'components/file.inc',
    ),
    'questions_field_managed_file' => array(
      'render element' => 'element',
      'file' => 'components/file.inc',
    ),
  );
}

/**
 * Implements _questions_field_edit_component().
 */
function _questions_field_edit_file($component) {
    $form = array();
    $form['#element_validate'] = array('_questions_field_edit_file_check_directory');
    $form['#after_build'] = array('_questions_field_edit_file_check_directory');

    $form['validation']['size'] = array(
      '#type' => 'textfield',
      '#title' => t('Max upload size'),
      '#default_value' => $component['extra']['filtering']['size'],
      '#description' => t('Enter the max file size a user may upload such as 2 MB or 800 KB. Your server has a max upload size of @size.', array('@size' => format_size(file_upload_max_size()))),
      '#size' => 10,
      '#parents' => array('extra', 'filtering', 'size'),
      '#element_validate' => array('_questions_field_edit_file_size_validate'),
      '#weight' => 1,
    );

    $form['validation']['extensions'] = array(
      '#element_validate' => array('_questions_field_edit_file_extensions_validate'),
      '#parents' => array('extra', 'filtering'),
      '#theme' => 'questions_field_edit_file_extensions',
      '#theme_wrappers' => array('form_element'),
      '#title' => t('Allowed file extensions'),
      '#attached' => array(
        'js' => array(drupal_get_path('module', 'questions_field') . '/js/questions-field-admin.js'),
        'css' => array(drupal_get_path('module', 'questions_field') . '/css/questions-field-admin.css'),
      ),
      '#type' => 'questions_field_file_extensions',
      '#weight' => 2,
    );

    // Find the list of all currently valid extensions.
    $current_types = isset($component['extra']['filtering']['types']) ? $component['extra']['filtering']['types'] : array();

    $types = array('gif', 'jpg', 'png');
    $form['validation']['extensions']['types']['webimages'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Web images'),
      '#options' => drupal_map_assoc($types),
      '#default_value' => array_intersect($current_types, $types),
    );

    $types = array('bmp', 'eps', 'tif', 'pict', 'psd');
    $form['validation']['extensions']['types']['desktopimages'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Desktop images'),
      '#options' => drupal_map_assoc($types),
      '#default_value' => array_intersect($current_types, $types),
    );

    $types = array('txt', 'rtf', 'html', 'pdf', 'doc', 'docx', 'odt', 'ppt', 'pptx', 'odp', 'xls', 'xlsx', 'ods', 'xml');
    $form['validation']['extensions']['types']['documents'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Documents'),
      '#options' => drupal_map_assoc($types),
      '#default_value' => array_intersect($current_types, $types),
    );

    $types = array('avi', 'mov', 'mp3', 'ogg', 'wav');
    $form['validation']['extensions']['types']['media'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Media'),
      '#options' => drupal_map_assoc($types),
      '#default_value' => array_intersect($current_types, $types),
    );

    $types = array('bz2', 'dmg', 'gz', 'jar', 'rar', 'sit', 'tar', 'zip');
    $form['validation']['extensions']['types']['archives'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Archives'),
      '#options' => drupal_map_assoc($types),
      '#default_value' => array_intersect($current_types, $types),
    );

    $form['validation']['extensions']['addextensions'] = array(
      '#type' => 'textfield',
      '#title' => t('Additional extensions'),
      '#default_value' => $component['extra']['filtering']['addextensions'],
      '#description' => t('Enter a list of additional file extensions for this upload field, separated by commas.<br /> Entered extensions will be appended to checked items above.'),
      '#size' => 20,
      '#weight' => 3,
    );

    $scheme_options = array();
    foreach (file_get_stream_wrappers(STREAM_WRAPPERS_WRITE_VISIBLE) as $scheme => $stream_wrapper) {
      $scheme_options[$scheme] = $stream_wrapper['name'];
    }
    $form['extra']['scheme'] = array(
      '#type' => 'radios',
      '#title' => t('Upload destination'),
      '#options' => $scheme_options,
      '#default_value' => $component['extra']['scheme'],
      '#description' => t('Private file storage has significantly more overhead than public files, but restricts file access to users who can view submissions.'),
      '#weight' => 4,
      '#access' => count($scheme_options) > 1,
    );
    $form['extra']['directory'] = array(
      '#type' => 'textfield',
      '#title' => t('Upload directory'),
      '#default_value' => $component['extra']['directory'],
      '#description' => t('You may optionally specify a sub-directory to store your files.') . ' ' . theme('questions_field_token_help'),
      '#weight' => 5,
      '#field_prefix' => 'questions_field/',
    );

    $form['extra']['rename'] = array(
      '#type' => 'textfield',
      '#title' => t('Rename files'),
      '#default_value' => $component['extra']['rename'],
      '#description' => t('You may optionally use tokens to create a pattern used to rename files upon submission. Omit the extension; it will be added automatically.') . ' ' . theme('questions_field_token_help', array('groups' => array('component', 'submission'))),
      '#weight' => 6,
      '#element_validate' => array('_questions_field_edit_file_rename_validate'),
    );

    $form['display']['progress_indicator'] = array(
      '#type' => 'radios',
      '#title' => t('Progress indicator'),
      '#options' => array(
        'throbber' => t('Throbber'),
        'bar' => t('Bar with progress meter'),
      ),
      '#default_value' => $component['extra']['progress_indicator'],
      '#description' => t('The throbber display does not show the status of uploads but takes up less space. The progress bar is helpful for monitoring progress on large uploads.'),
      '#weight' => 16,
      '#access' => file_progress_implementation(),
      '#parents' => array('extra', 'progress_indicator'),
    );

    // TODO: Make managed_file respect the "size" parameter.
    /*
    $form['display']['width'] = array(
      '#type' => 'textfield',
      '#title' => t('Width'),
      '#default_value' => $component['extra']['width'],
      '#description' => t('Width of the file field.') . ' ' . t('Leaving blank will use the default size.'),
      '#size' => 5,
      '#maxlength' => 10,
      '#weight' => 4,
      '#parents' => array('extra', 'width')
    );
    */

    return $form;
}

/**
 * A Form API element validate function to ensure that the rename string is
 * either empty or contains at least one token.
 */
function _questions_field_edit_file_rename_validate($element, &$form_state, $form) {
  $rename = trim($form_state['values']['extra']['rename']);
  form_set_value($element, $rename, $form_state);
  if (strlen($rename) && !count(token_scan($rename))) {
    form_error($element, t('To create unique file names, use at least one token in the file name pattern.'));
  }
}

/**
 * A Form API element validate function to check filesize is valid.
 */
function _questions_field_edit_file_size_validate($element) {
  if (!empty($element['#value'])) {
    $set_filesize = parse_size($element['#value']);
    if ($set_filesize == FALSE) {
      form_error($element, t('File size @value is not a valid filesize. Use a value such as 2 MB or 800 KB.', array('@value' => $element['#value'])));
    }
    else {
      $max_filesize = parse_size(file_upload_max_size());
      if ($max_filesize < $set_filesize) {
        form_error($element, t('An upload size of @value is too large, you are allow to upload files @max or less.', array('@value' => $element['#value'], '@max' => format_size($max_filesize))));
      }
    }
  }
}

/**
 * A Form API after build and validate function.
 *
 * Ensure that the destination directory exists and is writable.
 */
function _questions_field_edit_file_check_directory($element) {
  $scheme = $element['extra']['scheme']['#value'];
  $directory = $element['extra']['directory']['#value'];
  $destination_dir = file_stream_wrapper_uri_normalize($scheme . '://questions_field/' . $directory);
  $tokenized_dir = drupal_strtolower(questions_field_replace_tokens($destination_dir));

  // Sanity check input to prevent use parent (../) directories.
  if (preg_match('/\.\.[\/\\\]/', $tokenized_dir . '/')) {
    form_error($element['extra']['directory'], t('The save directory %directory is not valid.', array('%directory' => $tokenized_dir)));
  }
  else {
    if (!file_prepare_directory($tokenized_dir, FILE_CREATE_DIRECTORY)) {
      form_error($element['extra']['directory'], t('The save directory %directory could not be created. Check that the Questions field files directory is writable.', array('%directory' => $tokenized_dir)));
    }
  }

  return $element;
}

/**
 * A Form API element validate function.
 *
 * Change the submitted values of the component so that all filtering extensions
 * are saved as a single array.
 */
function _questions_field_edit_file_extensions_validate($element, &$form_state) {
  // Predefined types.
  $extensions = array();
  foreach (element_children($element['types']) as $category) {
    foreach (array_keys($element['types'][$category]['#value']) as $extension) {
      if ($element['types'][$category][$extension]['#value']) {
        $extensions[] = $extension;
        // jpeg is an exception. It is allowed anytime jpg is allowed.
        if ($extension == 'jpg') {
          $extensions[] = 'jpeg';
        }
      }
    }
  }

  // Additional types.
  $additional_extensions = explode(',', $element['addextensions']['#value']);
  foreach ($additional_extensions as $extension) {
    $clean_extension = drupal_strtolower(trim($extension));
    if (!empty($clean_extension) && !in_array($clean_extension, $extensions)) {
      $extensions[] = $clean_extension;
    }
  }

  form_set_value($element['types'], $extensions, $form_state);
}

/**
 * Output the list of allowed extensions as checkboxes.
 */
function theme_questions_field_edit_file_extensions($variables) {
  $element = $variables['element'];

  // Format the components into a table.
  $rows = array();
  foreach (element_children($element['types']) as $filtergroup) {
    $row = array();
    $first_row = count($rows);
    if ($element['types'][$filtergroup]['#type'] == 'checkboxes') {
      $select_link = ' <a href="#" class="questions-field-select-link questions-field-select-link-' . $filtergroup . '">(' . t('select') . ')</a>';
      $row[] = $element['types'][$filtergroup]['#title'];
      $row[] = array('data' => $select_link, 'width' => 40);
      $row[] = array('data' => drupal_render_children($element['types'][$filtergroup]), 'class' => array('questions-field-file-extensions', 'questions-field-select-group-' . $filtergroup));
      $rows[] = array('data' => $row);
      unset($element['types'][$filtergroup]);
    }
  }

  // Add the row for additional types.
  if (!isset($element['addextensions']['#access']) || $element['addextensions']['#access']) {
    $row = array();
    $title = $element['addextensions']['#title'];
    $element['addextensions']['#title'] = NULL;
    $row[] = array('data' => $title, 'colspan' => 2);
    $row[] = drupal_render($element['addextensions']);
    $rows[] = $row;
  }

  $header = array(array('data' => t('Category'), 'colspan' => '2'), array('data' => t('Types')));

  // Create the table inside the form.
  $element['types']['table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#attributes' => array('class' => array('questions-field-file-extensions')),
  );

  return drupal_render_children($element);
}

/**
 * Implements _questions_field_render_component().
 */
function _questions_field_render_file($component, $value = NULL, $entity, $filter = TRUE, $submission = NULL) {

  // Cap the upload size according to the PHP limit.
  $max_filesize = parse_size(file_upload_max_size());
  $set_filesize = $component['extra']['filtering']['size'];
  if (!empty($set_filesize) && parse_size($set_filesize) < $max_filesize) {
    $max_filesize = parse_size($set_filesize);
  }

  $element = array(
    '#type' => 'managed_file',
    '#theme' => 'questions_field_managed_file',
    '#title' =>  t($component['question']),
    '#title_display' => $component['extra']['title_display'] ? $component['extra']['title_display'] : 'before',
    '#required' => $component['required'],
    '#default_value' => isset($value) ? $value : NULL,
    '#attributes' => $component['extra']['attributes'],
    '#upload_validators' => array(
      'file_validate_size' => array($max_filesize),
      'file_validate_extensions' => array(implode(' ', $component['extra']['filtering']['types'])),
    ),
    '#pre_render' => array_merge(element_info_property('managed_file', '#pre_render'), array('questions_field_file_allow_access')),
    '#upload_location' => $component['extra']['scheme'] . '://questions_field/' . ($filter
                                                                              ? drupal_strtolower(questions_field_replace_tokens($component['extra']['directory'], $entity))
                                                                              : $component['extra']['directory']),
    '#progress_indicator' => $component['extra']['progress_indicator'],
    '#description' => $filter ? questions_field_filter_descriptions($component['extra']['description'], $entity) : $component['extra']['description'],
    '#weight' => $component['weight'],
    '#theme_wrappers' => array('form_element', 'questions_field_render_file'),
    '#translatable' => array('title', 'description'),
  );

	if(!empty($component['extra']['css_classes'])){
    $element['#attributes']['class'][] = $component['extra']['css_classes'];
 }

  if ($filter) {
    $element['#description'] = theme('file_upload_help', array('description' => $element['#description'], 'upload_validators' => $element['#upload_validators']));
  }
  return $element;
}

/**
 * Format the output of data for this component.
 */
function theme_questions_field_render_file($variables) {
 $component = $variables['element']['#questions_field_component'];
 $element = $variables['element'];
 $prefix = '';
 $suffix = '';

 if(!empty($component['extra']['wrapper_classes'])){
   if(isset($element['#prefix'])){
    if (strpos($element['#prefix'], 'class="') !== false){
      $element['#prefix'] = str_replace('class="', 'class="'.$component['extra']['wrapper_classes'], $element['#prefix']);
    }else{
      $element['#prefix'] = str_replace('id="', 'class="'.$component['extra']['wrapper_classes'].'" id="', $element['#prefix']);
    }
   }else{
    $element['#prefix'] = '<div class="'.$component['extra']['wrapper_classes'].'">';
    $element['#suffix'] = '</div>';
   }
   $prefix = $element['#prefix'];
   $suffix = $element['#suffix'];
 }
 // This wrapper is required to apply JS behaviors and CSS styling.
 $output  = $prefix;
 $output .= drupal_render_children($element);
 $output .= $suffix;
 return $output;
}


/**
 * Returns HTML for a questions field managed file element.
 *
 * See #2495821 and #2497909. The core theme_file_managed_file creates a
 * wrapper around the element with the element's id, thereby creating 2 elements
 * with the same id.
 *
 * @param $variables
 *   An associative array containing:
 *   - element: A render element representing the file.
 *
 */
function theme_questions_field_managed_file($variables) {
  $element = $variables['element'];

  $attributes = array();

  // For questions field use, do not add the id to the wrapper.
  //if (isset($element['#id'])) {
  //  $attributes['id'] = $element['#id'];
  //}

  if (!empty($element['#attributes']['class'])) {
    $attributes['class'] = (array) $element['#attributes']['class'];
  }
  $attributes['class'][] = 'form-managed-file';

  // This wrapper is required to apply JS behaviors and CSS styling.
  $output = '';
  $output .= '<div' . drupal_attributes($attributes) . '>';
  $output .= drupal_render_children($element);
  $output .= '</div>';
  return $output;
}


/**
 * Implements _questions_field_submit_component().
 */
function _questions_field_submit_file($component, $value) {
  $fid = is_array($value)
            ? (!empty($value['fid']) ? $value['fid'] : '')
            : (!empty($value) ? $value : '');
  // Extend access to this file, even if the submission has not been saved yet. This may happen when
  // previewing a private file which was selected but not explicitly uploaded, and then previewed.
  if ($fid) {
    $_SESSION['questions_field_files'][$fid] = $fid;
  }
  return $fid;
}

/**
 * Pre-render callback to allow access to uploaded files.
 *
 * Files that have not yet been saved into a submission must be accessible to
 * the user who uploaded it, but no one else. After the submission is saved,
 * access is granted through the file_usage table. Before then, we use a
 * $_SESSION value to record a user's upload.
 *
 * @see questions_field_file_download()
 */
function questions_field_file_allow_access($element) {
  if (!empty($element['#value']['fid'])) {
    $fid = $element['#value']['fid'];
    $_SESSION['questions_field_files'][$fid] = $fid;
  }

  return $element;
}

/**
 * Implements _questions_field_display_component().
 */
function _questions_field_display_file($component, $value, $format = 'html', $submission = array()) {
  $fid = isset($value) ? $value : NULL;
  return array(
    '#title' => $component['question'],
    '#title_display' => $component['extra']['title_display'] ? $component['extra']['title_display'] : 'before',
    '#value' => $fid ? questions_field_get_file($fid) : NULL,
    '#weight' => $component['weight'],
    '#theme' => 'questions_field_display_file',
    '#theme_wrappers' => $format == 'text' ? array('questions_field_element_text') : array('form_element'),
    '#format' => $format,
    '#component' => $component,
    '#translatable' => array('title'),
    '#parents' => array($component['form_key'])
  );
}

/**
 * Format the output of text data for this component
 */
function theme_questions_field_display_file($variables) {
  $element = $variables['element'];
  $file = $element['#value'];
  $url = !empty($file) ? questions_field_file_url($file->uri) : t('no upload');
  return !empty($file) ? ($element['#format'] == 'text' ? $url : l($file->filename, $url)) : ' ';
}

/**
 * Implements _questions_field_delete_component().
 */
function _questions_field_delete_file($component, $value) {
  // Delete an individual submission file.
  if (!empty($value) && ($file = questions_field_get_file($value))) {
    file_usage_delete($file, 'questions_field');
    file_delete($file);
  }
}



/**
 * Implements _questions_field_attachments_component().
 */
function _questions_field_attachments_file($component, $value) {
  $file = (array) questions_field_get_file($value);
  //This is necessary until the next release of mimemail is out, see [#1388786]
  $file['filepath'] = $file['uri'];
  $files = array($file);
  return $files;
}



/**
 * Implements _questions_field_analysis_component().
 */
function _questions_field_analysis_file($component, $sids = array(), $single = FALSE, $join = NULL) {

}

/**
 * Implements _questions_field_table_component().
 */
function _questions_field_table_file($component, $value) {
  $output = '';
  $file = questions_field_get_file($value);
  if (!empty($file->fid)) {
    $output = '<a href="' . questions_field_file_url($file->uri) . '">' . check_plain(questions_field_file_name($file->uri)) . '</a>';
    $output .= ' (' . (int) ($file->filesize/1024) . ' KB)';
  }
  return $output;
}



 function _questions_field_schema_column_file() {
       return  array(
        'description' => 'File.',
        'type' => 'varchar',
        'length' => 32,
        'not null' => FALSE,
        'default' => '',
      );
 }

function _questions_field_data_property_info_file($component) {
     return  array(
      'label'=> $component['name'],
      'type' => 'text',
      'getter callback' => 'entity_property_verbatim_get',
      'setter callback' => 'entity_property_verbatim_set',
    );
 }

/**
 * Implements _questions_field_csv_headers_component().
 */
function _questions_field_csv_headers_file($component, $export_options) {
  $header = array();
  // Two columns in header.
  $header[0] = array('', '');
  $header[1] = array($export_options['header_keys'] ? $component['form_key'] : $component['name'], '');
  $header[2] = array(t('Name'), t('Filesize (KB)'));
  return $header;
}

/**
 * Implements _questions_field_csv_data_component().
 */
function _questions_field_csv_data_file($component, $export_options, $value) {
  $file = questions_field_get_file($value);
  return empty($file->filename) ? array('', '') : array(questions_field_file_url($file->uri), (int) ($file->filesize/1024));
}

/**
 * Helper function to create proper file names for uploaded file.
 */
function questions_field_file_name($filepath) {
  if (!empty($filepath)) {
    $info = pathinfo($filepath);
    $file_name = $info['basename'];
  }
  return isset($file_name) ? $file_name : '';
}

/**
 * Helper function to create proper URLs for uploaded file.
 */
function questions_field_file_url($uri) {
  if (!empty($uri)) {
    $file_url = file_create_url($uri);
  }
  return isset($file_url) ? $file_url : '';
}

/**
 * Helper function to load a file from the database.
 */
function questions_field_get_file($fid) {
  // Simple check to prevent loading of NULL values, which throws an entity
  // system error.
  return $fid ? file_load($fid) : FALSE;
}

/**
 * Given a submission with file_usage set, add or remove file usage entries.
 */
function questions_field_file_usage_adjust($submission) {
  if (isset($submission->file_usage)) {
    $files = file_load_multiple($submission->file_usage['added_fids']);
    foreach ($files as $file) {
      $file->status = 1;
      file_save($file);
      file_usage_add($file, 'questions_field', 'submission', $submission->sid);
    }

    $files = file_load_multiple($submission->file_usage['deleted_fids']);
    foreach ($files as $file) {
      file_usage_delete($file, 'questions_field', 'submission', $submission->sid);
      file_delete($file);
    }
  }
}

/**
 * Rename any files which are eligible for renaming, if this submission is being
 * submitted for the first time.
 */
function questions_field_file_rename($component, $submission) {
  if (isset($submission->file_usage)) {
    foreach ($submission->file_usage['renameable'] as $cid => $fids) {
      foreach ($fids as $fid) {
        questions_field_file_process_rename($component, $submission, $component['cid'], $fid);
      }
    }
  }
}


/**
 * Renames the uploaded file name using tokens.
 *
 * @param $component
 *   The questions_field component object.
 * @param $submission
 *   The questions_field submission object.
 * @param $cid
 *   Component settings array for which fid is going to be processed.
 * @param $fid
 *   A file id to be processed.
 */
function questions_field_file_process_rename($component, $submission, $cid, $fid) {
  $file = questions_field_get_file($fid);

  if ($file) {
    // Get the destination uri.
    $destination_dir = $component['extra']['scheme'] . '://questions_field/' . drupal_strtolower(questions_field_replace_tokens($component['extra']['directory'], $component));
    $destination_dir = file_stream_wrapper_uri_normalize($destination_dir);

    // Get the file extension.
    $info = pathinfo($file->uri);
    $extension = $info['extension'];

    // Prepare new file name without extension.
    $new_file_name = questions_field_replace_tokens($component['extra']['rename'], $component, $submission, NULL, TRUE);
    $new_file_name = trim($new_file_name);
    $new_file_name = _questions_field_transliterate($new_file_name);
    $new_file_name = str_replace('/', '_', $new_file_name);
    $new_file_name = preg_replace('/[^a-zA-Z0-9_\- ]/', '', $new_file_name);
    if (strlen($new_file_name)) {
      // Prepare the new uri with new filename.
      $destination = "$destination_dir/$new_file_name.$extension";

      // Compare the uri and Rename the file name.
      if ($file->uri != $destination) {
        file_move($file, $destination, FILE_EXISTS_RENAME);
      }
    }
  }
}
