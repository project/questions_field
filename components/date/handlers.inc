<?php
 

/**

 * Implements _questions_field_defaults_component().

 */

function _questions_field_defaults_date() {

  return array(

    'name' => '',
    'form_key' => NULL,
    'question' => '',
    'weight' => 0,
    'value' => '',
    'required' => 0,
    'extra' => array(
      'type' => '',
      'timezone' => 'user',
      'start_date' => '-2 years',
      'end_date' => '+2 years',
      'year_textfield' => 0,
      'title_display' => 0,
      'description' => '',
      'description_above' => FALSE,
      'attributes' => array(),
    ),

  );

}



function _questions_field_field_date($component){

    $data = array('type', 'cid', 'name', 'form_key', 'extra' => array('timezone', 'type', 'start_date', 'end_date'));

        $compnent_returned = array();

    foreach($data as $key => $value){

               if($key === 'extra'){

            foreach($value as $extra_key){

              if(isset($component[$key][$extra_key]))

                  $compnent_returned[$key][$extra_key] = $component[$key][$extra_key];

            }

        }

        elseif(isset($component[$value])) $compnent_returned[$value] = $component[$value];

    }

 return $compnent_returned;



}



function _questions_field_instance_date($component){

    $exclude = array('type', 'cid', 'name', 'form_key', 'extra' => array('timezone', 'type', 'start_date', 'end_date'));
    foreach ($exclude as $key => $value){

        if($key === 'extra'){

            foreach($value as $extra_key){

              if(isset($component[$key][$extra_key]))

                  unset($component[$key][$extra_key]);

            }

        }

        elseif(isset($component[$value])) unset($component[$value]);

    }

    return $component;

}



/**
 * Implements _questions_field_theme_component().
 */
function _questions_field_theme_date() {

  return array(

    'questions_field_date' => array(
      'render element' => 'element',
      'file' => 'components/date/handlers.inc',
    ),

    'questions_field_calendar' => array(
      'variables' => array('data' => null),
      'template' => 'components/date/templates/calendar',
      'file' => 'components/date/handlers.inc',
    ),

  );

}


/**
 * @see Implements hook_element_info().
 */

function _questions_field_element_info_date()
	{
	$element['questions_field_date'] = array(
		'#input' => TRUE,
		'#theme' => 'questions_field_date',
		'#size' => 20,
  );
	return $element;
	}



 
/**
 * Implements _questions_field_action_set_component().
 */
function _questions_field_action_set_date($component, &$element, &$form_state, $value) {

  $element['#value'] = $value;

  form_set_value($element, $value, $form_state);

}
 



 function _questions_field_schema_column_date() {

       return  array(
        'type' => 'varchar',
        'length' => 20,
        'not null' => FALSE,
        'sortable' => TRUE,
        'views' => TRUE,
        'description' => 'Date'
      );


 }



function _questions_field_data_property_info_date($component) {

     return  array(
      'label'=> $component['name'],
      'type' => 'text',
      'getter callback' => 'entity_property_verbatim_get',
      'setter callback' => 'entity_property_verbatim_set',

    );

 }



/**
 * Implements _questions_field_edit_component().
 */

function _questions_field_edit_date($component) {

  $form = array();
 

  
  $form = array();
  $form['value'] = array(
    '#type' => 'textfield',
    '#title' => t('Default value'),
    '#default_value' => $component['value'],
    '#description' => t("Describe a time by reference to the current day, like '+90 days' (90 days from the day the field is created) or '+1 Saturday' (the next Saturday). See @strtotime_link for more details", array('@link' => l(t('strtotime'), 'http://www.php.net/manual/en/function.strtotime.php'))
                            ),
    '#size' => 60,
    '#maxlength' => 127,
    '#weight' => 0,
  );

  $types = array('date' => t('Date'), 'datetime-local' => t('Date and time control'));
 
   $form['extra']['type'] = array(
     '#type' => 'select',
     '#title' => t('Select the date type'),
     '#multiple' => FALSE,
     '#default_value' => empty($component['extra']['type'])? $component['extra']['type'] : 'date',
     '#options' => $types,
    //  '#empty_option' => t('- None -'),
    //  '#empty_value' => '_none',
     '#weight' => 0.1,
   );

  $form['extra']['timezone'] = array(
    '#type' => 'radios',
    '#title' => t('Default value timezone'),
    '#default_value' => empty($component['extra']['timezone']) ? 'user' : $component['extra']['timezone'],
    '#description' => t('If using relative dates for a default value (e.g. "today") base the current day on this timezone.'),
    '#options' => array('user' => t('User timezone'), 'site' => t('Website timezone')),
    '#weight' => 2,
    '#access' => variable_get('configurable_timezones', 1),
  );

  $form['extra']['year_textfield'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use a textfield for year'),
    '#default_value' => $component['extra']['year_textfield'],
    '#description' => t('If checked, the generated date field will use a textfield for the year. Otherwise it will use a select list.'),
    '#weight' => 5,
    '#parents' => array('extra', 'year_textfield'),
  );

  $form['extra']['start_date'] = array(
    '#type' => 'textfield',
    '#title' => t('Start date'),
    '#default_value' => $component['extra']['start_date'],
    '#description' => t('The earliest date that may be entered into the field.') . ' ' . t('Accepts any date in any <a href="http://www.gnu.org/software/tar/manual/html_chapter/Date-input-formats.html">GNU Date Input Format</a>.'),
    '#size' => 30,
    '#weight' => 3,
    '#parents' => array('extra', 'start_date'),
  );
  $form['extra']['end_date'] = array(
    '#type' => 'textfield',
    '#title' => t('End date'),
    '#default_value' => $component['extra']['end_date'],
    '#description' => t('The latest date that may be entered into the field.') . ' ' . t('Accepts any date in any <a href="http://www.gnu.org/software/tar/manual/html_chapter/Date-input-formats.html">GNU Date Input Format</a>.'),
    '#size' => 30,
    '#weight' => 4,
    '#parents' => array('extra', 'end_date'),
  );


  return $form;

}




/**
 * 
 * Implements _questions_field_render_component()
 * @see questions_field_field_widget_form()
 */

function _questions_field_render_date($component, $value = NULL, $entity = NULL, $filter = TRUE, $widget_arguments = array())  {

  list($form, $form_state, $field, $instance, $langcode, $items, $delta, $element_widget) = $widget_arguments;

  $element = array(
    '#type' => 'questions_field_date',
    '#title' =>  t($component['question']),
    '#title_display' => $component['extra']['title_display'] ? $component['extra']['title_display'] : 'before',
    '#default_value' => $filter ? questions_field_replace_tokens($component['value'], $entity) : $component['value'],
    '#required' => $component['required'],
    '#weight' => $component['weight'],
    '#start_date' => trim($component['extra']['start_date']),
    '#end_date' => trim($component['extra']['end_date']),
    '#year_textfield' => $component['extra']['year_textfield'],
    '#description' => $filter ? questions_field_filter_descriptions(t($component['extra']['description']), $entity) : t($component['extra']['description']),
    '#attributes' => $component['extra']['attributes'],
    '#timezone' => !empty($component['extra']['timezone']) ? $component['extra']['timezone'] : 'user',
    '#element_validate' => array('questions_field_validate_date'),
    '#theme_wrappers' => array('form_element'),
    '#translatable' => array('title', 'description'),
    '#delta' => $delta,
    '#value_callback' => 'questions_field_date_callback',
    '#attached' => array(
      'js' => array(drupal_get_path('module', 'questions_field') . '/components/date/js/qf_date.js'),
    ),
  );

 
  if ($component['required']) {
    $element['#attributes']['required'] = 'required';
  }

 
  

  if(!empty($component['extra']['wrapper_classes'])){

    $element['#prefix'] = '<div class="'.$component['extra']['wrapper_classes'].'">';

    $element['#suffix'] = '</div>';

  }



  if(!empty($component['extra']['css_classes'])){

    $element['#attributes']['class'][] = $component['extra']['css_classes'];

  }
  
    if(!isset($value) 
                  and isset($element['#default_value'])){
      $value = $element['#default_value'];
    }

    if(is_numeric($value)){
      $value = date('Y-m-d H:i:s', $value);
    }

    $format = 'Y-m-d';
    if($component['extra']['type'] != 'date'){
      $format .= '\TH:i:s';
    }

    $element['#default_value'] = !empty($value) ? questions_field_strtodate($format, $value, $element['#timezone']) : '';
 
 
  return $element;

}



/**

 * Implements _questions_field_display_component().

 */

function _questions_field_display_date($component, $value, $weight = 0, $format = 'html', $arguments_display = array())  {

  list($entity_type, $entity, $field, $instance, $langcode, $items, $display) = $arguments_display;

  $element =  array(
    '#title' => t($component['question']),
    '#title_display' => $component['extra']['title_display'] ? $component['extra']['title_display'] : 'before',
    '#weight' => $weight,
    '#theme' => 'questions_field_display_textfield',
    '#theme_wrappers' => $format == 'html' ? array('questions_field_form_element') : array('questions_field_element_text'),

    '#format' => $format,
    '#attributes' => array('class' => array('component', 
                                            'component-type-'.$component['type'], 
                                            'component-name-'.$component['cid'],
                                            $field['field_name'] . '-' . $component['cid']
                                          )
                          ),
    '#value' => isset($value) ? $value : '',
    '#translatable' => array('title'),
    '#parents' => array($component['form_key'])

  );
  return $element;

}
 

/**
 * Implements _questions_field_submit_component().
 */
function _questions_field_submit_date($component, $value) {
 return $value;
}

 