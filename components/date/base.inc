<?php


  /**
 * Theme a questions field date element.
 */
function theme_questions_field_date($variables) {
  $element = $variables['element'];
 $component = $element['#questions_field_component'];

    element_set_attributes($element, array(
      'id',
      'name',
      'value',
    ));
    _form_set_class($element, array(
      'form-date',
      'questions-field-calendar'
    ));
    
    $element['#attributes']['type'] = $component['extra']['type'];

    if ($element['#start_date']) {
      $element['#attributes']['min'] =  $element['#start_date'];  
    }

    if ($element['#end_date']) {
      $element['#attributes']['max'] =  $element['#end_date'];
    }
    //Add hidden input to unify the date format by cross platform browsers
    $output = '<input type="hidden" id="'.$element['#attributes']['id'].'"
                                    name="'.$element['#attributes']['name'].'" 
                                    value="'.$element['#attributes']['value'].'">';
             unset($element['#attributes']['name']);
             $element['#attributes']['id'] .= "_widget";
    $output .= '<input '. drupal_attributes($element['#attributes']) .' >';
  
 
  return $output;
}



function questions_field_date_callback($element, $input = FALSE, $form_state) {
  $value = $element['#default_value'];
  if ($input) {
          $value = drupal_array_get_nested_value($form_state['input'], $element['#parents']);
          $value = questions_field_strtotime($value);
   } 
   return  $value;
}


/**
 * Element validation for Questions field date fields.
 */
function questions_field_validate_date(&$element, &$form_state) {
 }
