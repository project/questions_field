(function ($) {

    "use strict";
  
  $(document).on('change','.form-type-questions-field-date input[type="date"]', function(event){
    var dateSelected = $(this).val();
    if(dateSelected){
     var d = new Date(dateSelected);
     var dateUniqueFormat= d.getDate()  + "-" + (d.getMonth()+1) + "-" + d.getFullYear();
     $(this).closest('.form-type-questions-field-date').find('input[type="hidden"]').val(dateUniqueFormat);
    }

  })
  
  })(jQuery);
  