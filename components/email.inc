<?php
/**
 * @file
 * Question field module email component.
 */
/**
 * Implements _questions_field_defaults_component().
 */

function _questions_field_defaults_email()
	{
	return array(
		'name' => '',
		'form_key' => NULL,
		'pid' => 0,
		'weight' => 0,
		'value' => '',
		'required' => 0,
		'extra' => array(
			'multiple' => 0,
			'email_format' => 'short',
			'width' => '',
			'unique' => 0,
			'disabled' => 0,
			'title_display' => 0,
			'description' => '',
			'description_above' => FALSE,
			'placeholder' => '',
			'attributes' => array() ,
			'private' => FALSE,
			'analysis' => FALSE,
		) ,
	);
	}

/**
 * Implements _questions_field_theme_component().
 */

function _questions_field_theme_email()
	{
	return array(
		'questions_field_email' => array(
			'render element' => 'element',
			'file' => 'components/email.inc',
		) ,
		'questions_field_display_email' => array(
			'render element' => 'element',
			'file' => 'components/email.inc',
		) ,
	);
	}

/**
 * Implements _questions_field_edit_component().
 */

function _questions_field_edit_email($component)
	{
	$form['value'] = array(
		'#type' => 'textfield',
		'#title' => t('Default value') ,
		'#default_value' => $component['value'],
		'#description' => t('The default value of the field.') . ' ' . theme('questions_field_token_help') ,
		'#size' => 60,
		'#maxlength' => 127,
		'#weight' => 0,
		'#attributes' => ($component['value'] == '[current-user:mail]' && count(form_get_errors()) == 0) ? array(
			'disabled' => TRUE
		) : array() ,
		'#id' => 'email-value',
	);
	$form['user_email'] = array(
		'#type' => 'checkbox',
		'#title' => t('User email as default') ,
		'#default_value' => $component['value'] == '[current-user:mail]' ? 1 : 0,
		'#description' => t('Set the default value of this field to the user email, if he/she is logged in.') ,
		'#attributes' => array(
			'onclick' => 'getElementById("email-value").value = (this.checked ? "[current-user:mail]" : ""); getElementById("email-value").disabled = this.checked;'
		) ,
		'#weight' => 0,
		'#element_validate' => array(
			'_questions_field_edit_email_validate'
		) ,
	);
	$form['extra']['multiple'] = array(
		'#type' => 'checkbox',
		'#title' => t('Multiple') ,
		'#default_value' => $component['extra']['multiple'],
		'#description' => t('Allow multiple e-mail addresses, separated by commas.') ,
		'#weight' => 0,
	);
	$form['extra']['email_format'] = array(
		'#type' => 'radios',
		'#title' => t('Email Format') ,
		'#options' => array(
			'long' => t('Allow long format: "Example Name" &lt;name@example.com&gt;') ,
			'short' => t('Short format only: name@example.com') ,
		) ,
		'#default_value' => $component['extra']['email_format'],
		'#description' => t('Not all servers support the "long" format.') ,
	);
	$form['display']['width'] = array(
		'#type' => 'textfield',
		'#title' => t('Width') ,
		'#default_value' => $component['extra']['width'],
		'#description' => t('Width of the textfield.') . ' ' . t('Leaving blank will use the default size.') ,
		'#size' => 5,
		'#maxlength' => 10,
		'#parents' => array(
			'extra',
			'width'
		) ,
	);
	$form['display']['placeholder'] = array(
		'#type' => 'textfield',
		'#title' => t('Placeholder') ,
		'#default_value' => $component['extra']['placeholder'],
		'#description' => t('The placeholder will be shown in the field until the user starts entering a value.') . ' ' . t('Often used for example values, such as "john@example.com".') ,
		'#parents' => array(
			'extra',
			'placeholder'
		) ,
	);
	$form['display']['disabled'] = array(
		'#type' => 'checkbox',
		'#title' => t('Disabled') ,
		'#return_value' => 1,
		'#description' => t('Make this field non-editable. Useful for displaying default value. Changeable via JavaScript or developer tools.') ,
		'#weight' => 11,
		'#default_value' => $component['extra']['disabled'],
		'#parents' => array(
			'extra',
			'disabled'
		) ,
	);
	$form['validation']['unique'] = array(
		'#type' => 'checkbox',
		'#title' => t('Unique') ,
		'#return_value' => 1,
		'#description' => t('Check that all entered values for this field are unique. The same value is not allowed to be used twice.') ,
		'#weight' => 1,
		'#default_value' => $component['extra']['unique'],
		'#parents' => array(
			'extra',
			'unique'
		) ,
	);
	return $form;
	}

/**
 * Implements _questions_field_submit_component().
 */
function _questions_field_submit_email($component, $value) {
return $value;
}


/**
 * Element validation function for the email edit form.
 */

function _questions_field_edit_email_validate($element, &$form_state)
	{
	if ($form_state['values']['user_email'])
		{
		$form_state['values']['value'] = '[current-user:mail]';
		}
	}

/**
 * Implements _questions_field_render_component().
 */

function _questions_field_render_email($component, $value = NULL, $entity, $filter = TRUE, $submission = NULL)
	{
	$element = array(
		'#type' => 'questions_field_email',
		'#title' =>  t($component['question']),
		'#title_display' => $component['extra']['title_display'] ? $component['extra']['title_display'] : 'before',
		'#default_value' => $filter ? questions_field_replace_tokens($component['value'], $entity) : $component['value'],
		'#required' => $component['required'],
		'#weight' => $component['weight'],
		'#description' => $filter ? questions_field_filter_descriptions($component['extra']['description'], $entity) : t($component['extra']['description']) ,
		'#attributes' => $component['extra']['attributes'],
		'#element_validate' => array(
			'_questions_field_validate_email'
		) ,
		'#theme_wrappers' => array('form_element') ,
		'#translatable' => array(
			'title',
			'description',
			'field_prefix',
			'field_suffix'
		) ,
	);
	if ($component['required'])
		{
		$element['#attributes']['required'] = 'required';
		}

	// Add an e-mail class for identifying the difference from normal textfields.

	$element['#attributes']['class'][] = 'email';

	// Enforce uniqueness.

	if ($component['extra']['unique'])
		{
		$element['#element_validate'][] = 'questions_field_validate_unique';
		}

	if ($component['extra']['email_format'] == 'long')
		{

		// html5 email elements enforce short-form email validation in addition to
		// pattern validation. This means that long format email addresses must be
		// rendered as text.

		$element['#attributes']['type'] = 'text';

		// html5 patterns have implied delimiters and start and end patterns.
		// The are also case sensitive, not global, and not multi-line.
		// See https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/HTML5/Constraint_validation
		// See http://stackoverflow.com/questions/19605773/html5-email-validation

		$address = '[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*';
		$name = '("[^<>"]*?"|[^<>",]*?)';
		$short_long = "($name *<$address>|$address)";
		$element['#attributes']['pattern'] = $component['extra']['multiple'] ? "$short_long(, *$short_long)*" : $short_long;
		}
	elseif ($component['extra']['multiple'])
		{
		$element['#attributes']['multiple'] = 'multiple';
		}

	if (!empty($value))
		{
		$element['#default_value'] = $value;
		}

	if ($component['extra']['placeholder'])
		{
		$element['#attributes']['placeholder'] = $component['extra']['placeholder'];
		}

	if ($component['extra']['disabled'])
		{
		if ($filter)
			{
			$element['#attributes']['readonly'] = 'readonly';
			}
		  else
			{
			$element['#disabled'] = TRUE;
			}
		}

	 if(!empty($component['extra']['wrapper_classes'])){
			$element['#prefix'] = '<div class="'.$component['extra']['wrapper_classes'].'">';
			$element['#suffix'] = '</div>';
	  }
		
		
	 if(!empty($component['extra']['css_classes'])){
			$element['#attributes']['class'][] = $component['extra']['css_classes'];
	  }

	// Change the 'width' option to the correct 'size' option.

	if ($component['extra']['width'] > 0)
		{
		$element['#size'] = $component['extra']['width'];
		}

	return $element;
	}

/**
 * Theme function to render an email component.
 */

function theme_questions_field_email($variables)
	{
	$element = $variables['element'];

	// This IF statement is mostly in place to allow our tests to set type="text"
	// because SimpleTest does not support type="email".

	if (!isset($element['#attributes']['type']))
		{
		$element['#attributes']['type'] = 'email';
		}

	// Convert properties to attributes on the element if set.

	foreach(array(
		'id',
		'name',
		'value',
		'size'
	) as $property)
		{
		if (isset($element['#' . $property]) && $element['#' . $property] !== '')
			{
			$element['#attributes'][$property] = $element['#' . $property];
			}
		}

	_form_set_class($element, array(
		'form-text',
		'form-email'
	));
	return '<input' . drupal_attributes($element['#attributes']) . ' />';
	}

/**
 * A Drupal Form API Validation function. Validates the entered values from
 * email components on the client-side form.
 *
 * @param $form_element
 *   The e-mail form element.
 * @param $form_state
 *   The full form state for the questions field.
 *
 * @return
 *   None. Calls a form_set_error if the e-mail is not valid.
 */

function _questions_field_validate_email($form_element, &$form_state)
	{
	$component = $form_element['#questions_field_component'];
	$email_format = !empty($component['extra']['email_format']) ? $component['extra']['email_format'] : 'short';
	questions_field_email_validate($form_element['#value'], implode('][', $form_element['#parents']) ,

	// Required validation is done elsewhere.

	TRUE, $component['extra']['multiple'],

	// No tokens are allowed in user input.

	FALSE, $email_format);
	}

function _questions_field_field_email($component)
	{
	$data = array(
		'type',
		'cid',
		'name',
		'form_key',
		'extra' => array(
			'maxlength',
			'multiple'
		)
	);
	$compnent_returned = array();
	foreach($data as $key => $value)
		{
		if ($key === 'extra')
			{
			foreach($value as $extra_key)
				{
				if (isset($component[$key][$extra_key])) $compnent_returned[$key][$extra_key] = $component[$key][$extra_key];
				}
			}
		elseif (isset($component[$value])) $compnent_returned[$value] = $component[$value];
		}

	return $compnent_returned;
	}

function _questions_field_instance_email($component)
	{
	$exclude = array(
		'type',
		'cid',
		'name',
		'form_key',
		'extra' => array(
			'maxlength',
			'multiple'
		)
	);
	foreach($exclude as $key => $value)
		{
		if ($key === 'extra')
			{
			foreach($value as $extra_key)
				{
				if (isset($component[$key][$extra_key])) unset($component[$key][$extra_key]);
				}
			}
		elseif (isset($component[$value])) unset($component[$value]);
		}

	return $component;
	}

/**
 * Implements _questions_field_csv_headers_component().
 */

function _questions_field_csv_headers_email($component, $export_options)
	{
	$header = array();
	$header[0] = '';
	$header[1] = '';
	$header[2] = $export_options['header_keys'] ? $component['form_key'] : $component['name'];
	return $header;
	}

/**
 * Implements _questions_field_action_set_component().
 */

function _questions_field_action_set_email($component, &$element, &$form_state, $value)
	{
	$element['#value'] = $value;
	form_set_value($element, $value, $form_state);
	}

/**
 * Implements _questions_field_csv_data_component().
 */

function _questions_field_csv_data_email($component, $export_options, $value)
	{
	return !isset($value) ? '' : $value;
	}

function _questions_field_schema_column_email()
	{
	return array(
		'description' => 'Email.',
		'type' => 'varchar',
		'length' => 255,
		'not null' => FALSE,
		'default' => '',
	);
	}

function _questions_field_data_property_info_email($component)
	{
	return array(
		'label' => $component['name'],
		'type' => 'text',
		'getter callback' => 'entity_property_verbatim_get',
		'setter callback' => 'entity_property_verbatim_set',
	);
	}

/**
 * Implements _questions_field_display_component().
 */

function _questions_field_display_email($component, $value, $weight = 0, $format = 'html', $submission = array())
	{

	// Allows the user to specify how to render his list,
	// Note: this variable(multiple_settings) used only with multiple values
	if (!empty($component['multiple_settings']))
		{

		$all_values = explode(',', $value);
		if ($component['multiple_settings']['delta_reversed'])
			{
			$all_values = array_reverse($all_values);
			}

		$delta_limit = $component['multiple_settings']['delta_limit'];
		$offset = intval($component['multiple_settings']['delta_offset']);

		// We should only get here in this case if there's an offset, and
		// in that case we're limiting to all values after the offset.

		if ($delta_limit == 'all')
			{
			$delta_limit = count($all_values) - $offset;
			}

		// Determine if only the first and last values should be shown

		$delta_first_last = $component['multiple_settings']['delta_first_last'];
		$new_values = array();
		for ($i = 0; $i < $delta_limit; $i++)
			{
			$new_delta = $offset + $i;
			if (isset($all_values[$new_delta]))
				{

				// If first-last option was selected, only use the first and last values

				if (!$delta_first_last

				// Use the first value.

				 || $new_delta == $offset

				// Use the last value.

				 || $new_delta == ($delta_limit + $offset - 1))
					{
					if ($format == 'html') $all_values[$new_delta] = check_plain($all_values[$new_delta]);
				      $new_values[] = _render_email($component, $all_values[$new_delta]);
					}
				}
			}

		if ($component['multiple_settings']['multi_type'] == 'separator')
			{
			foreach($new_values as $delta => $new_value)
				{
				$new_values[$delta] = isset($options[$new_value]) ? $options[$new_value] : $new_value;
				}

			$new_values = implode(filter_xss_admin($component['multiple_settings']['separator']) , $new_values);
			}
		  else
			{
			$new_values = theme('item_list', array(
				'items' => $new_values,
				'title' => null,
				'type' => $component['multiple_settings']['multi_type']
			));
			}

		$value = $new_values;
		}
	  else
		{
		if ($format == 'html') $value = check_plain($value);
     $new_values = '';
     foreach(explode(',', $value) as $delta => $new_value){
        $new_values .= _render_email($component, $new_value).' ';
     }
      $value = $new_values;
		}

	return array(
		'#title' => $component['question'],
		'#title_display' => $component['extra']['title_display'] ? $component['extra']['title_display'] : 'before',
		'#weight' => $weight,
		'#multiple' => $component['extra']['multiple'],
		'#theme' => 'questions_field_display_email',
		'#theme_wrappers' => $format == 'html' ? array(
			'form_element'
		) : array(
			'questions_field_element_text'
		) ,
		'#format' => $format,
		'#value' => !empty($value) ? $value : '',
		'#component' => $component,
		'#translatable' => array(
			'title'
		) ,
		'#parents' => array(
			$component['form_key']
		)
	);
	}

/**
 * Format the text output for this component.
 */

function theme_questions_field_display_email($variables)
	{
	$element = $variables['element'];
	$component = $element['#component'];
	$prefix = $element['#format'] == 'html' ? '' : $element['#field_prefix'];
	$suffix = $element['#format'] == 'html' ? '' : $element['#field_suffix'];
	return $element['#value'] !== '' ? ($prefix . $element['#value'] . $suffix) : ' ';
	}


function _render_email($component, $value){
  $email_format = !empty($component['email_settings']['email_format']) ? $component['email_settings']['email_format'] : 'email_default';
   switch($email_format){
    case 'email_plain':
    return $value;
    default:
   return l($value, 'mailto:' . $value);
 }

}

/**
 * Implements _questions_field_table_component().
 */

function _questions_field_table_email($component, $value)
	{
	return check_plain(empty($value[0]) ? '' : $value[0]);
	}
