<?php


/**

 * Format the output of data for this component.

 */

function theme_questions_field_display_textfield($variables) {

    $element = $variables['element'];
  
    $prefix = $element['#format'] == 'html' ? '' : $element['#field_prefix'];
  
    $suffix = $element['#format'] == 'html' ? '' : $element['#field_suffix'];
  
    $value = $element['#format'] == 'html' ? check_plain($element['#value']) : $element['#value'];
  
   return $value !== '' ? ($prefix . '<span class="form-item-value">' . $value . '</span>' . $suffix) : ' ';
  
  }
  
  function theme_questions_field_display_number($variables){
    $element = $variables['element'];
    $element['#attributes']['type'] = 'number';
    element_set_attributes($element, array(
      'id',
      'name',
      'value',
      'size',
      'maxlength',
    ));
    _form_set_class($element, array(
      'form-control',
      'form-text',
      'form-text-number'
    ));
  return '<input' . drupal_attributes($element['#attributes']) . ' />';
  }