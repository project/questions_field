<?php



/**

 * @file

 * question_field module textfield component.

 */



/**

 * Implements _questions_field_defaults_component().

 */

function _questions_field_defaults_textfield() {

  $defaults = array(

    'name' => '',

    'form_key' => NULL,

    'question' => '',

    'weight' => 0,

    'value' => null,

    'required' => 0,

    'extra' => array(
      'value_type' => '',

      'readonly' => false,

      'width' => '',

      'maxlength' => '',

      'field_prefix' => '',

      'field_suffix' => '',

      'disabled' => 0,

      'title_display' => 0,

      'description' => '',

      'description_above' => FALSE,

      'placeholder' => '',

      'attributes' => array(),

      'analysis' => FALSE,

    ),

  );
  if(module_exists('format_number')){
    $defaults['extra']['nf_precision'] = 0;
    $defaults['extra']['nf_decimals'] = 2;
  }
  return $defaults;

}



function _questions_field_field_textfield($component){

    $data = array('type', 'cid', 'name', 'form_key', 'extra' => array('value_type', 'maxlength'));

        $compnent_returned = array();

    foreach($data as $key => $value){

               if($key === 'extra'){

            foreach($value as $extra_key){

              if(isset($component[$key][$extra_key]))

                  $compnent_returned[$key][$extra_key] = $component[$key][$extra_key];

            }

        }

        elseif(isset($component[$value])) $compnent_returned[$value] = $component[$value];

    }

 return $compnent_returned;



}



function _questions_field_instance_textfield($component){

    $exclude = array('type', 'cid', 'name', 'form_key', 'extra' => array('value_type', 'maxlength'));

    foreach ($exclude as $key => $value){

        if($key === 'extra'){

            foreach($value as $extra_key){

              if(isset($component[$key][$extra_key]))

                  unset($component[$key][$extra_key]);

            }

        }

        elseif(isset($component[$value])) unset($component[$value]);

    }

    return $component;

}



/**

 * Implements _questions_field_theme_component().

 */

function _questions_field_theme_textfield() {

  return array(

    'questions_field_display_textfield' => array(
      'render element' => 'element',
      'file' => 'components/textfield/handlers.inc',
    ),

    'questions_field_display_number' => array(
      'render element' => 'element',
      'file' => 'components/textfield/handlers.inc',
    ),

  );

}



/**

 * Implements _questions_field_table_component().

 */

function _questions_field_table_textfield($component, $value) {

  return check_plain(empty($value[0]) ? '' : $value[0]);

}



/**

 * Implements _questions_field_action_set_component().

 */

function _questions_field_action_set_textfield($component, &$element, &$form_state, $value) {

  $element['#value'] = $value;

  form_set_value($element, $value, $form_state);

}



/**

 * Implements _questions_field_csv_headers_component().

 */

function _questions_field_csv_headers_textfield($component, $export_options) {

  $header = array();

  $header[0] = '';

  $header[1] = '';

  $header[2] = $export_options['header_keys'] ? $component['form_key'] : $component['name'];

  return $header;

}



/**

 * Implements _questions_field_csv_data_component().

 */

function _questions_field_csv_data_textfield($component, $export_options, $value) {

  return !isset($value[0]) ? '' : $value[0];

}



 function _questions_field_schema_column_textfield() {

       return  array(
        'description' => 'TextField.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
        'default' => NULL,

      );

 }



function _questions_field_data_property_info_textfield($component) {

     return  array(
      'label'=> $component['name'],
      'type' => 'text',
      'getter callback' => 'entity_property_verbatim_get',
      'setter callback' => 'entity_property_verbatim_set',

    );

 }



/**
 * Implements _questions_field_edit_component().
 */

function _questions_field_edit_textfield($component) {

  $form = array();

  $form['value'] = array(
    '#type' => 'textfield',
    '#title' => t('Default value'),
    '#default_value' => isset($component['value']) ? $component['value'] : null,
    '#description' => t('The default value of the field.') . ' ' . theme('questions_field_token_help'),
    '#size' => 60,
    '#maxlength' => 1024,
    '#weight' => 0,
  );
 
  $form['display']['width'] = array(

    '#type' => 'textfield',

    '#title' => t('Width'),

    '#default_value' => $component['extra']['width'],

    '#description' => t('Width of the textfield.') . ' ' . t('Leaving blank will use the default size.'),

    '#size' => 5,

    '#maxlength' => 10,

    '#weight' => 0,

    '#parents' => array('extra', 'width'),

  );

 $value_types = array('numeric' => t('Numeric'));
 
 if(module_exists('format_number')){
  $value_types['numeric_format'] =  t('Numeric with format');

  $form['extra']['nf_precision'] = array(
    '#type' => 'textfield',
    '#title' => t('Format number precision'),
    '#default_value' => $component['extra']['nf_precision'],
    // '#states' => array(
    //   'visible' => array(
    //     ':select[name="value_type"]' => array('value' => 'numeric_format'),
    //   ),
    // ),
  );
  $form['extra']['nf_decimals'] = array(
    '#type' => 'textfield',
    '#title' => t('Format number decimals'),
    '#default_value' => $component['extra']['nf_decimals'],
 
  );
  
 }

  $form['extra']['value_type'] = array(
    '#type' => 'select',
    '#title' => t('Select the value type'),
    '#multiple' => FALSE,
    '#default_value' => $component['extra']['value_type'],
    '#options' => $value_types,
    '#empty_option' => t('- None -'),
    '#empty_value' => '_none',
    '#weight' => 0.1,
  );
 


  $form['display']['placeholder'] = array(
    '#type' => 'textfield',
    '#title' => t('Placeholder'),
    '#default_value' => $component['extra']['placeholder'],
    '#description' => t('The placeholder will be shown in the field until the user starts entering a value.'),
    '#weight' => 1,
    '#parents' => array('extra', 'placeholder'),
  );

  $form['display']['field_prefix'] = array(

    '#type' => 'textfield',

    '#title' => t('Prefix text placed to the left of the textfield'),

    '#default_value' => $component['extra']['field_prefix'],

    '#description' => t('Examples: $, #, -.'),

    '#size' => 20,

    '#maxlength' => 127,

    '#weight' => 2.1,

    '#parents' => array('extra', 'field_prefix'),

  );

  $form['display']['field_suffix'] = array(

    '#type' => 'textfield',

    '#title' => t('Postfix text placed to the right of the textfield'),

    '#default_value' => $component['extra']['field_suffix'],

    '#description' => t('Examples: lb, kg, %.'),

    '#size' => 20,

    '#maxlength' => 127,

    '#weight' => 2.2,

    '#parents' => array('extra', 'field_suffix'),

  );

  $form['display']['disabled'] = array(

    '#type' => 'checkbox',

    '#title' => t('Disabled'),

    '#return_value' => 1,

    '#description' => t('Make this field non-editable. Useful for setting an unchangeable default value.'),

    '#weight' => 11,

    '#default_value' => $component['extra']['disabled'],

    '#parents' => array('extra', 'disabled'),

  );



  $form['validation']['maxlength'] = array(

    '#type' => 'textfield',

    '#title' => t('Maxlength'),

    '#default_value' => $component['extra']['maxlength'],

    '#description' => t('Maximum length of the textfield value.'),

    '#size' => 5,

    '#maxlength' => 10,

    '#weight' => 2,

    '#parents' => array('extra', 'maxlength'),

  );

  return $form;

}



/**
 * 
 * Implements _questions_field_render_component()
 * @see questions_field_field_widget_form()
 */

function _questions_field_render_textfield($component, $value = NULL, $entity = NULL, $token_replace = FALSE, $widget_arguments = array())  {

  list($form, $form_state, $field, $instance, $langcode, $items, $delta, $element_widget) = $widget_arguments;
 
  

  $element = array(
    '#type' => 'textfield',
    '#title' =>  t($component['question']),
    '#title_display' => $component['extra']['title_display'] ? t($component['extra']['title_display']) : 'before',
  //  @TODO: Integration with module token
    // '#default_value' => $token_replace ? questions_field_replace_tokens($component['value'], $entity) : isset($value) ? $value : $component['value'],
    '#default_value' => isset($value) ? $value : null,
    '#required' => $component['required'],
    '#weight' => $component['weight'],
    '#field_prefix' => (empty($component['extra']['field_prefix']) ? 
                                         NULL : ($token_replace ? filter_xss(t($component['extra']['field_prefix'])) : 
                                                                         t($component['extra']['field_prefix']))),

    '#field_suffix' => (empty($component['extra']['field_suffix']) ?
                                   NULL : ($token_replace ? filter_xss(t($component['extra']['field_suffix'])) : 
                                          t($component['extra']['field_suffix']))),

    '#description' => $token_replace ? questions_field_filter_descriptions(t($component['extra']['description']), $entity) : t($component['extra']['description']),
    '#attributes' => $component['extra']['attributes'],
    '#theme_wrappers' => array('form_element'),
    '#translatable' => array('title', 'description', 'field_prefix', 'field_suffix'),
    '#delta' => $delta,
  );

  if(!empty($element['#field_prefix'])){
    $element['#field_prefix'] = '<span class="field_prefix">'.$element['#field_prefix'].'</span>';
  }

  if(!empty($element['#field_suffix'])){
    $element['#field_suffix'] = '<span class="field_suffix">'.$element['#field_suffix'].'</span>';
  }


      if(!module_exists('format_number') and 
                !empty($component['extra']['value_type']) and 
                         $component['extra']['value_type'] == 'numeric_format'){

                          $component['extra']['value_type'] = 'numeric';
      }


      switch ($component['extra']['value_type']) {
        case "numeric":
            $element['#element_validate'][] = 'element_validate_number';
            $element['#theme'] = "questions_field_display_number";
        break;
        
        case "numeric_format":
            $element['#type'] = 'numericfield';
            $element['#decimals'] = $component['extra']['nf_decimals'];
            $element['#precision'] = $component['extra']['nf_precision'];
            $element['#attributes'] = array_merge($element['#attributes'], 
                                                    array('class' => array('form-control'))
                                                  );
            unset($element['#theme_wrappers']);
        break;

        default:
    
    }
 
  if ($component['required']) {
    $element['#attributes']['required'] = 'required';
  }


  if ($component['extra']['readonly']) {
    $element['#attributes']['readonly'] = null;
  }



  if ($component['extra']['placeholder']) {

    $element['#attributes']['placeholder'] = $component['extra']['placeholder'];

  }



  if ($component['extra']['disabled']) {

    if ($filter) {

      $element['#attributes']['readonly'] = 'readonly';

    }

    else {

      $element['#disabled'] = TRUE;

    }

  }

  

  if(!empty($component['extra']['wrapper_classes'])){

    $element['#prefix'] = '<div class="'.$component['extra']['wrapper_classes'].'">';

    $element['#suffix'] = '</div>';

  }



  if(!empty($component['extra']['css_classes'])){

    $element['#attributes']['class'][] = $component['extra']['css_classes'];

  }



  // Change the 'width' option to the correct 'size' option.

  if ($component['extra']['width'] > 0) {

    $element['#size'] = $component['extra']['width'];

  }

  if ($component['extra']['maxlength'] > 0) {

    $element['#maxlength'] = $component['extra']['maxlength'];

  }

  return $element;
}



/**

 * Implements _questions_field_display_component().

 */

function _questions_field_display_textfield($component, $value, $weight = 0, $format = 'html', $arguments_display = array())  {

  list($entity_type, $entity, $field, $instance, $langcode, $items, $display) = $arguments_display;

  $element =  array(
    '#title' => t($component['question']),
    '#title_display' => $component['extra']['title_display'] ? $component['extra']['title_display'] : 'before',
    '#weight' => $weight,
    '#theme' => 'questions_field_display_textfield',
    '#theme_wrappers' => $format == 'html' ? array('questions_field_form_element') : array('questions_field_element_text'),
    '#field_prefix' => '<span class="field_prefix">'.t($component['extra']['field_prefix']).'</span>',
    '#field_suffix' => '<span class="field_suffix">'.t($component['extra']['field_suffix']).'</span>',
    '#format' => $format,
    '#attributes' => array('class' => array('component', 
                                            'component-type-'.$component['type'], 
                                            'component-name-'.$component['cid'],
                                            $field['field_name'] . '-' . $component['cid']
                                          )
                          ),
    '#value' => isset($value) ? $value : '',
    '#translatable' => array('title', 'field_prefix', 'field_suffix'),
    '#parents' => array($component['form_key'])

  );
  return $element;

}
 

/**

 * Implements _questions_field_submit_component().

 */

function _questions_field_submit_textfield($component, $value) {
 if(!_qf_is_empty($value)){
    if(is_numeric($value)){
      //remove decimal not needed
      //0.0 => 0
      //0.1=>0.1
      $value = (double)$value;
      $value= strval($value);
    }
    return $value;
  }
return null;
}

