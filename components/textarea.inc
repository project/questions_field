<?php

/**
 * @file
 * Questions field module textarea component.
 */

/**
 * Implements _questions_field_defaults_component().
 */
function _questions_field_defaults_textarea() {
  return array(
    'name' => '',
    'form_key' => NULL,
    'question' => '',
    'weight' => 0,
    'value' => '',
    'required' => 0,
    'extra' => array(
      'cols' => '',
      'rows' => '',
      'title_display' => 0,
      'resizable' => 1,
      'disabled' => 0,
      'description' => '',
      'description_above' => FALSE,
      'placeholder' => '',
      'attributes' => array(),
      'private' => FALSE,
      'analysis' => FALSE,
    ),
  );
}


function _questions_field_field_textarea($component){
    return  array('type' => $component['type'],
                  'form_key' => $component['form_key'],
                  'cid' => $component['cid'],
                  'name' => $component['name'],
                );
}

function _questions_field_instance_textarea($component){
    $exclude = array('type', 'cid', 'name', 'form_key');
    foreach ($exclude as $key => $value){
        if($key === 'extra'){
            foreach($value as $extra_key){
              if(isset($component[$key][$extra_key]))
                  unset($component[$key][$extra_key]);
            }
        }
        elseif(isset($component[$value])) unset($component[$value]);
    }

    return $component;
}


/**
 * Implements _questions_field_theme_component().
 */
function _questions_field_theme_textarea() {
  return array(
    'questions_field_display_textarea' => array(
      'render element' => 'element',
      'file' => 'components/textarea.inc',
    ),
  );
}

/**
 * Implements _questions_field_edit_component().
 */
function _questions_field_edit_textarea($component) {
  $form = array();
  $form['value'] = array(
    '#type' => 'textarea',
    '#title' => t('Default value'),
    '#default_value' => $component['value'],
    '#description' => t('The default value of the field.') . ' ' . theme('questions_field_token_help'),
    '#cols' => 60,
    '#rows' => 5,
    '#weight' => 0,
  );
  $form['display']['cols'] = array(
    '#type' => 'textfield',
    '#title' => t('Width'),
    '#default_value' => $component['extra']['cols'],
    '#description' => t('Width of the textarea in columns. This property might not have a visual impact depending on the CSS of your site.') . ' ' . t('Leaving blank will use the default size.'),
    '#size' => 5,
    '#maxlength' => 10,
    '#parents' => array('extra', 'cols'),
  );
  $form['display']['rows'] = array(
    '#type' => 'textfield',
    '#title' => t('Height'),
    '#default_value' => $component['extra']['rows'],
    '#description' => t('Height of the textarea in rows.') . ' ' . t('Leaving blank will use the default size.'),
    '#size' => 5,
    '#maxlength' => 10,
    '#parents' => array('extra', 'rows'),
  );
  $form['display']['resizable'] = array(
    '#type' => 'checkbox',
    '#title' => t('Resizable'),
    '#description' => t('Make this field resizable by the user.'),
    '#weight' => 2,
    '#default_value' => $component['extra']['resizable'],
    '#parents' => array('extra', 'resizable'),
  );
  $form['display']['placeholder'] = array(
    '#type' => 'textfield',
    '#title' => t('Placeholder'),
    '#default_value' => $component['extra']['placeholder'],
    '#description' => t('The placeholder will be shown in the field until the user starts entering a value.'),
    '#parents' => array('extra', 'placeholder'),
  );
  $form['display']['disabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Disabled'),
    '#return_value' => 1,
    '#description' => t('Make this field non-editable. Useful for setting an unchangeable default value.'),
    '#weight' => 11,
    '#default_value' => $component['extra']['disabled'],
    '#parents' => array('extra', 'disabled'),
  );
  return $form;
}



/**
 * Implements _questions_field_render_component().
 */
function _questions_field_render_textarea($component, $value = NULL, $entity = NULL, $filter = TRUE, $submission = NULL) {

  $element = array(
    '#type' => 'textarea',
    '#title' =>  t($component['question']),
    '#title_display' => $component['extra']['title_display'] ? $component['extra']['title_display'] : 'before',
    '#default_value' =>$filter ? questions_field_replace_tokens($component['value'], $entity) : $component['value'],
    '#required' => $component['required'],
    '#weight' => $component['weight'],
    '#description' => $filter ? questions_field_filter_descriptions(t($component['extra']['description']), $entity)  : t($component['extra']['description']),
    '#rows' => !empty($component['extra']['rows']) ? $component['extra']['rows'] : 5,
    '#cols' => !empty($component['extra']['cols']) ? $component['extra']['cols'] : 60,
    '#attributes' => $component['extra']['attributes'],
    '#resizable' => (bool) $component['extra']['resizable'], // MUST be FALSE to disable.
    '#theme_wrappers' => array('form_element'),
    '#translatable' => array('title', 'description'),
  );

  if ($component['required']) {
    $element['#attributes']['required'] = 'required';
  }

  if ($component['extra']['placeholder']) {
    $element['#attributes']['placeholder'] = t($component['extra']['placeholder']);
  }

  if ($component['extra']['disabled']) {
    if ($filter) {
      $element['#attributes']['readonly'] = 'readonly';
    }
    else {
      $element['#disabled'] = TRUE;
    }
  }

  if(!empty($component['extra']['wrapper_classes'])){
    $element['#prefix'] = '<div class="'.$component['extra']['wrapper_classes'].'">';
    $element['#suffix'] = '</div>';
  }


  if(!empty($component['extra']['css_classes'])){
    $element['#attributes']['class'][] = $component['extra']['css_classes'];
  }


  if (isset($value)) {
    $element['#default_value'] = $value;
  }

  return $element;
}

/**
 * Implements _questions_field_display_component().
 */
function _questions_field_display_textarea($component, $value, $weight = 0, $format = 'html', $arguments_display = array()) {

  list($entity_type, $entity, $field, $instance, $langcode, $items, $display) = $arguments_display;

  return array(
    '#title' => t($component['question']),
    '#title_display' => $component['extra']['title_display'] ? $component['extra']['title_display'] : 'before',
    '#weight' => $weight,
    '#theme' => 'questions_field_display_textarea',
    '#theme_wrappers' => $format == 'html' ? array('questions_field_form_element') : array('questions_field_element_text'),
    '#format' => $format,
    '#value' => isset($value) ? $value : '',
    '#attributes' => array('class' => array('component', 
          'component-type-'.$component['type'], 
          'component-name-'.$component['cid'],
          $field['field_name'] . '-' . $component['cid']
        )
        ),
    '#translatable' => array('title'),
    '#parents' => array($component['form_key'])
  );
}

/**
 * Implements _questions_field_submit_component().
 */
function _questions_field_submit_textarea($component, $value) {
return $value;
}

/**
 * Format the output of data for this component.
 */
function theme_questions_field_display_textarea($variables) {
  $element = $variables['element'];
  $output = $element['#format'] == 'html' ? nl2br(check_plain($element['#value'])) : $element['#value'];
  if (drupal_strlen($output) > 80) {
    $output = ($element['#format'] == 'html') ? '<div class="questions-field-long-answer">' . $output . '</div>' : $output;
  }
  return $output !== '' ? $output : ' ';
}


 function _questions_field_schema_column_textarea() {
       return  array(
          'description' => 'textarea.',
          'type' => 'text',
          'size' => 'big',
          'not null' => FALSE,

      );
 }

 function _questions_field_data_property_info_textarea($component) {
     return  array(
      'label'=> $component['name'],
      'type' => 'text',
      'getter callback' => 'entity_property_verbatim_get',
      'setter callback' => 'entity_property_verbatim_set',
    );
 }

/**
 * Implements _questions_field_table_component().
 */
function _questions_field_table_textarea($component, $value) {
  return empty($value) ? '' : check_plain($value);
}

/**
 * Implements _questions_field_action_set_component().
 */
function _questions_field_action_set_textarea($component, &$element, &$form_state, $value) {
  $element['#value'] = $value;
  form_set_value($element, $value, $form_state);
}

