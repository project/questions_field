<?php

/**
 * Options used to display items in view entity
 */
function render_term_reference_items_list_options(){
  return array('id' => t('Id'),
              'label' => t('Label'), 
              'link' => t('Link'), 
              'term' => t('Term')
              );
}


/**
   *
   */
  function theme_questions_field_display_term_reference($variables) {
 
    $element = $variables['element'];
    $values = $element['#value'];
    $component = $element['#component'];
    
    if($component['display_type'] == 'questions_answers_default'){
     return $values;
   }
  
    $render_as = $component['display_settings']['render'];
    $new_values = array();
      if($render_as != 'id'){
        $values =  taxonomy_term_load_multiple($values);
      }
    foreach($values as $key => $record){
  
      if($render_as == 'id'){
        $new_values[] = $record;
        } 
        if($render_as == 'label'){
          $new_values[] = entity_label('taxonomy_term', $record);
        } 
       if($render_as == 'link'){
        $uri = entity_uri('taxonomy_term', $record);
        $new_values[] = l($record->name, $uri['path'], $uri['options']); 
        } 

        if($render_as == 'term'){
          $term_view = taxonomy_term_view($record);
          $new_values[] = drupal_render($term_view);
        }
      }
  
    if ( !empty($component['multiple_settings']['multi_type'] ) and $component['multiple_settings']['multi_type'] == 'separator') {
      return implode(filter_xss_admin( $component['multiple_settings']['separator']), $new_values);
    }	 
  
     return theme('item_list', array(
          'items' => $new_values,
          'title' => null,
          'type' => !empty($component['multiple_settings']['multi_type']) ? $component['multiple_settings']['multi_type'] : 'ul'
        ));
    
    }


function _qf_term_reference_allowed_values($referenceable_types) {
  $options = array();
    if ($vocabulary = taxonomy_vocabulary_load($referenceable_types)) {
      if ($terms = taxonomy_get_tree($vocabulary->vid)) {
        foreach ($terms as $term) {
          $options[$term->tid] = str_repeat('-', $term->depth) . $term->name;
        }
      }
    }
  return $options;
}
