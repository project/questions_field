<?php
 
/**
 * @file
 * Qustions field module multiple term_reference component.
 */

/**
 * Implements _questions_field_defaults_component().
 */
function _questions_field_defaults_term_reference() {
   
    return array(
        'name' => '',
        'form_key' => NULL,
        'question' => '',
        'required'  => 0,
        'weight' => 0,
        'referenceable_types' => '',
        'extra' => array(
          'description' => '',
          'widget_type' => '',
          'default_value' => NULL,
          'empty_option' => 'None',
          'empty_value' => '',
          'multiple' => NULL,
          'title_display' => 0,
          'attributes' => array(),
          'width' => '',
          'maxlength' => '',
          'description' => '',
          'description_above' => FALSE,
          'placeholder' => '',
          'disabled' => 0,
        ),
      );

  }
  
  
  function _questions_field_field_term_reference($component){
      $data= array('type', 'name','cid', 'form_key', 'referenceable_types', 'extra' => array('multiple'));
      $compnent_returned = array();
      foreach($data as $key => $value){
                 if($key === 'extra'){
              foreach($value as $extra_key){
                if(isset($component[$key][$extra_key]))
                    $compnent_returned[$key][$extra_key] = $component[$key][$extra_key];
              }
          }
          elseif(isset($component[$value])) $compnent_returned[$value] = $component[$value];
      }
   return $compnent_returned;
  }
  
  
  
  function _questions_field_instance_term_reference($component){
      $exclude = array('type', 'cid', 'name', 'form_key', 'referenceable_types','extra' => array( 'multiple'));
      foreach ($exclude as $key => $value){
          if($key === 'extra'){
              foreach($value as $extra_key){
                if(isset($component[$key][$extra_key]))
                    unset($component[$key][$extra_key]);
              }
          }
          elseif(isset($component[$value])) unset($component[$value]);
      }
  
      return $component;
  }
  
  
  /**
   * Implements _questions_field_theme_component().
   */
  function _questions_field_theme_term_reference() {
    return array(
      'questions_field_display_term_reference' => array(
        'render element' => 'element',
        'file' => 'components/term_reference/handlers.inc',
      ),
    );
  }
  
  /**
   * Implements _questions_field_edit_term_reference().
   */
  function _questions_field_edit_term_reference($component) {
    $form = array();
    $form['extra']['widget_type'] = array(
      '#type' => 'select',
      '#title' => t('Field Type'),
      '#multiple' => FALSE,
      '#default_value' => $component['extra']['widget_type'],
      '#options' => array('select' => t('Select List'), 'checkbox_radio' => t('Checkbox/Radio')),
      '#required' => TRUE,
    );

    $form['extra']['multiple'] = array(
      '#type' => 'checkbox',
      '#title' => t('Multiple'),
      '#default_value' => $component['extra']['multiple'],
      '#description' => t('Check this option if the user should be allowed to choose multiple values.'),
    );
 
    $voc_keys = array();
    foreach (taxonomy_get_vocabularies() as $vid => $vocab) {
      $voc_keys[$vid] = $vocab->name;
    }
    $form['referenceable_types'] = array(
      '#type' => 'select',
      '#title' => t('Select the vocabulary that can be referenced'),
      '#multiple' => FALSE,
      '#default_value' => $component['referenceable_types'],
      '#options' => $voc_keys,
    );

    $form['extra']['default_value'] = array(
      '#type' => 'textfield',
      '#title' => t('Default value'),
      '#default_value' => $component['extra']['default_value'],
      '#description' => t('The default value of the field.') . ' ' . theme('webform_token_help'),
    );

    $form['extra']['empty_option'] = array(
      '#type' => 'textfield',
      '#title' => t('Empty Option'),
      '#default_value' => $component['extra']['empty_option'],
      '#description' => t('The label to show for the initial option denoting no selection in a select element. By default, 
                                      the label is automatically set to "- Select -" for a required field and "- None -" for an optional field.'),
    );

    $form['extra']['empty_value'] = array(
      '#type' => 'textfield',
      '#title' => t('Empty Option'),
      '#default_value' => $component['extra']['empty_value'],
      '#description' => t('The value for the initial option denoting no selection in a select element, 
                                      which is used to determine whether the user submitted a value or not.'),
    );
    return $form;
  }
  
 
  
 
  
  /**
   * Implements _questions_field_render_component().
   *  @see questions_field_field_widget_form()
   */
  function _questions_field_render_term_reference($component, $value = NULL, $entity = NULL, $filter = TRUE, $widget_arguments = array()) {
 
    list($form, $form_state, $field, $instance, $langcode, $items, $delta, $element_widget) = $widget_arguments;
 
    $options = _qf_term_reference_allowed_values($component['referenceable_types']);

    $element = array(
      '#type' => 'select',
      '#options' => $options,
      '#multiple' => $component['extra']['multiple'],
      '#empty_option' => t($component['extra']['empty_option']),
      '#empty_value' => $component['extra']['empty_value'],
     );
     
     if( $component['extra']['multiple'] ){
      $value = array_filter(array_map('trim', explode(',', $value)), 'strlen');
    }
   
    if($component['extra']['widget_type'] == 'checkbox_radio'){
      unset($element['#multiple']);
      if($component['extra']['multiple']){
            $element['#type'] = 'checkboxes';
            if(!empty($value)){
               $value = array_filter(drupal_map_assoc($value), function($value) { return ((string)$value !== '0'); });
            }
       }else{
        $element['#type'] = 'radios'; 
            if(!$component['required']){
              $element['#options'] =   array($component['extra']['empty_value'] => t($component['extra']['empty_option'])) 
                                                                                        + $element['#options'];
                
            }
      }
    }

 
     $element += array( 
        '#title' =>  t($component['question']),
        '#title_display' => $component['extra']['title_display'] ? $component['extra']['title_display'] : 'before',
        '#required' => $component['required'],
        '#weight' => $component['weight'],
        '#field_prefix' => empty($component['extra']['field_prefix']) ? NULL : ($filter ? filter_xss(t($component['extra']['field_prefix'])) : t($component['extra']['field_prefix'])),
        '#field_suffix' => empty($component['extra']['field_suffix']) ? NULL : ($filter ? filter_xss(t($component['extra']['field_suffix'])) : t($component['extra']['field_suffix'])),
        '#description' => $filter ? questions_field_filter_descriptions(t($component['extra']['description']), $entity) : t($component['extra']['description']),
        '#attributes' => $component['extra']['attributes'],
        '#theme_wrappers' => array('form_element'),
        '#pre_render' => array(), // Needed to disable double-wrapping of radios and checkboxes.
        '#translatable' => array('title', 'description', 'field_prefix', 'field_suffix'),
        '#arguments' => array('entity_type' => $instance['entity_type'], 'bundle_name' => $instance['bundle'], 'field_name' => $field['field_name'],  'component_id' =>  $component['cid']),
        '#delta' => $delta,
      );
     
      if ($component['required']) {
        $element['#attributes']['required'] = 'required';
      }

      if ($component['extra']['placeholder']) {
        $element['#attributes']['placeholder'] = $component['extra']['placeholder'];
      }
      if ($component['extra']['disabled']) {
        if ($filter) {
          $element['#attributes']['readonly'] = 'readonly';
        }
        else {
          $element['#disabled'] = TRUE;
        }
    }
      
      if(!empty($component['extra']['wrapper_classes'])){
        $element['#prefix'] = '<div class="'.$component['extra']['wrapper_classes'].'">';
        $element['#suffix'] = '</div>';
      }
    
      if(!empty($component['extra']['css_classes'])){
       $element['#attributes']['class'][] = $component['extra']['css_classes'];
      }
    
      // Change the 'width' option to the correct 'size' option.
      if ($component['extra']['width'] > 0) {
        $element['#size'] = $component['extra']['width'];
      }
    
      if ($component['extra']['maxlength'] > 0) {
        $element['#maxlength'] = $component['extra']['maxlength'];
      }
     
      if (isset($value)) {
         $element['#default_value'] = $value;
      }
      return $element;
  }
  
   
 
  
  /**
   * Implements _questions_field_display_component().
   */
  function _questions_field_display_term_reference($component, $value, $weight = 0, $format = 'html',  $arguments_display = array()) {
    if(empty($value))  return;
 
    list($entity_type, $entity, $field, $instance, $langcode, $items, $display) = $arguments_display;
    
     $element =  array();
   
     if($component['display_type'] == 'questions_answers_selected'){
   
         $value = explode(',', $value);
         if(empty($items))  return;
   }
    
   $element +=  array(
     '#title' => t($component['question']),
     '#title_display' => $component['extra']['title_display'] ? $component['extra']['title_display'] : 'before',
     '#weight' => $weight,
     '#multiple' => $component['extra']['multiple'],
     '#theme' => 'questions_field_display_term_reference',
     '#theme_wrappers' => $format == 'html' ? array('questions_field_form_element') : array('questions_field_element_text'),
     '#attributes' => array('class' => array('component', 
             'component-type-'.$component['type'], 
             'component-name-'.$component['cid'],
             $field['field_name'] . '-' . $component['cid']
           )
           ),
     '#format' => $format,
     '#value' => $value,
     '#component' => $component,
     '#translatable' => array('title', 'options'),
     '#parents' => array($component['form_key'])
   );
   return $element;
  }
  
  /**
   * Implements _questions_field_submit_component().
   *
   * Handle select or other... modifications and convert FAPI 0/1 values into
   * something saveable.
   */
  function _questions_field_submit_term_reference($component, $value) {

    if(is_array($value)){
        $value = array_filter($value, function($value) { return ((string)$value !== '0'); });
       
        $value = implode(',', $value);
      }
    return $value;
  }
  
 
  
  
  /**
   * Implements _questions_field_table_component().
   */
  function _questions_field_table_term_reference($component, $value) {
    return check_plain(empty($value[0]) ? '' : $value[0]);
  }
  
  /**
   * Implements _questions_field_action_set_component().
   */
  function _questions_field_action_set_term_reference($component, &$element, &$form_state, $value) {
    $element['#value'] = $value;
    form_set_value($element, $value, $form_state);
  }
   

  
  
/**
 * Implements _questions_field_csv_headers_component().
 */
function _questions_field_csv_headers_term_reference($component, $export_options) {

    $header = array();
  
    $header[0] = '';
  
    $header[1] = '';
  
    $header[2] = $export_options['header_keys'] ? $component['form_key'] : $component['name'];
  
    return $header;
  
  }
  
  
  
  /**
  
   * Implements _questions_field_csv_data_component().
  
   */
  
  function _questions_field_csv_data_term_reference($component, $export_options, $value) {
  
    return !isset($value[0]) ? '' : $value[0];
  
  }

  function _questions_field_schema_column_term_reference() {

            return  array(
            'description' => 'Taxonomy term reference',
            'type' => 'varchar',
            'length' => 32,
            'not null' => FALSE,
            'default' => '',
        );
}



function _questions_field_data_property_info_term_reference($component) {

    return  array(
     'label'=> $component['name'],
     'type' => 'text',
     'getter callback' => 'entity_property_verbatim_get',
     'setter callback' => 'entity_property_verbatim_set',

   );

}
