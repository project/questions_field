<?php
/**
 * Retrieve a pipe delimited string of autocomplete suggestions
 */
function questions_field_revisionreference_autocomplete($entity_type, $bundle_name, $field_name, $component_id, $string = '') {
    // If the request has a '/' in the search text, then the menu system will have
    // split it into multiple arguments, recover the intended $string.
    $args = func_get_args();
    // Shift off the $entity_type argument.
    array_shift($args);
    // Shift off the $bundle_name argument.
    array_shift($args);
    // Shift off the $field_name argument.
    array_shift($args);
    // Shift off the $component_id argument.
    array_shift($args);

    $string = implode('/', $args);
  
    $field = field_info_field($field_name);
    $component = questions_field_get_component_load_single($entity_type, $bundle_name, $field_name, $component_id);
 
    if($component['extra']['multiple']){
      // The user enters a comma-separated list of tags.
      // We only autocomplete the last tag.
      $tags_typed = drupal_explode_tags($string);
      $tag_last = drupal_strtolower(array_pop($tags_typed));
      $string = $tag_last;
      if (!empty($tag_last)) {
        $prefix = count($tags_typed) ? implode(', ', $tags_typed) . ', ' : '';
      }
    }
 
    $options = array(
      'string' => $string,
      // @todo broken - this is in $field['widget']['settings']['autocomplete_match'] - we'd need the $instance.
      'match' => isset($component['autocomplete_match']) ? $component['autocomplete_match'] : 'contains',
      'limit' => 10,
    );
    $references = _questions_field_revisionreference_potential_references($field, $component, $options);
    foreach ($references as $vid => $row) {
      $matches[$prefix . $row['rendered'] .' [nid:'. $row['nid'] .'] [vid:'. $vid .']'] = _questions_field_revisionreference_item($field, $component, $row);
    }
    drupal_json_output($matches);
  }
 

  function questions_field_revisionreference_autocomplete_access($entity_type, $bundle_name, $field_name, $component_id) {
      return user_access('access content') && ($field = field_info_field($field_name)) && field_access('view', $field, $entity_type) && field_access('edit', $field, $entity_type);
  }
 
 

/**
 * Value callback for a revisionreference autocomplete element.
 *
 * Replace the node vid with a node title.
 */
function questions_field_revisionreference_autocomplete_value($element, $input = FALSE, $form_state) {
    if ($input === FALSE) {
      // We're building the displayed 'default value': expand the raw nid into
      // "node title [nid:n]".
      $vids = explode(',', $element['#default_value']);
      if (!empty($vids)) {
        $query = db_select('node_revision', 'n')
          ->fields('n', array('title', 'nid', 'vid'))
          ->condition('n.vid', $vids, 'IN')
          ->execute();
          $value = '';
          foreach($query->fetchAll() as $result){
            if(!empty($value)) $value .=',';
            $value .= $result->title .' [nid:'. $result->nid .'] [vid:'. $result->vid .']';
          }
        return $value;
      }
    }
  }

/**
 * @parm $referenceable_types content type
 * $return ('text', 'data')
 */
function qf_revision_options_list($referenceable_types, $return = "text"){
  $options = &drupal_static(__FUNCTION__);
  $key = (__FUNCTION__) . '_' . implode('_', $referenceable_types) . '_' . $return;
  if (!isset($options[$key])) {
    $options[$key] = array();
    foreach(_qf_revisions_list($referenceable_types) as $delta => $item){
      $item = (Array) $item;

      switch ($return) {
        case 'data':
        $options[$key][$item['vid']] = $item;
        break;

        default:
        $options[$key][$item['vid']] = ' [nid:'. $item['nid'] .'] ' . date("Y-m-d H:i",$item['timestamp']) .' by '. $item['name'] .' [vid:'. $item['vid'] .']';
        break;
      }
    }
  }
  return $options[$key];
}

  function _qf_revisions_list($referenceable_types, $vids = array()){
    $query = db_select('node_revision', 'r');
    $query->join('node', 'n', 'n.nid = r.nid');
    $query->join('users', 'u', 'u.uid = r.uid');
    $query->fields('r', array('title', 'timestamp', 'nid', 'vid', 'uid'));
    $query->fields('u', array('name'));
    $query->fields('n', array('title'));

    if(!empty($vids)) {
      $query->condition('r.vid', $vids, 'IN');
    }

    $query->condition('n.type', $referenceable_types, 'IN');
    $result = $query->execute();
  
    $records = $result->fetchAll();
    return $records;
  }


  /**
   * Validate an list element.
   *
   * Remove the wrapper layer and set the right element's value.
   */
  function questions_field_revisionreference_list_validate($element, &$form_state) {
     
    //skip the none value
    if($element['#value'] == '_none'){
      $element['#value'] = null;
        form_set_value($element, $element['#value'] , $form_state);
        return true;
      }

    $value = $element['#value'];
    if(!empty($element['#value']) and is_array($element['#value'])){
      $element['#value'] = implode(',', $element['#value']);
    }

     $is_valid = questions_field_revisionreference_textfield_validate($element, $form_state);

    if($is_valid){
      //Remove uncheked values
      form_set_value($element, $value, $form_state);
     }
    return $is_valid;
  }

  /**
   * Validate an textfield element.
   *
   * Remove the wrapper layer and set the right element's value.
   */
  function questions_field_revisionreference_textfield_validate($element, &$form_state) {
      
    list($entity_type, $bundle_name, $field_name,  $component_id) = array_values($element['#arguments']);
 
    $field = field_info_field($field_name);
    $component = questions_field_get_component_load_single($entity_type, $bundle_name, $field_name, $component_id);
    $value = $element['#value'];
    $vid = NULL;
    if (!empty($value)) {
 
        if(preg_match('/^[0-9]+(,[0-9]+)*$/', $value)) {

        //First check if valid value
        if(empty($component['extra']['multiple']) and strpos($value, ',') !== false){
          form_error($element, t('%name(%delta) component You cannot entered multiple values.', array('%name' => $element['#title'], '%delta' => $element['#delta'])));
          return false;
        }

        }else{
          form_error($element, t('%name(%delta) component Invalid value.', array('%name' => $element['#title'], '%delta' => $element['#delta'])));
          return false;
        }

       //After make sure format is valid, then check the value if exist
       $value = explode(',', $value);

       if (!$component['referenceable_types']) {
        return array();
      }
      foreach ($component['referenceable_types'] as $type => $_value) {
        if ($_value) {
          $referenceable_types[$type] = $type;
        }
      }
      $vids_not_exist =  check_node_revision($value, $referenceable_types);
      if(!empty($vids_not_exist)){
        form_error($element, t('%name(%delta) component the value <i>@value</i> is not exist.', array('%name' => $element['#title'], 
                                                                                                '%delta' => $element['#delta'], 
                                                                                                '@value' => implode(',', $vids_not_exist))
                                                                                              )
                                                                                            );
          return false;
        }


      }

      return true;
  }

  /**
   * @param $vids revision Id
   * @param $referenceable_types node types
   * Check the $vids given if exist or not.
   * return list of vids that not exist, return empty in case all exist
  */
  function check_node_revision($vids, $referenceable_types){
    $query = db_select('node_revision', 'r');
    $query->join('node', 'n', 'n.nid = r.nid');
    $query->fields('r', array('vid'));
    $query->condition('n.type', $referenceable_types, 'IN');
    $query->condition('r.vid', $vids, 'IN');
    $result = $query->execute(); 
    $vids_fetched = array();
    while($record = $result->fetchAssoc()) {
      $vids_fetched[] = trim($record['vid']);
    }

    $vids_not_exist = array();
    foreach($vids as $vid){
    if(!in_array($vid, $vids_fetched)){
      $vids_not_exist[] = $vid;
      }
    }
   return $vids_not_exist;
  }

  
  /**
   * Validate an autocomplete element.
   *
   * Remove the wrapper layer and set the right element's value.
   */
  function questions_field_revisionreference_autocomplete_validate($element, &$form_state) {
      
 
    list($entity_type, $bundle_name, $field_name,  $component_id) = $element['#arguments'];
 
    $field = field_info_field($field_name);
    $component = $element['#questions_field_component'];
    $value = $element['#value'];
     
   
    $vids_values = array();
    if (!empty($value)) {

      if(!$component['extra']['multiple'] and strpos($value, ',') !== false){
        form_error($element, t('%name(%delta) component You cannot entered multiple values.', array('%name' => $element['#title'], '%delta' => $element['#delta'])));
        return false;
      }else{
         $value = explode(',', $value);
      }

     
     foreach($value as $delta => $_value){
      $vid = NULL;
      preg_match('/^(?:\s*|(.*) )?\[\s*nid\s*:\s*(\d+)\s*\] ?\[\s*vid\s*:\s*(\d+)\s*\]$/', $_value, $matches);
      if (!empty($matches)) {
        // explicit title, vid and nid
        list(, $title, $nid, $vid) = $matches;
        $node = node_load($nid, $vid);
        if ($title && trim($title) != trim($node->title)) {
          form_error($element, t('%name: title mismatch. Please check your selection.', array('%name' => $element['#title'])));
          return false;
        }
      }
      else {
        $options = array(
          'string' => $_value,
          'match' => 'equals',
          'limit' => 1,
        );
        $vids = _questions_field_revisionreference_potential_references($field, $component, $options);
        $vid = (!empty($vids)) ? array_shift(array_keys($vids)) : 0;

        if(empty( $vid)) {
        form_error($element, t('%name(%delta) component the value <b><i>@value</i></b> title mismatch. Please check your selection.', array('%name' => $element['#title'], 
        '%delta' => $element['#delta'], 
        '@value' => $_value)
            )
         );
         return false;
        }

      }
      $vids_values[] = $vid;
    }
    form_set_value($element, implode(',', $vids_values), $form_state);
   }
 
    return $element;
  }



 
/**
 * Fetch an array of all candidate referenced nodes,
 * for use in presenting the selection form to the user.
 */
function _questions_field_revisionreference_potential_references($field, $component, $options = array()) {
    $options += array(
      'string' => '',
      'match' => 'contains',
      'ids' => array(),
      'limit' => 0,
    );
    if (module_exists('views')
    && !empty($field['advanced_revision_view'])
    && $field['advanced_revision_view'] != '--'
    && ($view = revisionreference_get_view($field, $string, $exact_string))) {
      // advanced field : referenceable nodes defined by a view
      // let views.module build the query
      $options = array();
      foreach ($view->result as $item) {
        $options[$item->vid] = _questions_field_revisionreference_item($field, $component, $item);
      }
      return $options;
    }
    else {
      $results = &drupal_static(__FUNCTION__, array());
      $match = 'contains';
      // Create unique id for static cache.
      $cid = $field['field_name'] . ':' . $options['match'] . ':'. ($options['string'] !== '' ? $options['string'] : implode('-', $options['ids'])). ':' . $options['limit'];
      if (!isset($results[$cid])) {
        $references = _questions_field_revisionreference_potential_references_standard($field, $component, $options);
        // Store the results.
        $results[$cid] = !empty($references) ? $references : array();
      }
      return $results[$cid];
    }
  } 


  /**
 *
 */
function _questions_field_revisionreference_item($field, $component, $item, $html = FALSE) {
    if (module_exists('views') && isset($component['advanced_revision_view']) && $component['advanced_revision_view'] != '--' && ($view = revisionreference_get_view($field))) {
      $field_names = array();
      foreach ($view->field as $name => $viewfield) {
        $field_names[] = isset($viewfield->definition['content_field_name']) ? $viewfield->definition['content_field_name'] : $name;
      }
      $output = theme('revisionreference_item_advanced', $item, $field_names, $view);
      if (!$html) {
        $output = html_entity_decode(strip_tags($output), ENT_QUOTES);
      }
    }
    else {
      $output = theme('questions_field_revisionreference_item_simple', $item);
      $output = $html ? check_plain($output) : $output;
    }
    return $output;
  }
  
  /**
   *
   */
  function theme_questions_field_revisionreference_item_simple($item) {
    return $item['rendered'] .' [nid:'. $item['nid'] .'] ' .  date("Y-m-d H:i ",$item['timestamp']) .' by '. $item['uid'] .' [vid:'. $item['vid'] .']';
  }
  


function render_items_list_options(){
      return array('id' => t('Id'),
                  'label' => t('Label'), 
                  'link' => t('Link'), 
                  'text' => t('Text')
                  );
}
  
/**
   *
   */
  function theme_questions_field_display_revisionreference($variables) {
 
  $element = $variables['element'];
  $items = $element['#items'];
  $value = $element['#value'];
  $component = $element['#component'];

  if($component['display_type'] == 'questions_answers_default'){
   return $value;
 }

  $render_as = $component['display_settings']['render'];
  $new_values = array();
 
  foreach($value as $delta => $vid){

    if($render_as == 'id'){
      $new_values[] = $vid;
      } 
      if($render_as == 'label'){
        $new_values[] = $items[$vid]['title']; 
      } 
     if($render_as == 'link'){
      $new_values[] = l($items[$vid]['title'], 'node/'.  $items[$vid]['nid'] .'/revisions/'. $vid . '/view'); 
      } 
      if($render_as == 'text'){
        $new_values[] = theme('questions_field_revisionreference_item_simple', $items[$vid]);
      }
      if($render_as == 'entity'){
      
      } 
    }

  if ( !empty($component['multiple_settings']['multi_type'] ) and $component['multiple_settings']['multi_type'] == 'separator') {
    return implode(filter_xss_admin( $component['multiple_settings']['separator']), $new_values);
  }	 

   return theme('item_list', array(
				'items' => $new_values,
				'title' => null,
		  	'type' => !empty($component['multiple_settings']['multi_type']) ? $component['multiple_settings']['multi_type'] : 'ul'
			));
  
  }


  /**
 * Helper function for _questions_field_revisionreference_potential_references().
 *
 * List of referenceable nodes defined by content types.
 */
function _questions_field_revisionreference_potential_references_standard($field, $component, $options) {
    if (!$component['referenceable_types']) {
      return array();
    }
    foreach ($component['referenceable_types'] as $type => $value) {
      if ($value) {
        $referenceable_types[$type] = $type;
      }
    }
    $query = db_select('node_revision', 'r');
    $query->join('node', 'n', 'n.nid = r.nid');
    $node_nid_alias   = $query->addField('n', 'nid');
    $node_vid_alias   = $query->addField('r', 'vid');
    $query->addField('r', 'timestamp');
    $query->addField('r', 'uid');
    $node_title_alias = $query->addField('r', 'title', 'node_title');
    $node_type_alias  = $query->addField('n', 'type',  'node_type');
    $query->addTag('node_access');
    $query->addMetaData('id', ' _questions_field_revisionreference_potential_references_standard');
    $query->condition('n.type', $referenceable_types, 'IN');
  
    if ($options['string'] !== '') {

      switch ($options['match']) {
        case 'contains':
          $query->condition('n.title', '%' . $options['string'] . '%', 'LIKE');
          break;
        case 'starts_with':
          $query->condition('n.title', $options['string'] . '%', 'LIKE');
          break;
        case 'equals':
          $query->condition('n.title', $options['string']);
          break;
      }
    }
    elseif ($options['ids']) {
      $query->condition('r.vid', $options['ids'], 'IN', $options['ids']);
    }
  
    $query
      ->orderBy($node_vid_alias, 'DESC')
      ->orderBy($node_title_alias)
      ->orderBy($node_type_alias);
  
    if ($options['limit']) {
      $query->range(0, $options['limit']);
    }
    $result = $query->execute();

    $references = array();
    foreach ($result->fetchAll() as $node) {
       $author = user_load($node->uid);
      $references[$node->vid] = array(
        'title'     => $node->node_title,
        'nid'       => $node->nid,
        'vid'       => $node->vid,
        'timestamp' => date("Y-m-d", $node->timestamp),
        'uid'      => $author->name,
        'rendered'  => check_plain($node->node_title),
      );
    }
    return $references;
  }
  

 /**
 *
 */
function questions_field_revisionreference_nid_from_vid($vid) {
    return db_select('node_revision', 'nr')
      ->fields('nr', array('nid'))
      ->condition('vid', $vid)
      ->execute()
      ->fetchField();
  }
  
  /**
   *
   */
  function questions_field_revisionreference_vid_is_current($vid, $nid = NULL) {
    static $max_vid_for_nids = array();
    if ($nid == NULL) {
      $nid = questions_field_revisionreference_nid_from_vid($vid);
    } 
    
    if (!isset($max_vid_for_nids[$nid])) {
      $max_vid_for_nids[$nid] = db_select('node', 'n')
        ->fields('n', array('vid'))
        ->condition('nid', $nid, '=')
        ->execute()
        ->fetchField();
    }
    return ($max_vid_for_nids[$nid] == $vid);
  } 