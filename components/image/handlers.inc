<?php
/**
 * @file
 * questions_field select image module select image component.
 */
/**
 * Implements _questions_field_defaults_component().
 */

function _questions_field_defaults_image()
	{
	return array(
		'name' => '',
		'form_key' => NULL,
		'mandatory' => 0,
		'pid' => 0,
		'weight' => 0,
		'value' => '',
		'image_style_settings' => array(),
		'extra' => array(
			'default_image' => '',
			'multiple' => NULL,
			'title_display' => 0,
			'description' => '',
			'custom_keys' => FALSE,
			'private' => FALSE,
			'image_style' => 'thumbnail',
		) ,
	);
}


 
function _questions_field_field_image($component)
	{
	$data = array(
		'type',
		'cid',
		'name',
		'form_key',
		'extra' => array(
			'default_image',
			'image_style',
			'num_images',
			'multiple'
		)
	);
	$compnent_returned = array();
	foreach($data as $key => $value)
		{
		if ($key === 'extra')
			{
			foreach($value as $extra_key)
				{
				if (isset($component[$key][$extra_key])) $compnent_returned[$key][$extra_key] = $component[$key][$extra_key];
				}
			}
		elseif (isset($component[$value])) $compnent_returned[$value] = $component[$value];
		}

	return $compnent_returned;
	}

function _questions_field_instance_image($component)
	{
	$exclude = array(
		'type',
		'cid',
		'name',
		'form_key',
		'extra' => array(
			'default_image',
			'image_style',
			'num_images',
			'multiple'
		)
	);
	foreach($exclude as $key => $value)
		{
		if ($key === 'extra')
			{
			foreach($value as $extra_key)
				{
				if (isset($component[$key][$extra_key])) unset($component[$key][$extra_key]);
				}
			}
		elseif (isset($component[$value])) unset($component[$value]);
		}

	return $component;
	}

/**
 * Implements _questions_field_theme_component().
 */

function _questions_field_theme_image()
	{
	return array(
		'questions_field_display_image' => array(
			'render element' => 'element',
			'path' => drupal_get_path('module', 'questions_field') ,
			'file' => 'components/image/handlers.inc',
		) ,
	);
	}

/**
 * FIXME: Find a another way to just add "questions-field-component-select-image" class to element
 */

function theme_questions_field_element_image($variables)
	{

	// Ensure defaults.

	$variables['element']+= array(
		'#title_display' => 'before',
	);
	$element = $variables['element'];

	// All elements using this for di
	// All elements using this for display only are given the "display" type.

	if (isset($element['#format']) && $element['#format'] == 'html')
		{
		$type = 'display';
		}
	  else
		{
		$type = (isset($element['#type']) && !in_array($element['#type'], array(
			'markup',
			'textfield',
			'questions_field_email',
			'questions_field_number'
		))) ? $element['#type'] : $element['#questions_field_component']['type'];
		}

	$nested_level = 0;
	$parents = str_replace('_', '-', implode('--', array_slice($element['#parents'], $nested_level)));
	$wrapper_classes = array(
		'form-item',
		'questions-field-component',
		'questions-field-component-' . $type,
		'questions-field-component-image',
	);
	if (isset($element['#title_display']) && strcmp($element['#title_display'], 'inline') === 0)
		{
		$wrapper_classes[] = 'questions-field-container-inline';
		}

	$output = '<div class="' . implode(' ', $wrapper_classes) . '" id="questions-field-component-' . $parents . '">' . "\n";

	// If #title is not set, we don't display any label or required marker.

	if (!isset($element['#title']))
		{
		$element['#title_display'] = 'none';
		}

	$prefix = isset($element['#field_prefix']) ? '<span class="field-prefix">' . filter_xss($element['#field_prefix']) . '</span> ' : '';
	$suffix = isset($element['#field_suffix']) ? ' <span class="field-suffix">' . filter_xss($element['#field_suffix']) . '</span>' : '';
	switch ($element['#title_display'])
		{
	case 'inline':
	case 'before':
	case 'invisible':
		$output.= ' ' . theme('form_element_label', $variables);
		$output.= ' ' . $prefix . $element['#children'] . $suffix . "\n";
		break;

	case 'after':
		$output.= ' ' . $prefix . $element['#children'] . $suffix;
		$output.= ' ' . theme('form_element_label', $variables) . "\n";
		break;

	case 'none':
	case 'attribute':

		// Output no label and no required marker, only the children.

		$output.= ' ' . $prefix . $element['#children'] . $suffix . "\n";
		break;
		}

	if (!empty($element['#description']))
		{
		$output.= ' <div class="description">' . $element['#description'] . "</div>\n";
		}

	$output.= "</div>\n";
	return $output;
	}

/**
 * Implements _questions_field_edit_component().
 */

function _questions_field_edit_image($component, $form, $form_state)
	{
	// Create form
	$element = array();

	$element['#tree'] = TRUE;

	$element['extra']['image_style'] = array(
		'#type' => 'select',
		'#title' => t('Image style') ,
		'#default_value' => $component['extra']['image_style'],
		'#description' => t('The image style used to display images.') ,
		'#options' => image_style_options() ,
		'#weight' => 0,
	);

	$element['extra']['default_image'] = array(
		'#type' => 'managed_file',
		'#title' => t('Default image') ,
		'#description' => t('Allowed extensions: gif png jpg jpeg') ,
		'#default_value' => $component['extra']['default_image'],
		'#upload_location' => file_build_uri('questions_field/' . $component['cid'] . '/images') ,
		'#upload_validators' => array(
			'file_validate_extensions' => array(
				'gif png jpg jpeg'
			) ,
			'file_validate_size' => array(
				file_upload_max_size()
			) ,
		) ,
		
		'#element_validate' => array(
			'_questions_field_edit_image_managed_file_validate'
		) ,
	);

 
	$element['extra']['multiple'] = array(
		'#type' => 'checkbox',
		'#title' => t('Multiple') ,
		'#default_value' => $component['extra']['multiple'],
		'#description' => t('Check this option if the user should be allowed to choose multiple values.') ,
		'#weight' => 0,
	);

	$element['#submit'][] = '_questions_field_edit_image_managed_file_submit';
	return $element;
	}


/**
 * Implements _questions_field_render_component().
 */

function _questions_field_render_image($component, $value = NULL, $entity, $filter = TRUE, $widget_arguments = array())
	{
 
	$element['#element_validate'] = array(
		'_questions_field_image_edit_validate'
	);
	list($form, $form_state, $field, $instance, $langcode, $items, $delta, $element_widget) = $widget_arguments;
	$field_name = $field['field_name'];
	$cid = $component['cid'];
	$_images = null;
 

	if(!empty($form_state['input'][$field_name][$langcode][$delta][$cid]['items']['images']))
			{
				 
				$_images = $form_state['input'][$field_name][$langcode][$delta][$cid]['items']['images'];
				$fids = _fetch_fids($_images);
				$value = implode(',', $fids);
	 	}else{
			 if(!empty($value['items']['images'])){
				$value = null;
			 }
		 }
 

	$images = !empty($value) ? explode(',', $value) : array();
	$num_images = ($component['extra']['multiple']) ? count($images) : 0;
	
	$id_component = 'questions-field-images-' . $field_name . '-' . $cid . '-fieldset-wrapper';
	$prefix_attributes['id'] = $id_component;


	if(!empty($component['extra']['wrapper_classes'])){
    	$prefix_attributes['class'] = $component['extra']['wrapper_classes'];
	}

	// update component in the element because
	// sometimes it updated by form_state
	$element['#questions_field_component'] = $component;
	$element['#weight'] = $component['weight'];
	$element['#tree'] = TRUE;
	$element['items'] = array(
		'#type' => 'fieldset',
		'#title' => t($component['question']),
		'#prefix' => '<div '.drupal_attributes($prefix_attributes).'>',
		'#suffix' => '</div>',
	);
 
	if(!empty($component['extra']['css_classes'])){
			$element['items']['#attributes']['class'][] = $component['extra']['css_classes'];
	 }

	for ($i = 0; $i <= $num_images; $i++)
		{
		$element['items']['images'][$i] = array(
			'#type' => 'container',
		);
		$element['items']['images'][$i]['src'] = array(
			'#type' => 'managed_file',
			'#title' => t('Picture') ,
			'#description' => t('Allowed extensions: gif png jpg jpeg') ,
			'#default_value' => (isset($images[$i]) ? $images[$i] : '') ,
			'#upload_location' => file_build_uri('questions_field/' . $component['cid'] . '/images') ,
			'#upload_validators' => array(
				'file_validate_extensions' => array(
					'gif png jpg jpeg'
				) ,
				'file_validate_size' => array(
					file_upload_max_size()
				) ,
			) ,
			'#element_validate' => array(
				'_questions_field_image_managed_file_validate'
			) ,
      '#preview' => true,
      '#prefix' => '',
      '#suffix' => '',
      '#component' => $component
		);
		}

	if ($component['extra']['multiple'])
		{
		$element['items']['add_image'] = array(
			'#type' => 'submit',
			'#value' => t('Add image') ,
			'#name' => 'add_image_' . $field_name . '_' . $cid,
			'#submit' => array(
				'_questions_field_add_image'
			) ,
			'#ajax' => array(
				'callback' => '_questions_field_add_image_callback',
				'wrapper' => $id_component,
			) ,
			'#limit_validation_errors' => array()
		);
		}


	return $element;
	}


/**
 * Callback for both ajax-enabled buttons.
 *
 * Selects and returns the fieldset with the images in it.
 */

function _questions_field_add_image_callback($form, &$form_state)
	{
	$parents = $form_state['triggering_element']['#array_parents'];
	$button_key = array_pop($parents);
	$items = drupal_array_get_nested_value($form, $parents);

	// Image preview @TODO => not here

	/*
	if(!empty($form['items']['images'][0]['#file']->uri)) {
	$form['items']['images'][0]['filename']['#markup'] = theme(
	'image_style',
	array(
	'style_name' => 'thumbnail',
	'path' => $form['items']['images'][0]['#file']->uri,
	)
	);
	}

	*/
	return $items;
	}

/**
 * Submit handler for the "add-image-option" button.
 *
 * Increments the max counter and causes a rebuild.
 */

function _questions_field_add_image($form, &$form_state)
	{
	$form_state['rebuild'] = TRUE;
	}


/**
 * Implements _questions_field_submit_component().
 */
function _questions_field_submit_image($component, $value) {

 //set values
 if(!empty($value['items']['images'])){
   $fids = _fetch_fids($value['items']['images']);
   return implode(',', $fids);
 }

 return '';
}
/**
 * Set the appropriate questions field values when using the options element module.
 */

function _questions_field_image_edit_validate($element, &$form_state, $form)
	{

	if ($form_state['triggering_element']['#name'] == 'op')
		{
		list($entity_id, $entity_vid, $entity_bundle) = entity_extract_ids($form['#entity_type'], $form['#entity']);
		$parents = $element['#array_parents'];
		$parents[] = 'items';

		// Save new files as permanent that exist in values
		$items = &drupal_array_get_nested_value($form_state['values'], $parents);
		$count = 0;
		$fids = array();
		foreach($items['images'] as $item)
			{
			if (!empty($item['src']['fid']))
				{
				$file = file_load($item['src']['fid']);
				$file->status = FILE_STATUS_PERMANENT;
				file_save($file);

				// On insert entity use hook_field_insert()
				// On update use the following code in order to add usage

				if ($entity_id)
					{
					file_usage_add($file, 'questions_field', $form['#entity_type'], $entity_id);
					}

				}
			}


		// Image to delete
		if (!empty($form_state['images_to_delete']))
			{
			foreach($form_state['images_to_delete'] as $fid)
				{
				if ($file = file_load($fid))
					{

					// On insert entity use hook_field_insert()
					// On update use the following code in order to add usage

					if ($entity_id)
						{
						file_usage_delete($file, 'questions_field', $form['#entity_type'], $entity_id);
						}

					file_delete($file);
					}
				}
			}
		}
	}

function _fetch_fids($items)
	{
	$fids = array();
	if (!empty($items))
		{
		$count = 0;
		$fids = array();
		foreach($items as $item)
			{
			if (!empty($item['src']['fid']))
				{
				$fids[] = $item['src']['fid'];
				}
			}
		}

	return $fids;
	}

/**
 * Implements _questions_field_display_component().
 */

function _questions_field_display_image($component, $value, $weight = 0, $format = 'html', $arguments = array())
	{

    list($entity_type, $entity, $field, $instance, $langcode, $items, $display ) = $arguments;
 
if(empty($value)){
	$value = (!empty($component['extra']['default_image']) ?  $component['extra']['default_image'] : '' );
}

if (!empty($component['multiple_settings']))
		{

		$all_values = explode(',', $value);
		if ($component['multiple_settings']['delta_reversed'])
			{
			$all_values = array_reverse($all_values);
			}

		$delta_limit = $component['multiple_settings']['delta_limit'];
		$offset = intval($component['multiple_settings']['delta_offset']);

		// We should only get here in this case if there's an offset, and
		// in that case we're limiting to all values after the offset.

		if ($delta_limit == 'all')
			{
			$delta_limit = count($all_values) - $offset;
			}

		// Determine if only the first and last values should be shown

		$delta_first_last = $component['multiple_settings']['delta_first_last'];
		$new_values = array();
		for ($i = 0; $i < $delta_limit; $i++)
			{
			$new_delta = $offset + $i;
			if (isset($all_values[$new_delta]))
				{

				// If first-last option was selected, only use the first and last values

				if (!$delta_first_last

				// Use the first value.

				 || $new_delta == $offset

				// Use the last value.

				 || $new_delta == ($delta_limit + $offset - 1))
					{
					if ($format == 'html') $all_values[$new_delta] = check_plain($all_values[$new_delta]);

        	$file = file_load($all_values[$new_delta]);
        		  $new_value = _render_image($component, $file, $entity_type, $entity);
		     	$new_values[] = drupal_render($new_value);
					}
				}
			}

		if ($component['multiple_settings']['multi_type'] == 'separator')
			{
			foreach($new_values as $delta => $new_value)
				{
				$new_values[$delta] = isset($options[$new_value]) ? $options[$new_value] : $new_value;
				}

			$new_values = implode(filter_xss_admin($component['multiple_settings']['separator']) , $new_values);
			}
		  else
			{
			$new_values = theme('item_list', array(
				'items' => $new_values,
				'title' => null,
				'type' => $component['multiple_settings']['multi_type']
			));
			}

		$value = $new_values;
		}
	  else
		{
	 $new_values = '';
 foreach(explode(',', $value) as $delta => $fid){
		   $file = file_load($fid);
		   $new_value = _render_image($component, $file, $entity_type, $entity);
       		$new_values .= drupal_render($new_value);
		}
      $value = $new_values;
  }

	return array(
		'#title' => t($component['question']) ,
  	    '#weight' => $weight,
		'#theme' => 'questions_field_display_image',
		'#theme_wrappers' => $format == 'html' ? array(
			'questions_field_element_image'
		) : array(
			'questions_field_element_text'
		) ,
		'#format' => $format,
		'#questions_field_component' => $component,
		'#value' => $value,
		'#translatable' => array(
			'title'
		) ,
		'#parents' => array() ,
	);
	}


function _render_image($component, $file, $entity_type, $entity){

  $file = (array) $file;
  $image_link = !empty($component['image_style_settings']['image_link']) ? $component['image_style_settings']['image_link'] : '';
  $image_style = !empty($component['image_style_settings']['image_style']) ? $component['image_style_settings']['image_style'] : '';
  // Check if the formatter involves a link.
  if ($image_link == 'content') {
    $uri = entity_uri($entity_type, $entity);
  }
  elseif ($image_link == 'file') {
    $link_file = TRUE;
  }

   if (isset($link_file)) {
      $uri = array(
        'path' => file_create_url($file['uri']),
        'options' => array(),
      );
    }

    return  array(
                '#theme' => 'image_formatter',
                '#item' => (array)$file,
                '#image_style' => $image_style,
                '#path' => isset($uri) ? $uri : '',
           );
}
/**
 * Format the text output for this component.
 */

function theme_questions_field_display_image($variables)
	{

	$element_form = $variables['element'];
	$component = $element_form['#questions_field_component'];

	return  $element_form['#value'];
	}

/**
 * Implements _questions_field_analysis_component().
 */

function _questions_field_analysis_image($component, $sids = array() , $single = FALSE)
	{
	}

/**
 * Implements _questions_field_table_component().
 */

function _questions_field_table_image($component, $value)
	{

	// Convert submitted 'safe' values to un-edited, original form.

	$options = _questions_field_image_options($component, TRUE);
	$value = (array)$value;
	$items = array();

	// Set the value as a single string.

	foreach($value as $option_value)
		{
		if ($option_value !== '')
			{
			if (isset($options[$option_value]))
				{
				$items[] = filter_xss($options[$option_value]);
				}
			}
		}

	return implode('<br />', $items);
	}

/**
 * Implements _questions_field_csv_headers_component().
 */

function _question_field_csv_headers_image($component, $export_options)
	{
	$headers = array(
		0 => array() ,
		1 => array() ,
		2 => array() ,
	);
	if ($component['extra']['multiple'] && $export_options['select_format'] == 'separate')
		{
		$headers[0][] = '';
		$headers[1][] = $component['name'];
		$options = _questions_field_image_options($component, FALSE);
		$count = 0;
		foreach($options as $key => $item)
			{

			// Empty column per sub-field in main header.

			if ($count != 0)
				{
				$headers[0][] = '';
				$headers[1][] = '';
				}

			if ($export_options['select_keys'])
				{
				$headers[2][] = $key;
				}
			  else
				{
				$headers[2][] = $item;
				}

			$count++;
			}
		}
	  else
		{
		$headers[0][] = '';
		$headers[1][] = '';
		$headers[2][] = $component['name'];
		}

	return $headers;
	}

/**
 * Implements _questions_field_csv_data_component().
 */

function _questions_field_csv_data_image($component, $export_options, $value)
	{
	$options = _questions_field_image_options($component, FALSE);
	$return = array();
	if ($component['extra']['multiple'])
		{
		foreach($options as $key => $item)
			{
			$index = array_search($key, (array)$value);
			if ($index !== FALSE)
				{
				if ($export_options['select_format'] == 'separate')
					{
					$return[] = 'X';
					}
				  else
					{
					$return[] = $export_options['select_keys'] ? $key : $item;
					}

				unset($value[$index]);
				}
			elseif ($export_options['select_format'] == 'separate')
				{
				$return[] = '';
				}
			}
		}
	  else
		{
		$key = !empty($value) ? current($value) : '';
		if ($export_options['select_keys'])
			{
			$return = $key;
			}
		  else
			{
			$return = isset($options[$key]) ? $options[$key] : $key;
			}
		}

	if ($component['extra']['multiple'] && $export_options['select_format'] == 'compact')
		{
		$return = implode(',', (array)$return);
		}

	return $return;
	}

/**
 * Convert an array of images into text.
 */

function _questions_field_images_to_text($items)
	{
	$options = array();
	foreach($items as $key => $value)
		{
		if (!empty($value['src']['fid']))
			{
			$options[] = $value['src']['fid'];
			}
		}

	return implode('||', $options);
	}

/**
 * Convert an array of options into text.
 */

function _questions_field_image_options_to_text($items)
	{
	$output = '';
	foreach($items as $key => $item)
		{
		if ($file = file_load($item['src']['fid']))
			{
			$output.= $file->fid . '||' . $file->filename . "\n";
			}
		}

	return $output;
	}

function _questions_field_schema_column_image()
	{
	return array(
		'description' => 'Image.',
		'type' => 'varchar',
		'length' => 32,
		'not null' => FALSE,
		'default' => '',
	);
	}

function _questions_field_data_property_info_image($component)
	{
	return array(
		'label' => $component['name'],
		'type' => 'text',
		'getter callback' => 'entity_property_verbatim_get',
		'setter callback' => 'entity_property_verbatim_set',
	);
	}
