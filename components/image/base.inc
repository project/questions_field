<?php

function _questions_field_edit_image_managed_file_validate(&$element, &$form_state, $form)
{

if ($form_state['triggering_element']['#name'] == $element['remove_button']['#name'])
	{
	$form_state['images_to_delete'] = $element['#file']->fid;	
	}

  form_set_value($element,  $element['fid']['#value'], $form_state);
}

function _questions_field_edit_image_managed_file_submit($form, $form_state) {

	if(!empty($form_state['images_to_delete'])){
		$file = file_load($form_state['images_to_delete']);
		if($file){
		file_delete($file);
		}   
	}

	if(!empty($form_state['values']['extra']['default_image'])){
		$fid = $form_state['values']['extra']['default_image'];
		$file = file_load($fid);
		$file->status = FILE_STATUS_PERMANENT;
		file_save($file);
	}
}

/**
 * Callback validate for managed file element
 * @see _questions_field_edit_image()
 */

function _questions_field_image_managed_file_validate($element, &$form_state, $form)
	{
	$parents = $element['#array_parents'];
	list($field_name, $lang, $delta, $cid) = $parents;

	// Remove images
	if ($form_state['triggering_element']['#name'] == $element['remove_button']['#name'])
		{
		$form_state['images_to_delete'][] = $element['#file']->fid;
		}

	// Add images
	if ($form_state['triggering_element']['#name'] == $element['upload_button']['#name'])
		{
		$item = & drupal_array_get_nested_value($form_state['input'], $parents);
		if(!empty($element['#file'])){
            $item['fid'] = $element['#file']->fid;
	  	}
   }

	}
