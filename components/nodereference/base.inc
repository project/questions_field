<?php
/**
 * Retrieve a pipe delimited string of autocomplete suggestions
 */
function questions_field_nodereference_autocomplete($entity_type, $bundle_name, $field_name, $component_id, $string = '') {
    // If the request has a '/' in the search text, then the menu system will have
    // split it into multiple arguments, recover the intended $string.
    $args = func_get_args();
    // Shift off the $entity_type argument.
    array_shift($args);
    // Shift off the $bundle_name argument.
    array_shift($args);
    // Shift off the $field_name argument.
    array_shift($args);
    // Shift off the $component_id argument.
    array_shift($args);

    $string = implode('/', $args);
  
    $field = field_info_field($field_name);
    $component = questions_field_get_component_load_single($entity_type, $bundle_name, $field_name, $component_id);
 
    if(!empty($component['extra']['multiple']) and $component['extra']['multiple']){
      // The user enters a comma-separated list of tags.
      // We only autocomplete the last tag.
      $tags_typed = drupal_explode_tags($string);
      $tag_last = drupal_strtolower(array_pop($tags_typed));
      $string = $tag_last;
      if (!empty($tag_last)) {
        $prefix = count($tags_typed) ? implode(', ', $tags_typed) . ', ' : '';
      }
    }
 
    $options = array(
      'string' => $string,
      // @todo broken - this is in $field['widget']['settings']['autocomplete_match'] - we'd need the $instance.
      'match' => isset($component['autocomplete_match']) ? $component['autocomplete_match'] : 'contains',
      'limit' => 10,
    );
    $references = _questions_field_nodereference_potential_references($field, $component, $options);
    $matches = array();
    foreach ($references as $id => $row) {
      // Markup is fine in autocompletion results (might happen when rendered
      // through Views) but we want to remove hyperlinks.
      $suggestion = preg_replace('/<a href="([^<]*)">([^<]*)<\/a>/', '$2', $row['rendered']);
      // Add a class wrapper for a few required CSS overrides.
      $matches[$row['title'] . " [nid:$id]"] = '<div class="reference-autocomplete">' . $suggestion . '</div>';
    }
  
    drupal_json_output($matches);   
  }
 

  function questions_field_nodereference_autocomplete_access($entity_type, $bundle_name, $field_name, $component_id) {
      return user_access('access content') && ($field = field_info_field($field_name)) && field_access('view', $field, $entity_type) && field_access('edit', $field, $entity_type);
  }
 
 

/**
 * Value callback for a nodereference autocomplete element.
 *
 * Replace the node vid with a node title.
 */
function questions_field_nodereference_autocomplete_value($element, $input = FALSE, $form_state) {
    if ($input === FALSE) {
      // We're building the displayed 'default value': expand the raw nid into
      // "node title [nid:n]".
      $nids = explode(',', isset($element['#default_value']) ? $element['#default_value'] : '');
      if (!empty($nids)) {
        $q = db_select('node', 'n');
        $q->fields('n', array('title', 'nid'));
        $q->addTag('node_access')
          ->condition('n.nid', $nids, 'IN');
        $result = $q->execute();
          $value = '';
          foreach($result->fetchAll() as $record){
            if(!empty($value)) $value .=',';
            $value .= $record->title .' [nid:'. $record->nid .']';
          }
        return $value;
      }
    }
  }

  /**
 * Node Reference Options.
 *
 * Builds a list of referenceable nodes suitable for the '#option' FAPI
 * property.
 *
 * Warning: the function does NOT take care of encoding or escaping the node
 * titles. Proper massaging needs to be performed by the caller, according to
 * the destination FAPI '#type' (radios / checkboxes / select).
 *
 * @param array $field
 *   The field definition.
 * @param bool $flat
 *   Whether optgroups are allowed.
 *
 * @return array
 *   An array of referenceable node titles, keyed by node id. If the $flat
 *   parameter is TRUE, the list might be nested by optgroup first.
 *
 * @codingStandardsIgnoreStart
 */ 
function qf_node_reference_options_list($field, $component, $flat = TRUE) {
  // @codingStandardsIgnoreEnd
  $references = _questions_field_nodereference_potential_references($field, $component);

  $options = array();
  foreach ($references as $key => $value) {
    // The label, displayed in selects and checkboxes/radios, should have HTML
    // entities unencoded. The widgets (core's options.module) take care of
    // applying the relevant filters (strip_tags() or filter_xss()).
    $label = html_entity_decode($value['rendered'], ENT_QUOTES);
    if (empty($value['group']) || $flat) {
      $options[$key] = $label;
    }
    else {
      // The group name, displayed in selects, cannot contain tags, and should
      // have HTML entities unencoded.
      $group = html_entity_decode(strip_tags($value['group']), ENT_QUOTES);
      $options[$group][$key] = $label;
    }
  }

  return $options;
}



  function _qf_node_list($referenceable_types, $vids = array()){
    $query = db_select('node_node', 'r');
    $query->join('node', 'n', 'n.nid = r.nid');
    $query->join('users', 'u', 'u.uid = r.uid');
    $query->fields('r', array('title', 'timestamp', 'nid', 'vid', 'uid'));
    $query->fields('u', array('name'));
    $query->fields('n', array('title'));

    if(!empty($vids)) {
      $query->condition('r.vid', $vids, 'IN');
    }

    $query->condition('n.type', $referenceable_types, 'IN');
    $result = $query->execute();
  
    $records = $result->fetchAll();
    return $records;
  }


  /**
   * Validate an list element.
   *
   * Remove the wrapper layer and set the right element's value.
   */
  function questions_field_nodereference_list_validate($element, &$form_state) {
     
    //skip the none value
    if($element['#value'] == '_none'){
      $element['#value'] = null;
        form_set_value($element, $element['#value'] , $form_state);
        return true;
      }

    $value = $element['#value'];
    if(!empty($element['#value']) and is_array($element['#value'])){
      $element['#value'] = implode(',', $element['#value']);
    }

     $is_valid = questions_field_nodereference_textfield_validate($element, $form_state);

    if($is_valid){
      //Remove uncheked values
      form_set_value($element, $value, $form_state);
     }
    return $is_valid;
  }

  /**
   * Validate an textfield element.
   *
   * Remove the wrapper layer and set the right element's value.
   */
  function questions_field_nodereference_textfield_validate($element, &$form_state) {
      
    list($entity_type, $bundle_name, $field_name,  $component_id) = array_values($element['#arguments']);
 
    $field = field_info_field($field_name);
    $component = questions_field_get_component_load_single($entity_type, $bundle_name, $field_name, $component_id);
    $value = $element['#value'];
    $vid = NULL;
    if (!empty($value)) {
 
        if(preg_match('/^[0-9]+(,[0-9]+)*$/', $value)) {

        //First check if valid value
        if(empty($component['extra']['multiple']) and strpos($value, ',') !== false){
          form_error($element, t('%name(%delta) component You cannot entered multiple values.', array('%name' => $element['#title'], '%delta' => $element['#delta'])));
          return false;
        }

        }else{
          form_error($element, t('%name(%delta) component Invalid value.', array('%name' => $element['#title'], '%delta' => $element['#delta'])));
          return false;
        }

       //After make sure format is valid, then check the value if exist
       $value = explode(',', $value);

       if (!$component['referenceable_types']) {
        return array();
      }
      foreach ($component['referenceable_types'] as $type => $_value) {
        if ($_value) {
          $referenceable_types[$type] = $type;
        }
      }
      $nids_not_exist =  qf_check_node($value, $referenceable_types);
      if(!empty($nids_not_exist)){
        form_error($element, t('%name(%delta) component the value <i>@value</i> is not exist.', array('%name' => $element['#title'], 
                                                                                                '%delta' => $element['#delta'], 
                                                                                                '@value' => implode(',', $nids_not_exist))
                                                                                              )
                                                                                            );
          return false;
        }


      }

      return true;
  }

  /**
   * @param $nids node Id
   * @param $referenceable_types node types
   * Check the $nids given if exist or not.
   * return list of nids that not exist, return empty in case all exist
  */
  function qf_check_node($nids, $referenceable_types){
    $query = db_select('node', 'n');
    $query->fields('n', array('nid'));
    $query->condition('n.type', $referenceable_types, 'IN');
    $query->condition('n.nid', $nids, 'IN');
    $result = $query->execute(); 
    $nids_fetched = array();
    while($record = $result->fetchAssoc()) {
      $nids_fetched[] = trim($record['nid']);
    }

    $nids_not_exist = array();
    foreach($nids as $nid){
    if(!in_array($nid, $nids_fetched)){
      $nids_not_exist[] = $nid;
      }
    }
   return $nids_not_exist;
  }

  
  /**
   * Validate an autocomplete element.
   *
   * Remove the wrapper layer and set the right element's value.
   */
  function questions_field_nodereference_autocomplete_validate($element, &$form_state) {
      
 
    list($entity_type, $bundle_name, $field_name,  $component_id) = $element['#arguments'];
 
    $field = field_info_field($field_name);
    $component = $element['#questions_field_component'];
    $values = $element['#value'];
     
   
    $nids_values = array();
    if (!empty($values)) {

      if(!$component['extra']['multiple'] and strpos($values, ',') !== false){
        form_error($element, t('%name(%delta) component You cannot entered multiple values.', array('%name' => $element['#title'], '%delta' => $element['#delta'])));
        return false;
      }else{
         $values = explode(',', $values);
      }

     
     foreach($values as $delta => $value){
      $nid = NULL;

        // Check whether we have an explicit "[nid:n]" input.
        preg_match('/^(?:\s*|(.*) )?\[\s*nid\s*:\s*(\d+)\s*\]$/', $value, $matches);
        if (!empty($matches)) {
          // Explicit nid. Check that the 'title' part matches the actual title for
          // the nid.
          list(, , $nid) = $matches;
          if (!empty($nid)) {
            $real_title = db_select('node', 'n')
              ->fields('n', array('title'))
              ->condition('n.nid', $nid)
              ->execute()
              ->fetchField();
            if (empty($real_title)) {
              form_error($element, t('%name: No node found. Please check your selection.', array('%name' =>  $element['#title'])));
            }
          }
        }
        else {
          // No explicit nid (the submitted value was not populated by autocomplete
          // selection). Get the nid of a referencable node from the entered title.
          $options = array(
            'string' => $value,
            'match' => 'equals',
            'limit' => 1,
          );
          $references = _questions_field_nodereference_potential_references($field, $component, $options);
          if ($references) {
            // @todo The best thing would be to present the user with an
            // additional form, allowing the user to choose between valid
            // candidates with the same title. ATM, we pick the first
            // matching candidate...
            $nid = key($references);
          }
          else {
            form_error($element, t('%name(%delta) component the value <b><i>@value</i></b> title mismatch. Please check your selection.', array('%name' => $element['#title'], 
            '%delta' => $element['#delta'], 
            '@value' => $value)
                )
             );
             return false;
          }
        }

      $nids_values[] = $vid;


    }
    form_set_value($element, implode(',', $nids_values), $form_state);
   }
 
    return $element;
  }



 
/**
 * Fetch an array of all candidate referenced nodes,
 * for use in presenting the selection form to the user.
 */
function _questions_field_nodereference_potential_references($field, $component, $options = array()) {
   // @codingStandardsIgnoreEnd
  // Fill in default options.
  $options += array(
    'string' => '',
    'match' => 'contains',
    'ids' => array(),
    'limit' => 25,
  );

  $results = &drupal_static(__FUNCTION__, array());

  // Create unique id for static cache.
  $cid = $field['field_name'] .':'. $component['cid']. ':' . $options['match'] . ':'
    . ($options['string'] !== '' ? $options['string'] : implode('-', $options['ids']))
    . ':' . $options['limit'];
  if (!isset($results[$cid])) {
    $references = FALSE;
  //  @TODO integrate with views
    // if (module_exists('views') && !empty($field['settings']['view']['view_name'])) {
    //   $references = _node_reference_potential_references_views($field, $options);
    // }

    if ($references === FALSE) {
      $references = _questions_field_nodereference_potential_references_standard($field, $component, $options);
    }

    // Store the results.
    $results[$cid] = !empty($references) ? $references : array();
  }

  return $results[$cid];
  } 


  /**
 *deprecated should remove it
 */
function _questions_field_nodereference_item($field, $component, $item, $html = FALSE) {
    if (module_exists('views') && isset($component['advanced_node_view']) && $component['advanced_node_view'] != '--' && ($view = nodereference_get_view($field))) {
      $field_names = array();
      foreach ($view->field as $name => $viewfield) {
        $field_names[] = isset($viewfield->definition['content_field_name']) ? $viewfield->definition['content_field_name'] : $name;
      }
      $output = theme('nodereference_item_advanced', $item, $field_names, $view);
      if (!$html) {
        $output = html_entity_decode(strip_tags($output), ENT_QUOTES);
      }
    }
    else {
      $output = theme('questions_field_nodereference_item_simple', $item);
      $output = $html ? check_plain($output) : $output;
    }
    return $output;
  }
  
  /**
   *
   */
  function theme_questions_field_nodereference_item_simple($item) {
    return $item['title'] .' [nid:'. $item['nid'] . '] '. t('Created') . ' ' .  date("Y-m-d H:i ",$item['created']) .' by '. $item['uid'];
  }
  

/**
 * Options used to display items in view entity
 */
function render_nodereference_items_list_options(){
      return array('id' => t('Id'),
                  'label' => t('Label'), 
                  'link' => t('Link'), 
                  'text' => t('Text'),
                  'entity' => t('Entity')
                  );
}
  
/**
   *
   */
  function theme_questions_field_display_nodereference($variables) {
 
  $element = $variables['element'];
  $values = $element['#value'];
  $component = $element['#component'];
  
  if($component['display_type'] == 'questions_answers_default'){
   return $values;
 }

  $render_as = $component['display_settings']['render'];
  $new_values = array();
    if($render_as != 'id'){
      $values = node_load_multiple($values);
    }
  foreach($values as $key => $record){

    if($render_as == 'id'){
      $new_values[] = $record;
      } 
      if($render_as == 'label'){
        $new_values[] = entity_label('node', $record);
      } 
     if($render_as == 'link'){
      $new_values[] = l($record->title, 'node/'.  $record->nid); 
      } 
      if($render_as == 'text'){
        $new_values[] = theme('questions_field_nodereference_item_simple',(array)$record);
      }
      if($render_as == 'entity'){
        $node_view = node_view($record);
        $new_values[] = drupal_render($node_view);
      } 
    }

  if ( !empty($component['multiple_settings']['multi_type'] ) and $component['multiple_settings']['multi_type'] == 'separator') {
    return implode(filter_xss_admin( $component['multiple_settings']['separator']), $new_values);
  }	 

   return theme('item_list', array(
				'items' => $new_values,
				'title' => null,
		  	'type' => !empty($component['multiple_settings']['multi_type']) ? $component['multiple_settings']['multi_type'] : 'ul'
			));
  
  }


  /**
 * Helper function for _questions_field_nodereference_potential_references().
 *
 * List of referenceable nodes defined by content types.
 */
function _questions_field_nodereference_potential_references_standard($field, $component, $options) {
    if (!$component['referenceable_types']) {
      return array();
    }
    foreach ($component['referenceable_types'] as $type => $value) {
      if ($value) {
        $referenceable_types[$type] = $type;
      }
    }



  $query = db_select('node', 'n');

  if (!user_access('bypass node access')) {
    // If the user is able to view their own unpublished nodes, allow them to
    // see these in addition to published nodes. Check that they actually have
    // some unpublished nodes to view before adding the condition.
    if (user_access('view own unpublished content') && $own_unpublished = db_query('SELECT nid FROM {node} WHERE uid = :uid AND status = :status', array(':uid' => $GLOBALS['user']->uid, ':status' => NODE_NOT_PUBLISHED))->fetchCol()) {
      $query->condition(db_or()
        ->condition('n.status', NODE_PUBLISHED)
        ->condition('n.nid', $own_unpublished, 'IN')
      );
    }
    else {
      // If not, restrict the query to published nodes.
      $query->condition('n.status', NODE_PUBLISHED);
    }

    $query->addTag('node_access');
  }

  $query->addField('n', 'nid');
  $node_title_alias = $query->addField('n', 'title', 'node_title');
  $node_type_alias = $query->addField('n', 'type', 'node_type');
  $query->addMetaData('id', ' _node_reference_potential_references_standard')
    ->addMetaData('field', $field)
    ->addMetaData('options', $options);

  if (is_array($referenceable_types)) {
    $query->condition('n.type', $referenceable_types, 'IN');
  }

  if ($options['string'] !== '') {
    switch ($options['match']) {
      case 'contains':
        $query->condition('n.title', '%' . $options['string'] . '%', 'LIKE');
        break;

      case 'starts_with':
        $query->condition('n.title', $options['string'] . '%', 'LIKE');
        break;

      case 'fuzzy':
        $words = explode(' ', $options['string']);
        foreach ($words as $word) {
          $query->condition('n.title', '%' . $word . '%', 'LIKE');
        }
        break;

      case 'equals':
        // No match type or incorrect match type: use "=".
      default:
        $query->condition('n.title', $options['string']);
        break;
    }
  }

  if ($options['ids']) {
    $query->condition('n.nid', $options['ids'], 'IN');
  }

  if ($options['limit']) {
    $query->range(0, $options['limit']);
  }

  $query
    ->orderBy($node_title_alias)
    ->orderBy($node_type_alias);

  $result = $query->execute()->fetchAll();
  $references = array();
  foreach ($result as $node) {
    $references[$node->nid] = array(
      'title'    => $node->node_title,
      'rendered' => check_plain($node->node_title),
    );
  }
  return $references;
  }
 