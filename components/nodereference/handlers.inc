<?php



/**

 * @file

 * question_field module nodereference component.

 */



/**

 * Implements _questions_field_defaults_component().

 */

function _questions_field_defaults_nodereference() {

  return array(

    'name' => '',
    'form_key' => NULL,
    'question' => '',
    'weight' => 0,
    'value' => '',
    'required' => 0,
    'referenceable_types' => array(),
    'autocomplete_match' => 'contains',
    'autocomplete_path' => 'questionsfield-nodereference/autocomplete',
    'widget_type' => 'nodereference_autocomplete',
    'extra' => array(
      'multiple' => 0,
      'empty_option' => 'None',
      'empty_value' => '',
      'width' => '',
      'maxlength' => '',
      'field_prefix' => '',
      'field_suffix' => '',
      'disabled' => 0,
      'title_display' => 0,
      'description' => '',
      'description_above' => FALSE,
      'placeholder' => '',
      'attributes' => array(),
      'analysis' => FALSE,
    ),

  );

}


function _questions_field_field_nodereference($component){

    $data = array('type', 'cid', 'referenceable_types', 'widget_type', 'name', 'form_key', 'extra' => array('multiple', 'maxlength'));

        $compnent_returned = array();

    foreach($data as $key => $value){

               if($key === 'extra'){

            foreach($value as $extra_key){

              if(isset($component[$key][$extra_key]))

                  $compnent_returned[$key][$extra_key] = $component[$key][$extra_key];

            }

        }

        elseif(isset($component[$value])) $compnent_returned[$value] = $component[$value];

    }

 return $compnent_returned;
}



function _questions_field_instance_nodereference($component){

    $exclude = array('type', 'cid',  'referenceable_types', 'widget_type', 'name', 'form_key', 'extra' => array('multiple', 'maxlength'));

    foreach ($exclude as $key => $value){

        if($key === 'extra'){

            foreach($value as $extra_key){

              if(isset($component[$key][$extra_key]))

                  unset($component[$key][$extra_key]);

            }

        }

        elseif(isset($component[$value])) unset($component[$value]);

    }

    return $component;
}

 
/**
 * Implements _questions_field_options_list_component().
 * @see questions_field_options_list()
 */
function _questions_field_options_list_nodereference($field, $component) {
  return qf_node_reference_options_list($field, $component);
}


/**
 * Implements _questions_field_field_views_data_component().
 * @see questions_field_field_views_data()
 */
function _questions_field_field_views_data_nodereference($data, $field, $component) {
 // It supported only node 
  $target_type = "node"; 
  $entity_info = entity_get_info($target_type);
   
   foreach ($data as $table_name => $table_data) {
     
     if(isset($entity_info['base table'])) {
       $entity = $entity_info['label'];
       if ($entity == t('Node node')) {
         $entity = t('Content node');
       }

       $field_name = $field['field_name'] . '_' . $component['cid'];
       $parameters = array('@entity' => $entity, '!field_name' => $field['field_name'], '!component' => $component['cid']);
       $data[$table_name][$field_name]['relationship'] = array(
         'handler' => 'views_handler_relationship',
         'base' => $entity_info['base table'],
         'base field' => $entity_info['entity keys']['id'],
         'label' => t('@entity entity content referenced from !field_name with component !component', $parameters),
         'title' => t('Referenced Content with component'),
         'help' => t('A bridge to the @entity entity that is referenced via !field_name with component !component', $parameters),
        'arguments' => array('module' => 'questions_field', 'component_id' => $component['cid'], 'field_name' => $field['field_name'] ), 
        );
     }
   }
 return $data;
}



/**
 * Implements _questions_field_field_views_data_views_data_alter_component().
 * @see questions_field_field_views_data_views_data_alter()
 */
function _questions_field_field_views_data_views_data_alter_nodereference($data, $field, $component) {

  //It supported only node 
  $target_type = "node";    
  foreach ($field['bundles'] as $entity_type => $bundles) {
   $target_entity_info = entity_get_info($target_type);
  if (isset($target_entity_info['base table'])) {
    $entity_info = entity_get_info($entity_type);

    $entity = $entity_info['label'];
    if ($entity == t('Node')) {
      $entity = t('Content');
    }
   $target_entity = $target_entity_info['label'];
    if ($target_entity == t('Node')) {
      $target_entity = t('Content');
    }
    $pseudo_field_name = 'reverse_' . $field['field_name']. '_' . $component['cid'] . '_' . $entity_type;
    $replacements = array('@entity' => $entity, '@target_entity' => $target_entity, '!field_name' => $field['field_name'], '!component' => $component['cid']);
  if(isset($data[$target_entity_info['base table']]))
    $data[$target_entity_info['base table']][$pseudo_field_name]['relationship'] = array(
      'handler' => 'views_handler_relationship_entity_reverse',
      'field_name' => $field['field_name'],
      'field table' => _field_sql_storage_tablename($field),
      'field field' => $field['field_name'] . '_' . $component['cid'],
      'base' => $entity_info['base table'],
      'base field' => $entity_info['entity keys']['id'],
      'label' => t('@entity referencing @target_entity from !field_name with component !component', $replacements),
      'group' => t('Content Reference'),
      'title' => t('Referencing Content with component'),
      'help' => t('A bridge to the @entity entity that is referencing @target_entity via !field_name with component !component', $replacements),
     'join_extra' => array(
        0 => array(
          'field' => 'entity_type',
          'value' => $entity_type,
        ),
        1 => array(
          'field' => 'deleted',
          'value' => 0,
          'numeric' => TRUE,
        ),
      ),
     'arguments' => array('module' => 'questions_field', 'component_id' => $component['cid'], 'field_name' => $field['field_name'] ), 
    );
  }
}

return $data;
}






/**
 * Implements _questions_field_theme_component().
 */

function _questions_field_theme_nodereference() {

    return array(
  
      'questions_field_display_nodereference' => array(
        'render element' => 'element',
        'file' => 'components/nodereference/handlers.inc',
      ),
  
      'questions_field_nodereference_item_simple' => array(
        'arguments' => array('item' => NULL),
        'file' => 'components/nodereference/handlers.inc',
      ),
    );
  
  }

/**
 * Implements _questions_field_menu_component().
 * @see questions_field_menu()
 */

function _questions_field_menu_nodereference() {
    $items = array();
    $items['questionsfield-nodereference/autocomplete/%/%/%/%'] = array(
        'title' => 'Questions field nodereference autocomplete',
        'page callback' => 'questions_field_nodereference_autocomplete',
        'page arguments' => array(2, 3, 4, 5),
        'access callback' => 'questions_field_nodereference_autocomplete_access',
        'access arguments' => array(2, 3, 4, 5),
        'type' => MENU_DEFAULT_LOCAL_TASK,
      );
      return $items;
  }
 

/**
 * Implements _questions_field_edit_component().
 */
function _questions_field_edit_nodereference($component) {

    $form = array();
    $form['value'] = array(
      '#type' => 'textfield',
      '#title' => t('Default value'),
      '#default_value' => $component['value'],
      '#description' => t('The default value of the field.') . ' ' . theme('questions_field_token_help'),
      '#size' => 60,
      '#maxlength' => 1024,
      '#weight' => 0,
    );

    $form['autocomplete_match'] = array(
        '#type'             => 'select',
        '#title'            => t('Autocomplete matching'),
        '#default_value'    => $component['autocomplete_match'],
        '#options'          => array(
          'starts_with'     => t('Starts with'),
          'contains'        => t('Contains'),
          'fuzzy'           => t('Fuzzy search'),
          'equals' => t('equals'),
        ),
        '#description'      => t('Select the method used to collect autocomplete suggestions. Note that <em>Contains</em> can cause performance issues on sites with thousands of nodes.'),
      );

      $form['widget_type'] = array(
        '#type'             => 'select',
        '#title'            => t('Widget type'),
        '#default_value'    => $component['widget_type'],
        '#options'          => array(
          'nodereference_autocomplete'     => t('Autocomplete text field'),
          'options_buttons'        => t('Check boxes/radio buttons'),
          'options_select' => t('Select list'),
          'textfield' => t('Textfield'),
        ),
        '#description'      => t('The type of form element you would like to present to the user.'),
      );

    $form['referenceable_types'] = array(
        '#type' => 'checkboxes',
        '#title' => t('Content types that can be referenced'),
        '#multiple' => TRUE,
        '#default_value' => $component['referenceable_types'],
        '#options' => array_map('check_plain', node_type_get_names()),
      );
  
    $form['display']['width'] = array(
      '#type' => 'textfield',
      '#title' => t('Width'),
      '#default_value' => $component['extra']['width'],
      '#description' => t('Width of the textfield.') . ' ' . t('Leaving blank will use the default size.'),
      '#size' => 5,
      '#maxlength' => 10,
      '#weight' => 0,
      '#parents' => array('extra', 'width'),
    );

    $form['extra']['multiple'] = array(
      '#type' => 'checkbox',
      '#title' => t('Multiple') ,
      '#default_value' => $component['extra']['multiple'],
      '#description' => t('Allow multiple node id (nid), separated by commas.') ,
      '#weight' => 0,
    );
  
    
    $form['extra']['empty_option'] = array(
      '#type' => 'textfield',
      '#title' => t('Empty Option'),
      '#default_value' => $component['extra']['empty_option'],
      '#description' => t('The label to show for the initial option denoting no selection in a select element. By default, 
                                      the label is automatically set to "- Select -" for a required field and "- None -" for an optional field.'),
    );

    $form['extra']['empty_value'] = array(
      '#type' => 'textfield',
      '#title' => t('Empty Option'),
      '#default_value' => $component['extra']['empty_value'],
      '#description' => t('The value for the initial option denoting no selection in a select element, 
                                      which is used to determine whether the user submitted a value or not.'),
    );

    $form['display']['placeholder'] = array(
      '#type' => 'textfield',
      '#title' => t('Placeholder'),
      '#default_value' => $component['extra']['placeholder'],
      '#description' => t('The placeholder will be shown in the field until the user starts entering a value.'),
      '#weight' => 1,
      '#parents' => array('extra', 'placeholder'),
    );
  
    $form['display']['field_prefix'] = array(
      '#type' => 'textfield',
      '#title' => t('Prefix text placed to the left of the textfield'),
      '#default_value' => $component['extra']['field_prefix'],
      '#description' => t('Examples: $, #, -.'),
      '#size' => 20,
      '#maxlength' => 127,
      '#weight' => 2.1,
      '#parents' => array('extra', 'field_prefix'),
    );
  
    $form['display']['field_suffix'] = array(
      '#type' => 'textfield',
      '#title' => t('Postfix text placed to the right of the textfield'),
      '#default_value' => $component['extra']['field_suffix'],
      '#description' => t('Examples: lb, kg, %.'),
      '#size' => 20,
      '#maxlength' => 127,
      '#weight' => 2.2,
      '#parents' => array('extra', 'field_suffix'),
    );
  
    $form['display']['disabled'] = array(
      '#type' => 'checkbox',
      '#title' => t('Disabled'),
      '#return_value' => 1,
      '#description' => t('Make this field non-editable. Useful for setting an unchangeable default value.'),
      '#weight' => 11,
      '#default_value' => $component['extra']['disabled'],
      '#parents' => array('extra', 'disabled'),
    );
  
  
  
    $form['validation']['maxlength'] = array(
      '#type' => 'textfield',
      '#title' => t('Maxlength'),
      '#default_value' => $component['extra']['maxlength'],
      '#description' => t('Maximum length of the textfield value.'),
      '#size' => 5,
      '#maxlength' => 10,
      '#weight' => 2,
      '#parents' => array('extra', 'maxlength'),
    );
  
    return $form;
  }

 

/**
 * 
 * Implements _questions_field_render_component()
 * @see questions_field_field_widget_form()
 */

function _questions_field_render_nodereference($component, $value = NULL, $entity = NULL, $filter = TRUE, $widget_arguments = array()) {
	list($form, $form_state, $field, $instance, $langcode, $items, $delta, $element_widget) = $widget_arguments;
 
  $element = array();
  
  switch($component['widget_type']){
      case 'nodereference_autocomplete':
 
      $element = array( 
        '#type' => 'textfield',
        '#element_validate' => array('questions_field_nodereference_autocomplete_validate'),
        '#value_callback' => 'questions_field_nodereference_autocomplete_value',
        '#autocomplete_path' => $component['autocomplete_path'] . '/' . $instance['entity_type'] . '/' . $instance['bundle']  . '/' . $field['field_name']. '/' . $component['cid'],
      );

      break;

      case 'options_buttons':
      case 'options_select':
      
      $options = qf_node_reference_options_list($field, $component);
    
      $element = array(
        '#type' => 'select',
        '#options' => $options,
        '#multiple' => $component['extra']['multiple'],
        '#element_validate' => array('questions_field_nodereference_list_validate'),
        '#empty_option' => t($component['extra']['empty_option']),
        '#empty_value' => $component['extra']['empty_value'],
       );
       if( $component['extra']['multiple'] ){
        $value = array_filter(array_map('trim', explode(',', $value)), 'strlen');
      }
     

      if($component['widget_type'] == 'options_buttons'){
        unset($element['#multiple']);
        if($component['extra']['multiple']){
              $element['#type'] = 'checkboxes';
              if(!empty($value)){
                 $value = array_filter(drupal_map_assoc($value), function($value) { return ((string)$value !== '0'); });
              }
         }else{
          $element['#type'] = 'radios'; 
              if(!$component['required']){
                $element['#options'] =   array($component['extra']['empty_value'] => t($component['extra']['empty_option'])) 
                + $element['#options'];
                  
              }
        }
      }
  
      break;

      case 'textfield':

      $element = array( 
        '#type' => 'textfield',
        '#element_validate' => array('questions_field_nodereference_textfield_validate'),
    );

      break;
  }
  
  $element += array( 
      '#title' =>  t($component['question']),
      '#title_display' => $component['extra']['title_display'] ? $component['extra']['title_display'] : 'before',
      '#required' => $component['required'],
      '#weight' => $component['weight'],
      '#field_prefix' => empty($component['extra']['field_prefix']) ? NULL : ($filter ? filter_xss(t($component['extra']['field_prefix'])) : t($component['extra']['field_prefix'])),
      '#field_suffix' => empty($component['extra']['field_suffix']) ? NULL : ($filter ? filter_xss(t($component['extra']['field_suffix'])) : t($component['extra']['field_suffix'])),
      '#description' => $filter ? questions_field_filter_descriptions(t($component['extra']['description']), $entity) : t($component['extra']['description']),
      '#attributes' => $component['extra']['attributes'],
      '#theme_wrappers' => array('form_element'),
      '#pre_render' => array(), // Needed to disable double-wrapping of radios and checkboxes.
      '#translatable' => array('title', 'description', 'field_prefix', 'field_suffix'),
      '#arguments' => array('entity_type' => $instance['entity_type'], 'bundle_name' => $instance['bundle'], 'field_name' => $field['field_name'],  'component_id' =>  $component['cid']),
      '#delta' => $delta,
    );
   

    if ($component['required']) {
  
      $element['#attributes']['required'] = 'required';
    
    }
   
    if ($component['extra']['placeholder']) {
  
      $element['#attributes']['placeholder'] = $component['extra']['placeholder'];
  
    }
   
    if ($component['extra']['disabled']) {
  
      if ($filter) {
  
        $element['#attributes']['readonly'] = 'readonly';
  
      }
  
      else {
  
        $element['#disabled'] = TRUE;
  
      }
  
    }
  
  
    if(!empty($component['extra']['wrapper_classes'])){
      $element['#prefix'] = '<div class="'.$component['extra']['wrapper_classes'].'">';
      $element['#suffix'] = '</div>';
    }
  
  
    if(!empty($component['extra']['css_classes'])){
     $element['#attributes']['class'][] = $component['extra']['css_classes'];
    }
  
  
    // Change the 'width' option to the correct 'size' option.
    if ($component['extra']['width'] > 0) {
  
      $element['#size'] = $component['extra']['width'];
  
    }
  
    if ($component['extra']['maxlength'] > 0) {
  
      $element['#maxlength'] = $component['extra']['maxlength'];
  
    }
   
    if (isset($value)) {
       $element['#default_value'] = $value;
    }
    return $element;
  
  }

  /**
 * Implements _questions_field_display_component().
 */
function _questions_field_display_nodereference($component, $value, $weight = 0, $format = 'html', $arguments_display) {
  if(empty($value))  return;
 
 list($entity_type, $entity, $field, $instance, $langcode, $items, $display) = $arguments_display;
 
  $element =  array();

  if($component['display_type'] == 'questions_answers_selected'){

      $value = explode(',', $value);
      if(empty($items))  return;
}
 
$element +=  array(
  '#title' => t($component['question']),
  '#title_display' => $component['extra']['title_display'] ? $component['extra']['title_display'] : 'before',
  '#weight' => $weight,
  '#multiple' => $component['extra']['multiple'],
  '#theme' => 'questions_field_display_nodereference',
  '#theme_wrappers' => $format == 'html' ? array('questions_field_form_element') : array('questions_field_element_text'),
  '#attributes' => array('class' => array('component', 
          'component-type-'.$component['type'], 
          'component-name-'.$component['cid'],
          $field['field_name'] . '-' . $component['cid']
        )
        ),
  '#format' => $format,
  '#value' => $value,
  '#component' => $component,
  '#translatable' => array('title', 'options'),
  '#parents' => array($component['form_key'])
);
return $element;
  
}


/**
 * Implements _questions_field_table_component().
 */

function _questions_field_table_nodereference($component, $value) {
    return check_plain(empty($value[0]) ? '' : $value[0]);
  }


/**
 * Implements _questions_field_submit_component().
 */
function _questions_field_submit_nodereference($component, $value) {
  if(is_array($value)){
    $value = array_filter($value, function($value) { return ((string)$value !== '0'); });
   
    $value = implode(',', $value);
  }
return $value;
}
    


/**
 * Implements _questions_field_action_set_component().
 */
function _questions_field_action_set_nodereference($component, &$element, &$form_state, $value) {

    $element['#value'] = $value;
  
    form_set_value($element, $value, $form_state);
  
  }
  
  
/**
 * Implements _questions_field_csv_headers_component().
 */
function _questions_field_csv_headers_nodereference($component, $export_options) {

    $header = array();
  
    $header[0] = '';
  
    $header[1] = '';
  
    $header[2] = $export_options['header_keys'] ? $component['form_key'] : $component['name'];
  
    return $header;
  
  }
  
  
  
  /**
  
   * Implements _questions_field_csv_data_component().
  
   */
  
  function _questions_field_csv_data_nodereference($component, $export_options, $value) {
  
    return !isset($value[0]) ? '' : $value[0];
  
  }
  


  function _questions_field_schema_column_nodereference() {

            return  array(
            'description' => 'Node node Reference',
            'type' => 'varchar',
            'length' => 32,
            'not null' => FALSE,
            'default' => '',
        );
}



function _questions_field_data_property_info_nodereference($component) {

    return  array(
     'label'=> $component['name'],
     'type' => 'text',
     'getter callback' => 'entity_property_verbatim_get',
     'setter callback' => 'entity_property_verbatim_set',

   );

}
