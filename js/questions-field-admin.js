/**
 * @file
 * Questions field form interface enhancments.
 */

(function($) {

    "use strict";

    Drupal.behaviors.questionsFieldAdmin = {};
    Drupal.behaviors.questionsFieldAdmin.attach = function(context) {
        // On click or change, make a parent radio button selected.
        Drupal.questionsField.setActive(context);
        Drupal.questionsField.updateTemplate(context);
        // Update the template select list upon changing a template.
        // Select all link for file extensions.
        Drupal.questionsField.selectCheckboxesLink(context);
        // Enhance the normal tableselect.js file to support indentations.
        Drupal.questionsField.tableSelectIndentation(context);
        // Automatically download exports if available.
        Drupal.questionsField.downloadExport(context);
        // Enhancements for the conditionals administrative page.
        Drupal.questionsField.conditionalAdmin(context);
        // Trigger radio/checkbox change when label click automatically selected by
        // browser.
        Drupal.questionsField.radioLabelAutoClick(context);
    };

    Drupal.questionsField = Drupal.questionsField || {};

    Drupal.questionsField.setActive = function(context) {
        $('.questions-field-inline-radio', context).click(function(e) {
            $(this).closest('.form-type-radio').find('input[type=radio]').questionsFieldProp('checked', true);
        });
        $('.questions-field-set-active', context).change(function(e) {
            if ($(this).val()) {
                $(this).closest('.form-type-radio').find('input[type=radio]').questionsFieldProp('checked', true);
            }
            e.preventDefault();
        });

        // Firefox improperly selects the parent radio button when clicking inside
        // a label that contains an input field. The only way of preventing this
        // currently is to remove the "for" attribute on the label.
        // See https://bugzilla.mozilla.org/show_bug.cgi?id=213519.
        if (navigator.userAgent.match(/Firefox/)) {
            $('.questions-field-inline-radio', context).removeAttr('for');
        }
    };

    // Update e-mail templates between default and custom.
    Drupal.questionsField.updateTemplate = function(context) {
        var defaultTemplate = $('#edit-templates-default').val();
        var $templateSelect = $('#questions-field-template-fieldset select#edit-template-option', context);
        var $templateTextarea = $('#questions-field-template-fieldset textarea:visible', context);

        var updateTemplateSelect = function() {
            if ($(this).val() == defaultTemplate) {
                $templateSelect.val('default');
            } else {
                $templateSelect.val('custom');
            }
        };

        var updateTemplateText = function() {
            if ($(this).val() == 'default' && $templateTextarea.val() != defaultTemplate) {
                if (confirm(Drupal.settings.questionsField.revertConfirm)) {
                    $templateTextarea.val(defaultTemplate);
                } else {
                    $(this).val('custom');
                }
            }
        };

        $templateTextarea.keyup(updateTemplateSelect);
        $templateSelect.change(updateTemplateText);
    };

    Drupal.questionsField.selectCheckboxesLink = function(context) {
        function selectCheckboxes() {
            var group = this.className.replace(/.*?questions-field-select-link-([^ ]*).*/, '$1');
            var $checkboxes = $('.questions-field-select-group-' + group + ' input[type=checkbox]');
            var reverseCheck = !$checkboxes[0].checked;
            $checkboxes.each(function() {
                this.checked = reverseCheck;
            });
            $checkboxes.trigger('change');
            return false;
        }
        $('a.questions-field-select-link', context).click(selectCheckboxes);
    };

    Drupal.questionsField.tableSelectIndentation = function(context) {
        var $tables = $('th.select-all', context).parents('table');
        $tables.find('input.form-checkbox').change(function() {
            var $rows = $(this).parents('table:first').find('tr');
            var row = $(this).parents('tr:first').get(0);
            var rowNumber = $rows.index(row);
            var rowTotal = $rows.size();
            var indentLevel = $(row).find('div.indentation').size();
            for (var n = rowNumber + 1; n < rowTotal; n++) {
                if ($rows.eq(n).find('div.indentation').size() <= indentLevel) {
                    break;
                }
                $rows.eq(n).find('input.form-checkbox').questionsFieldProp('checked', this.checked);
            }
        });
    };

    /**
     * Attach behaviors for questionsField results download page.
     */
    Drupal.questionsField.downloadExport = function(context) {
        if (context === document && Drupal.settings && Drupal.settings.questionsFieldExport && document.cookie.match(/questions_field_export_info=1/)) {
            window.location = Drupal.settings.questionsFieldExport;
            delete Drupal.settings.questionsFieldExport;
        }
    };


    /**
     * Triggers a change event when a label receives a click.
     *
     * When the browser automatically selects a radio button when it's label is
     * clicked, the FAPI states jQuery code doesn't receive an event. This function
     * ensures that automatically-selected radio buttons keep in sync with the
     * FAPI states.
     */
    Drupal.questionsField.radioLabelAutoClick = function(context) {
        $('label').once('questions-field-label').click(function() {
            $(this).prev('input:radio').change();
        });
    };

    /**
     * Make a prop shim for jQuery < 1.9.
     */
    $.fn.questionsFieldProp = $.fn.questionsFieldProp || function(name, value) {
        if (value) {
            return $.fn.prop ? this.prop(name, true) : this.attr(name, true);
        } else {
            return $.fn.prop ? this.prop(name, false) : this.removeAttr(name);
        }
    };

})(jQuery);