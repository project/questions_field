/**
 * @file
 * Enhancements for select list configuration options.
 */

(function ($) {

  "use strict";

  Drupal.behaviors.questionsFieldSelectLoadOptions = {};
  Drupal.behaviors.questionsFieldSelectLoadOptions.attach = function (context) {

    $('#edit-extra-options-source', context).change(function () {
      var url = Drupal.settings.questions_field.selectOptionsUrl + '/' + this.value;
      $.ajax({
        url: url,
        success: Drupal.questions_field.selectOptionsLoad,
        dataType: 'json'
      });
    });
  };

  Drupal.questions_field = Drupal.questions_field || {};

  Drupal.questions_field.selectOptionsOriginal = false;
  Drupal.questions_field.selectOptionsLoad = function (result) {
    if (Drupal.optionsElement) {
      if (result.options) {
        // Save the current select options the first time a new list is chosen.
        if (Drupal.questions_field.selectOptionsOriginal === false) {
          Drupal.questions_field.selectOptionsOriginal = $(Drupal.optionElements[result.elementId].manualOptionsElement).val();
        }
        $(Drupal.optionElements[result.elementId].manualOptionsElement).val(result.options);
        Drupal.optionElements[result.elementId].disable();
        Drupal.optionElements[result.elementId].updateWidgetElements();
      }
      else {
        Drupal.optionElements[result.elementId].enable();
        if (Drupal.questions_field.selectOptionsOriginal) {
          $(Drupal.optionElements[result.elementId].manualOptionsElement).val(Drupal.questions_field.selectOptionsOriginal);
          Drupal.optionElements[result.elementId].updateWidgetElements();
          Drupal.questions_field.selectOptionsOriginal = false;
        }
      }
    }
    else {
      var $element = $('#' + result.elementId);
      $element.questionsFieldProp('readonly', result.options);
      if (result.options) {
        $element.val(result.options);
      }
    }
  }

  /**
   * Make a prop shim for jQuery < 1.9.
   */
  $.fn.questionsFieldProp = $.fn.questionsFieldProp || function (name, value) {
    if (value) {
      return $.fn.prop ? this.prop(name, true) : this.attr(name, true);
    }
    else {
      return $.fn.prop ? this.prop(name, false) : this.removeAttr(name);
    }
  };

})(jQuery);
